<?php

use App\Service;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Service::create([
            "title_en"       => 'Extra Curricular Activities',
            "title_ar"       => 'نشاطات اضافية',
            "description_en" => 'Activities are initiated by student clubs and coordinated by the Student Center Office.',
            "description_ar" => 'اندية الطلاب تبادر بالانشطة والمركز الطلابي يساعد في تنسيقها',
            "icon"           => 'fa-futbol-o',
        ]);
        Service::create([
            "title_en"       => 'Student Orientation Programs',
            "title_ar"       => 'برنامج ارشاد الطلاب',
            "description_en" => 'Aimed to introduce new students to the campus and academic services, rules and procedures',
            "description_ar" => 'يهدف الى تعريف الطلاب الجدد على الحرم الجامعي بالاضافة الى الخدمات والقوانين واللوائح الاكاديمية',
            "icon"           => 'fa-map-signs',
        ]);
        Service::create([
            "title_en"       => 'Academic Advising Services',
            "title_ar"       => 'خدمات استشارية اكادمية',
            "description_en" => 'The academic advising develops mutual confidence between faculty adviser and students',
            "description_ar" => 'الاستشارة الاكادمية مصممة لتطور ثقة متبادلة ما بين الاستشاري الاكاديمي والطالب',
            "icon"           => 'fa-handshake-o',
        ]);
        Service::create([
            "title_en"       => 'Tutoring Classes and Courses',
            "title_ar"       => 'دورات تقوية',
            "description_en" => 'These classes are offered by excellent students to help others with their courses',
            "description_ar" => 'هذه التورات تقدم تطوعا من قبل طلاب ممتازين يسعادون فيها الطلاب الاخرون في موادهم',
            "icon"           => 'fa-book',
        ]);
        $this->command->info("Services table seeded!");
    }
}
