<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        $faker_ar = \Faker\Factory::create('ar_SA');
        Post::create([
            'title_en' => 'A Master Degree In Business Administration',
            'title_ar' => 'شهادة في ماجستير ادارة اعمال',
            'body_en'  => 'We offer a Master Degree in business administration that comprises of many comprehensive courses.',
            'body_ar'  => 'نحن نقدم شهادة ماجستير في ادارة العمال يتكون من دورات شاملة عديدة.',
            'status'   => 1,
            'type'     => Post::$TYPE_SLIDER_POST,
            'picture'  => '/images/posts/post_1.jpg',
        ]);
        Post::create([
            'title_en' => 'All Courses We Teach Are In English',
            'title_ar' => 'جميع موادنا تدرس باللغة الانجليزية',
            'body_en'  => 'Our curricula, teaching methods, and our tests are all designed to be taught in English.',
            'body_ar'  => 'مناهجنا ، وطريقة التدريس لدينا صممت خصيصا ليتم تدريسها باللغة الانجليزية.',
            'status'   => 1,
            'type'     => Post::$TYPE_SLIDER_POST,
            'picture'  => '/images/posts/post_2.jpg',
        ]);
        Post::create([
            'title_en' => 'Choose Subjects That Match Your Schedule',
            'title_ar' => 'اختر المواد التي تتناسب مع جدولك الزمني',
            'body_en'  => 'You can choose what courses to study and at what time. Just like in international universities.',
            'body_ar'  => 'يمكنك اختيار المادة واوقاتها. تماما كما هو النظام في الجامعات الدولية.',
            'status'   => 1,
            'type'     => Post::$TYPE_SLIDER_POST,
            'picture'  => '/images/posts/post_3.jpg',
        ]);
        Post::create([
            'title_en' => 'Enhance Your English Language Abilities',
            'title_ar' => 'حسن من قدراتك في اللغة الانجليزية',
            'body_en'  => 'We offer individual set of courses that focus only on improving the English language.',
            'body_ar'  => 'نحن نوفر دورات مستقلة تركز فقط على تحسين اللغة الانجليزية.',
            'status'   => 1,
            'type'     => Post::$TYPE_SLIDER_POST,
            'picture'  => '/images/posts/post_4.jpg',
        ]);
        Post::create([
            'title_en' => 'Be a Part of Many Events and Activities',
            'title_ar' => 'كن جزء من انشطة وفعاليات عديدة',
            'body_en'  => 'The Student Center is always active with clubs that promote student participation in a wide variety of activities.',
            'body_ar'  => 'المركز الطلابي هو دائم النشاط مع اندية تحفز مشاركة الطلاب في انشطة مختلفة ومتنوعة.',
            'status'   => 1,
            'type'     => Post::$TYPE_SLIDER_POST,
            'picture'  => '/images/posts/post_5.jpg',
        ]);
        Post::create([
            'title_en' => 'Get an Internationally Recognized Certificate',
            'title_ar' => 'احصل على شهادة معتمدة عالميا',
            'body_en'  => 'Our graduates can easily travel abroad and have their degrees approved for admission in higher education.',
            'body_ar'  => 'خريجين جامعتنا يستطيعون السفر والحصول على قبول في الخارج لاكمال الدراسات العليا.',
            'status'   => 1,
            'type'     => Post::$TYPE_SLIDER_POST,
            'picture'  => '/images/posts/post_6.jpg',
        ]);
        Post::create([
            'title_en' => 'Have a Great Time in Our Various Facilities',
            'title_ar' => 'استمتع بوقتك في منشئاتنا المختلفة',
            'body_en'  => 'The campus has volley ball courts, tennis tables, a silent library, and a tasty cafeteria.',
            'body_ar'  => 'الحرم الجامعي يحتوي على ملاعب كرة طائرة ، طاولات تنس ، مكتبة هادئة ، و كافيتريا شهيه.',
            'status'   => 1,
            'type'     => Post::$TYPE_SLIDER_POST,
            'picture'  => '/images/posts/post_7.jpg',
        ]);
        Post::create([
            'title_en' => 'Make-up Exam Schedule Fall 2018',
            'title_ar' => 'جدول الاختبارات التعويضية. خريف 2018',
            'body_en'  => 'Prepare yourselves, and good luck! Don\'t forget to register your names as soon as you see this!',
            'body_ar'  => 'جهزوا انفسكم وحظا سعيداً. لا تنسى ان تسجل اسمك عند التسجيل عندما ترى هذا الاعلان.',
            'status'   => 1,
            'type'     => Post::$TYPE_FEATURED_ANNOUNCEMENT,
            'picture'  => '/images/posts/makeup.jpg',
        ]);
        Post::create([
            'title_en' => 'Graduate Students Have to Register Their Names to Receive The Robes!',
            'title_ar' => 'الطلاب الخريجين يجب عليهم تسجيل اسمائهم لاستلام ثياب التخرج!',
            'body_en'  => 'Please head to the student center as soon as possible. If you want a robe you have to register your name, if you already have a robe you also have to register your name.',
            'body_ar'  => 'توجه الى المركز الطلابي باسرع وقت ممكن. اذا اردت ان تاخذ ثياب التخرج سجل اسمك. واذا لديك هي فعلا يجب ايضا ان تسجل اسمك',
            'status'   => 1,
            'type'     => Post::$TYPE_FEATURED_ANNOUNCEMENT,
            'picture'  => '/images/posts/graduation_gown.jpg',
        ]);

        if (env('DEV_SEEDS', false)) {
            factory(App\Post::class, 50)->create();
        }
        $this->command->info("Posts table seeded!");
    }
}
