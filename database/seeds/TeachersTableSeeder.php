<?php

use App\Teacher;
use Illuminate\Database\Seeder;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teacher::create([
            'name_en'  => 'Wail Al-Hakimi',
            'name_ar'  => 'وائل الحكيمي',
            'title_en' => 'Dean of Business School',
            'title_ar' => 'عميد كلية ادارة الاعمال',
            'picture'  => '/images/teachers/wail.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Shahm Othman',
            'name_ar'  => 'شهم عصمان',
            'title_en' => 'Senior Instructor of Arts',
            'title_ar' => 'محاظر في مجال الفنون',
            'picture'  => '/images/teachers/shahm.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Mohammed Al-Awlaqi',
            'name_ar'  => 'محمد العولقي',
            'title_en' => 'Finance Instructor',
            'title_ar' => 'محاظر محاسبة ومالية',
            'picture'  => '/images/teachers/awlaki.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Mohammed Faya',
            'name_ar'  => 'محمد فايع',
            'title_en' => 'Dean of Art Department',
            'title_ar' => 'محاظر في مجال الفنون',
            'picture'  => '/images/teachers/faya.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Ali Al-Sharafi',
            'name_ar'  => 'علي الشرفي',
            'title_en' => 'English Instructor',
            'title_ar' => 'مدرس',
            'picture'  => '/images/teachers/sharafi.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Assem Al-Maqtari',
            'name_ar'  => 'عاصم المقطري',
            'title_en' => 'Senior IT Instructor',
            'title_ar' => 'محاظر تكنولوجيا معلومات',
            'picture'  => '/images/teachers/assem.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Adel Al-Mawry',
            'name_ar'  => 'عادل الماوري',
            'title_en' => 'Senior Instructor of Arts',
            'title_ar' => 'عميد كلية الفنون',
            'picture'  => '/images/teachers/adel.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Abdulbaset Al-Sermi',
            'name_ar'  => 'عبدالباسط الصرمي',
            'title_en' => 'Dean of TEFL Department',
            'title_ar' => 'عميد كلية اللغة الانجليزية',
            'picture'  => '/images/teachers/abdulbaset.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Ahmed Taqi',
            'name_ar'  => 'احمد تقي',
            'title_en' => 'Dean of IT Department',
            'title_ar' => 'عميد كلية تكنولوجيا المعلومات',
            'picture'  => '/images/teachers/taqi.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Hilal Al-Khaiwani',
            'name_ar'  => 'هلال الخيواني',
            'title_en' => 'Instructor and Mentor',
            'title_ar' => 'محاظر ومرشد',
            'picture'  => '/images/teachers/hilal.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Fayez Sakran',
            'name_ar'  => 'فايز سكران',
            'title_en' => 'Pharmacy Instructor',
            'title_ar' => 'محاظر صيدلاني',
            'picture'  => '/images/teachers/faiz.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Riad Al-Oqab',
            'name_ar'  => 'رياض العقاب',
            'title_en' => 'Dean of Pharmacy Department',
            'title_ar' => 'عميد كلية الصيدلة',
            'picture'  => '/images/teachers/riadh.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Abdullah Zaid Ayssa',
            'name_ar'  => 'عبدالله زيد عيسى',
            'title_en' => 'Architecture Instructor',
            'title_ar' => 'مدرس هندسة معمارية',
            'picture'  => '/images/teachers/ayssa.jpg',
        ]);
        Teacher::create([
            'name_en'  => 'Ali Al-Ashwal',
            'name_ar'  => 'علي الاشول',
            'title_en' => 'Dean of Engineering School',
            'title_ar' => 'عميد كلية الهندسة',
            'picture'  => '/images/teachers/ashwal.jpg',
        ]);
        $this->command->info("Teachers table seeded!");
    }
}
