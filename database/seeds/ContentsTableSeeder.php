<?php

use App\Content;
use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $TYPE_NORMAL = Content::TYPE_NORMAL;
        $TYPE_EDITOR = Content::TYPE_EDITOR;

        ////////////////////////////////////
        /// Slider Start
        ////////////////////////////////////
        Content::create([
            "key"              => "first_slide_main",
            "name_en"          => "Slider First Title",
            "name_ar"          => "سلايدر واحد عنوان",
            "value_en"         => "Let your dreams come <br> true.",
            "value_ar"         => "دع احلامك تصبح <br> حقيقة.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Slider",
        ]);
        Content::create([
            "key"              => "first_slide_subtitle",
            "name_en"          => "Slider First Subtitle",
            "name_ar"          => "سلايدر واحد عنوان فرعي",
            "value_en"         => "We will provide with the best education <br> you can get in Yemen.",
            "value_ar"         => "سوف نوفر لك افضل تعليم يمكن ان <br> تحصل عليه في اليمن.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Slider",

        ]);
        Content::create([
            "key"              => "second_slide_main",
            "name_en"          => "Slider Second Title",
            "name_ar"          => "سلايدر اثنين عنوان",
            "value_en"         => "Become who you really <br> want to be.",
            "value_ar"         => "كن من تريد <br> ان تكون.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Slider",
        ]);
        Content::create([
            "key"              => "second_slide_subtitle",
            "name_en"          => "Slider Second Subtitle",
            "name_ar"          => "سلايدر اثنين عنوان فرعي",
            "value_en"         => "We guarantee an education that matches that of <br> international universities.",
            "value_ar"         => "نحن نضمن جودة تعليم تشابه تلك التي في <br> الجامعات العالمية.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Slider",

        ]);
        Content::create([
            "key"              => "third_slide_main",
            "name_en"          => "Slider Third Title",
            "name_ar"          => "سلايدر ثلاثة عنوان",
            "value_en"         => "Have the future you <br> always dreamt of.",
            "value_ar"         => "احظ بالمستقبل الذي <br> لطالما حلمت به.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Slider",
        ]);
        Content::create([
            "key"              => "third_slide_subtitle",
            "name_en"          => "Slider Third Subtitle",
            "name_ar"          => "سلايدر ثلاثة عنوان فرعي",
            "value_en"         => "Our teaching methods will make you ready to start working <br> and competing in the market.",
            "value_ar"         => "طرق تعليمنا سيجعلك جاهزا للعمل <br> والمنافسة في السوق.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Slider",

        ]);
        ////////////////////////////////////
        /// Slider End ///////////////////////////////////
        ////////////////////////////////////



        ////////////////////////////////////
        /// Home Start
        ////////////////////////////////////
        Content::create([
            "key"              => "home_courses_cant_decide",
            "name_en"          => "Home Courses Can't Decide",
            "name_ar"          => "الرئيسية كورسات لا تستطيع ان تقرر",
            "value_en"         => "Can’t Decide the Right Course? Don’t Worry!",
            "value_ar"         => "لا تستطيع ان تقرر؟ لا تقلق!",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",

        ]);
        Content::create([
            "key"              => "home_about_liu_desc",
            "name_en"          => "Home About LIU Description",
            "name_ar"          => "رئيسية وصف عن الجامعة اللبنانية",
            "value_en"         =>
                "LIU Yemen is a leading university in Yemen. It offers a range of educational and academic services. The most important of them are:",
            "value_ar"         =>
                "الجامعة اللبنانية هي الرائدة في التعليم الجامعي في اليمن. وتقدم العديد من الخدمات التعليمية والاكاديمية. اهم الخدمات منها هي",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Home",

        ]);
        Content::create([
            "key"              => "home_join_us",
            "name_en"          => "Home Teacher Join Us",
            "name_ar"          => "الرئيسية انظم الينا للتدريس",
            "value_en"         =>
                "Are you looking for an opportunity to teach? 
            Join Us!",
            "value_ar"         =>
                "هل تبحث عن فرصة لتكون معلما؟
                انظم الينا!",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",

        ]);
        Content::create([
            "key"              => "home_count_1",
            "name_en"          => "Home Count 1",
            "name_ar"          => "الرئيسية عدد 1",
            "value_en"         => "Sports Trophies",
            "value_ar"         => "كؤوس رياضة",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_count_2",
            "name_en"          => "Home Count 2",
            "name_ar"          => "الرئيسية عدد 2",
            "value_en"         => "Faculty Members",
            "value_ar"         => "عدد المدرسين",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_count_3",
            "name_en"          => "Home Count 3",
            "name_ar"          => "الرئيسية عدد 3",
            "value_en"         => "Graduation Each Year",
            "value_ar"         => "طلاب خريجين كل سنة",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);
        Content::create([
            "key"              => "home_count_4",
            "name_en"          => "Home Count 4",
            "name_ar"          => "الرئيسية عدد 4",
            "value_en"         => "Graduates get Employed",
            "value_ar"         => "خريجين يتوظفون",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Home",
        ]);


        ////////////////////////////////////
        /// Home End ///////////////////////////////////
        ////////////////////////////////////

        Content::create([
            "key"              => "footer_about_liu",
            "name_en"          => "Footer About LIU",
            "name_ar"          => "ذيل الصفحة نبذة عن الجامعة",
            "value_en"         => "A fine example of academic excellence and educational prosperity in Yemen.",
            "value_ar"         => "مثال رائع للامتياز التعليمي والأكاديمي في اليمن.",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Footer",
        ]);

        ////////////////////////////////////
        /// About Start
        ////////////////////////////////////

        Content::create([
            "key"              => "about_we_offer_best_title",
            "name_en"          => "About We Offer Best Title",
            "name_ar"          => "نحم نقدم افضل الخبرات عنوان رئيسي",
            "value_en"         => "We offer the best academic experience with the help of ",
            "value_ar"         => "نحن نقدم افضل الخبرات الاكاديمية بمساعدة",
            "type"             => $TYPE_NORMAL,
            "content_category" => "About",
        ]);

        Content::create([
            "key"              => "about_we_offer_best_subtitle",
            "name_en"          => "About We Offer Best Subtitle",
            "name_ar"          => "نحن نقدم افضل الخبرات عنوان فرعي",
            "value_en"         => "our qualified instructors and our well structured curriculum",
            "value_ar"         => "مدرسينا المؤهلين ومنهجنا المنسق بكفاءة",
            "type"             => $TYPE_NORMAL,
            "content_category" => "About",
        ]);


        Content::create([
            "key"              => "about_what_we_offer_desc",
            "name_en"          => "About What We Offer Description",
            "name_ar"          => "عنا الخدمات التي نوفرها وصف",
            "value_en"         => "This is a summarized view of some of what we offer as a university and an educational center",
            "value_ar"         => "هذه نبذة مختصرة عن ما نوفره كجامعة وكمركز تعليمي",
            "type"             => $TYPE_NORMAL,
            "content_category" => "About",
        ]);

        Content::create([
            "key"              => "about_body_large",
            "name_en"          => "About Body Large",
            "name_ar"          => "نبذة عن الجامعة",
            "value_en"         =>
                "<h3>Brief History</h3>
                        <p>Lebanese International University was founded in the republic of Lebanon. The main goal was to reshape the educational infrastructure for orphan children especially</p>

                        <h4>Our success was the reason for building the future of many orphans</h4>
                        <p>After that, many branches were established in different cities. All of which aimed to deliver the grand message that revolves around building educated future generations that include individuals from all layers of society</p>",
            "value_ar"         =>
                "<h3>تاريخ موجز</h3>
                        <p>الجامعة اللبنانية الدولية تأسست في جمهورية لبنان الشقيقة. وكان هدفها السامي هو اعادة بناء البنية التحتية التعليمية للاطفال اليتامى</p>

                        <h4>كان نجاحنا سبب في تأهيل العديد من اليتامى </h4>
                        <p>بعد ذلك تم تأسيس عدة فروع في العديد من المدن. لايصال الرسالة السامية التتي تتمحور حول انشاء اجيال متعلمة من جميع شرائح المجتمع.</p>",
            "type"             => $TYPE_EDITOR,
            "content_category" => "About",
        ]);


        Content::create([
            "key"              => "about_quote_body",
            "name_en"          => "About Quote Body",
            "name_ar"          => "عنا مقولة النص",
            "value_en"         =>
                "LIU Yemen is a unique university that ensures to provide academic excellency while also nurturing the feeling of being part of a big, loving family.",
            "value_ar"         =>
                "الجامعة اللبنانية الدولية هي جامعة فريدة ودائما تطمح لتوفير امتياز اكاديمي وايضا توفير الجو الذي يجعلك تشعر كانك فرد من عائلة كبيرة.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "About",
        ]);

        Content::create([
            "key"              => "about_quote_person",
            "name_en"          => "About Quote Person",
            "name_ar"          => "عنا مقولة الشخص",
            "value_en"         => "Ahmed Zuhrah",
            "value_ar"         => "أحمد زهرة",
            "type"             => $TYPE_EDITOR,
            "content_category" => "About",
        ]);

        Content::create([
            "key"              => "about_quote_title",
            "name_en"          => "About Quote Title",
            "name_ar"          => "عنا مقولة اللقب",
            "value_en"         => "LIU Accounting Administrator and Former LIU Student",
            "value_ar"         => "مسؤول محاسبة بالجامعة اللبنانية وطالب فيها سابقا",
            "type"             => $TYPE_EDITOR,
            "content_category" => "About",
        ]);


        ////////////////////////////////////
        /// About End ///////////////////////////////////
        ////////////////////////////////////

        ////////////////////////////////////
        /// Contact Us Start
        ////////////////////////////////////

        Content::create([
            "key"              => "contact_subtitle_desc",
            "name_en"          => "Contact Subtitle Description",
            "name_ar"          => "تواصل معنا عنوان فرعي وصف",
            "value_en"         => "We are always available to receive feedback from you. Your suggestions and complaints are always most welcome.",
            "value_ar"         => "نحن متواجدون دوما للرد على استفساراتكم. وننتظر بصدر رحب اي مقترحات او شكاوى تودون ان تدلون بها. نسعى الى ارضاءكم",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Contact",
        ]);

        ////////////////////////////////////
        /// Contact Us End ///////////////////////////////////
        ////////////////////////////////////


        ////////////////////////////////////
        /// Majors Start
        ////////////////////////////////////

        Content::create([
            "key"              => "majors_departments_desc",
            "name_en"          => "Majors Departments Description",
            "name_ar"          => "التخصصات والاقسام وصف",
            "value_en"         => "LIU Sana'a consists of many departments. Each department has different majors. Courses can be part of one or more majors. This is a list of the available departments and majors.",
            "value_ar"         => "الجامعة اللبنانية الدولية فرع صنعاء تتكون من عدة اقسام. كل قسم له عدة تخصصات. المواد يمكن ان تكون جزءا من عدة تخصصات. هذه قائمة بجميع الاقسام و التخصصات الموجودة حاليا.",
            "type"             => $TYPE_EDITOR,
            "content_category" => "Majors",
        ]);

        ////////////////////////////////////
        /// Majors End ///////////////////////////////////
        ////////////////////////////////////


        ////////////////////////////////////
        /// Teachers Start
        ////////////////////////////////////

        Content::create([
            "key"              => "teachers_subtitle_desc",
            "name_en"          => "Teachers Subtitle Description",
            "name_ar"          => "المعلمون عنوان فرعي وصف",
            "value_en"         => "These are our full-time teaching staff. They are available throughout the day, either teaching or providing consult in their offices",
            "value_ar"         => "هؤلاء هم مدرسنا المثبتين. هم متواجدون طيلة ساعات العمل الصباحية يقومون بالتدريس او بتقديم استشاراتهم في مكاتبهم.",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Teachers",
        ]);

        ////////////////////////////////////
        /// Teachers End ///////////////////////////////////
        ////////////////////////////////////

        ////////////////////////////////////
        /// Gallery Start
        ////////////////////////////////////

        Content::create([
            "key"              => "gallery_subtitle_desc",
            "name_en"          => "Gallery Subtitle Description",
            "name_ar"          => "المعرض عنوان فرعي وصف",
            "value_en"         => "A curated list of pictures of LIU Yemen and their students, performing activities and organizing events and more",
            "value_ar"         => "مجموعة مختارة من الصور للجامعة للبنانية صنعاء وطلابها وهم يقومون بانشطة وينسقون الفعاليات.",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Gallery",
        ]);

        ////////////////////////////////////
        /// Gallery End ///////////////////////////////////
        ////////////////////////////////////

        ////////////////////////////////////
        /// Site Info Start
        ////////////////////////////////////

        Content::create([
            "key"              => "site_info_address",
            "name_en"          => "Building Address",
            "name_ar"          => "عنوان المبنى",
            "value_en"         => "50th Street, Sana'a - Yemen",
            "value_ar"         => "شارع الخمسين ، صنعاء - اليمن",
            "type"             => $TYPE_NORMAL,
            "content_category" => "Info",
        ]);

        ////////////////////////////////////
        /// Site Info End ///////////////////////////////////
        ////////////////////////////////////

        ////////////////////////////////////
        /// Privacy Start
        ////////////////////////////////////

        Content::create([
            "key"              => "privacy_policy",
            "name_en"          => "Privacy Policy Description",
            "name_ar"          => "لوائح الخصوصية",
            "value_en"         => view('seeds.privacy-policy')->render(),
            "value_ar"         => view('seeds.privacy-policy')->render(),
            "type"             => $TYPE_EDITOR,
            "content_category" => "Pages",
        ]);
        Content::create([
            "key"              => "credits",
            "name_en"          => "Credits Description",
            "name_ar"          => "الشكر والعرفان",
            "value_en"         => view('seeds.credits')->render(),
            "value_ar"         => view('seeds.credits-ar')->render(),
            "type"             => $TYPE_EDITOR,
            "content_category" => "Pages",
        ]);

        Content::create([
            "key"              => "admission",
            "name_en"          => "Admission Description",
            "name_ar"          => "وصف التسجيل",
            "value_en"         => view('seeds.admission')->render(),
            "value_ar"         => view('seeds.admission-ar')->render(),
            "type"             => $TYPE_EDITOR,
            "content_category" => "Pages",
        ]);



        ////////////////////////////////////
        /// Privacy End ///////////////////////////////////
        ////////////////////////////////////


        $this->command->info("Contents table seeded!");
    }
}
