<?php

use Illuminate\Database\Seeder;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DEV_SEEDS', false)){
            factory(\App\Application::class, 50)->create();
        }
        $this->command->info("Applications table seeded!");
    }
}
