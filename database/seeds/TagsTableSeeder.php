<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'name_en' => 'Club Event',
            'name_ar' => 'فعالية نادي'
        ]);
        Tag::create([
            'name_en' => 'Poster',
            'name_ar' => 'ملصق'
        ]);
        Tag::create([
            'name_en' => 'Activity',
            'name_ar' => 'نشاط'
        ]);
        Tag::create([
            'name_en' => 'Student Center',
            'name_ar' => 'المركز الطلابي'
        ]);
        Tag::create([
            'name_en' => 'Graduation',
            'name_ar' => 'تخرج'
        ]);
        Tag::create([
            'name_en' => 'Guests',
            'name_ar' => 'ضيوف'
        ]);
        $this->command->info("Tags table seeded!");

    }
}
