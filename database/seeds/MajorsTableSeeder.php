<?php

use App\Major;
use Illuminate\Database\Seeder;

class MajorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Major::create([
            'name_en'       => 'Bachelor of Pharmacy',
            'name_ar'       => 'بكلاريوس صيدلة',
            'code'          => 'PHAR',
            'department_id' => 1,
        ]);
        Major::create([
            'name_en'       => 'International Business',
            'name_ar'       => 'ادارة اعمال دولية',
            'code'          => 'IMGT',
            'department_id' => 2,
        ]);
        Major::create([
            'name_en'       => 'Accounting',
            'name_ar'       => 'نظم معلومات محاسبية',
            'code'          => 'BAIS',
            'department_id' => 2,
        ]);
        Major::create([
            'name_en'       => 'MIS',
            'name_ar'       => 'نظم معلومات ادارية',
            'code'          => 'BMIS',
            'department_id' => 2,
        ]);
        Major::create([
            'name_en'       => 'Banking and Finance',
            'name_ar'       => 'البنوك والمالية',
            'code'          => 'BFIN',
            'department_id' => 2,
        ]);
        Major::create([
            'name_en'       => 'IT',
            'name_ar'       => 'تكولوجيا المعلومات',
            'code'          => 'CSIT',
            'department_id' => 3,
        ]);
        Major::create([
            'name_en'       => 'Graphic Design',
            'name_ar'       => 'التصميم الجرافيكي',
            'code'          => 'GDES',
            'department_id' => 3,
        ]);
        Major::create([
            'name_en'       => 'Interior Design',
            'name_ar'       => 'التصميم الداخلي',
            'code'          => 'IDES',
            'department_id' => 3,
        ]);
        Major::create([
            'name_en'       => 'Biomedical',
            'name_ar'       => 'مختبرات',
            'code'          => 'BMED',
            'department_id' => 3,
        ]);
        Major::create([
            'name_en'       => 'Nutrition',
            'name_ar'       => 'تغذية',
            'code'          => 'NUTR',
            'department_id' => 3,
        ]);
        Major::create([
            'name_en'       => 'Teaching English as a Foreign Language',
            'name_ar'       => 'تدريس اللغة الانجليزية لغير الناطقين بها',
            'code'          => 'TEFL',
            'department_id' => 4,
        ]);
        Major::create([
            'name_en'       => 'Electrical Engineering',
            'name_ar'       => 'هندسة كهرباء',
            'code'          => 'EENG',
            'department_id' => 5,
        ]);
        Major::create([
            'name_en'       => 'Biomedical Engineering',
            'name_ar'       => 'هندسة معدات طبية',
            'code'          => 'BENG',
            'department_id' => 5,
        ]);
        Major::create([
            'name_en'       => 'Telecom Engineering',
            'name_ar'       => 'هندسة اتصالات',
            'code'          => 'TENG',
            'department_id' => 5,
        ]);
        Major::create([
            'name_en'       => 'Architecture Engineering',
            'name_ar'       => 'هندسة معمارية',
            'code'          => 'ARCH',
            'department_id' => 5,
        ]);
        Major::create([
            'name_en'       => 'Masters in Business Administration',
            'name_ar'       => 'ماجستير ادارة اعمال',
            'code'          => 'MBA',
            'department_id' => 6,
        ]);

        $this->command->info("Majors table seeded!");
    }
}
