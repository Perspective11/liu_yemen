<?php

use App\ImageContent;
use Illuminate\Database\Seeder;

class ImageContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ImageContent::create([
            "key"            => "slider_1",
            "name"           => "Home Slider #1",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/slider_1.jpg",
        ]);

        ImageContent::create([
            "key"            => "slider_2",
            "name"           => "Home Slider #2",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/slider_2.jpg",
        ]);

        ImageContent::create([
            "key"            => "slider_3",
            "name"           => "Home Slider #3",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/slider_3.jpg",
        ]);

        ImageContent::create([
            "key"            => "home_course_section",
            "name"           => "Home Course Section Bg",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/home_course_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "home_latest_events_section",
            "name"           => "Home Latest Events Section Bg",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/home_latest_events_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "home_joining_section",
            "name"           => "Home Join Us Section Bg",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/home_joining_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "home_about_us_students",
            "name"           => "Home About Us Section Students Img",
            "type"           => "png",
            "resolution_x"   => "350",
            "resolution_y"   => "700",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/home_about_us_students.png",
        ]);

        ImageContent::create([
            "key"            => "home_blog_right_bar",
            "name"           => "Home Blog Right Bar Img",
            "aspect_ratio"   => "1:2",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/home_blog_right_bar.jpg",
        ]);

        ImageContent::create([
            "key"            => "home_joining_section_employee",
            "name"           => "Home Join Us Section Employee Img",
            "resolution_x"   => "225",
            "resolution_y"   => "290",
            "image_category" => "Home",
            "image_path"     => "/images/image_contents/home_joining_section_employee.png",
        ]);

        ImageContent::create([
            "key"            => "about_header_title",
            "name"           => "About Us Header Bg",
            "image_category" => "About",
            "image_path"     => "/images/image_contents/about_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "about_testimonials_section",
            "name"           => "About Us Testimonials Section Bg",
            "image_category" => "About",
            "image_path"     => "/images/image_contents/about_testimonials_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "about_history_img",
            "name"           => "About Us History Image",
            "image_category" => "About",
            "image_path"     => "/images/image_contents/about_history_img.jpg",
        ]);

        ImageContent::create([
            "key"            => "about_meet_teachers_section",
            "name"           => "About Us Meet Our Instructors Section Bg",
            "image_category" => "About",
            "image_path"     => "/images/image_contents/about_meet_teachers_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "about_employee_quote",
            "name"           => "About Us Employee Quote Img",
            "type"           => "png",
            "image_category" => "About",
            "image_path"     => "/images/image_contents/about_employee_quote.png",
        ]);

        ImageContent::create([
            "key"            => "contact_header_title",
            "name"           => "Contact Us Header Title Bg",
            "image_category" => "Contact",
            "image_path"     => "/images/image_contents/contact_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "contact_map_section",
            "name"           => "Contact Us Map Section Bg",
            "image_category" => "Contact",
            "image_path"     => "/images/image_contents/contact_map_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "majors_header_title",
            "name"           => "Majors Header Title",
            "image_category" => "Majors",
            "image_path"     => "/images/image_contents/majors_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "majors_slide_1",
            "name"           => "Majors Slide 1",
            "image_category" => "Majors",
            "image_path"     => "/images/image_contents/majors_slide_1.jpg",
        ]);

        ImageContent::create([
            "key"            => "majors_slide_2",
            "name"           => "Majors Slide 2",
            "image_category" => "Majors",
            "image_path"     => "/images/image_contents/majors_slide_2.jpg",
        ]);

        ImageContent::create([
            "key"            => "majors_faculty_section",
            "name"           => "Majors Faculty Section Bg",
            "image_category" => "Majors",
            "image_path"     => "/images/image_contents/majors_faculty_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "courses_header_title",
            "name"           => "Courses Header Title Bg",
            "image_category" => "Courses",
            "image_path"     => "/images/image_contents/courses_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "courses_course_section",
            "name"           => "Courses Course Section Bg",
            "image_category" => "Courses",
            "image_path"     => "/images/image_contents/courses_course_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "teachers_header_title",
            "name"           => "Teachers Header Title Bg",
            "image_category" => "Teachers",
            "image_path"     => "/images/image_contents/teachers_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "teachers_staff_section",
            "name"           => "Teachers Staff Section Bg",
            "image_category" => "Teachers",
            "image_path"     => "/images/image_contents/teachers_staff_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "apply_teachers_header_title",
            "name"           => "Apply Teachers Header Title Bg",
            "image_category" => "Teachers",
            "image_path"     => "/images/image_contents/apply_teachers_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "apply_teacher_form",
            "name"           => "Apply Teacher Form Bg",
            "image_category" => "Teachers",
            "image_path"     => "/images/image_contents/apply_teacher_form.jpg",
        ]);

        ImageContent::create([
            "key"            => "facilities_header_title",
            "name"           => "Facilities Header Title Bg",
            "image_category" => "Teachers",
            "image_path"     => "/images/image_contents/facilities_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "facilities_facil_section",
            "name"           => "Facilities Faculty Section Bg",
            "image_category" => "Teachers",
            "image_path"     => "/images/image_contents/facilities_facil_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "gallery_header_title",
            "name"           => "Gallery Header Title Bg",
            "image_category" => "Gallery",
            "image_path"     => "/images/image_contents/gallery_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "gallery_section",
            "name"           => "Gallery Section Bg",
            "image_category" => "Gallery",
            "image_path"     => "/images/image_contents/gallery_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "events_header_title",
            "name"           => "Events Header Title Bg",
            "image_category" => "Events",
            "image_path"     => "/images/image_contents/events_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "events_events_list",
            "name"           => "Events List Bg",
            "image_category" => "Events",
            "image_path"     => "/images/image_contents/events_events_list.jpg",
        ]);

        ImageContent::create([
            "key"            => "single_event_header_title",
            "name"           => "Single Event Header Title Bg",
            "image_category" => "Events",
            "image_path"     => "/images/image_contents/single_event_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "single_event_section",
            "name"           => "Single Event Section Bg",
            "image_category" => "Events",
            "image_path"     => "/images/image_contents/single_event_section.jpg",
        ]);


        ImageContent::create([
            "key"            => "calendar_header_title",
            "name"           => "Calendar Header Title Bg",
            "image_category" => "Events",
            "image_path"     => "/images/image_contents/calendar_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "calendar_section",
            "name"           => "Calendar Section Bg",
            "image_category" => "Events",
            "image_path"     => "/images/image_contents/calendar_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "posts_header_title",
            "name"           => "Posts Header Title Bg",
            "image_category" => "Posts",
            "image_path"     => "/images/image_contents/posts_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "posts_section",
            "name"           => "Posts Section Bg",
            "image_category" => "Posts",
            "image_path"     => "/images/image_contents/posts_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "single_post_header_title",
            "name"           => "Single Post Header Title Bg",
            "image_category" => "Posts",
            "image_path"     => "/images/image_contents/single_post_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "single_post_section",
            "name"           => "Single Post Section Bg",
            "image_category" => "Posts",
            "image_path"     => "/images/image_contents/single_post_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "announcements_header_title",
            "name"           => "Announcements Header Title Bg",
            "image_category" => "Announcements",
            "image_path"     => "/images/image_contents/announcements_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "announcements_section",
            "name"           => "Announcements Section Bg",
            "image_category" => "Announcements",
            "image_path"     => "/images/image_contents/announcements_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "single_announcement_header_title",
            "name"           => "Single Announcement Header Title Bg",
            "image_category" => "Announcements",
            "image_path"     => "/images/image_contents/single_announcement_header_title.jpg",
        ]);

        ImageContent::create([
            "key"            => "campus_map_header_en",
            "name"           => "Campus Map Header Bg English",
            "image_category" => "Campus Map",
            "image_path"     => "/images/image_contents/campus_map_header_en.jpg",
        ]);

        ImageContent::create([
            "key"            => "campus_map_header_ar",
            "name"           => "Campus Map Header Bg Arabic",
            "image_category" => "Announcements",
            "image_path"     => "/images/image_contents/campus_map_header_ar.jpg",
        ]);

        ImageContent::create([
            "key"            => "single_announcement_section",
            "name"           => "Single Announcement Section Bg",
            "image_category" => "Announcements",
            "image_path"     => "/images/image_contents/single_announcement_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "footer_section",
            "name"           => "Footer Section Bg",
            "image_category" => "Footer",
            "image_path"     => "/images/image_contents/footer_section.jpg",
        ]);

        ImageContent::create([
            "key"            => "privacy_policy_header",
            "name"           => "Privacy Policy Page Header Bg",
            "image_category" => "Pages",
            "image_path"     => "/images/image_contents/privacy_policy_header.jpg",
        ]);
        ImageContent::create([
            "key"            => "credits_header",
            "name"           => "Credits Page Header Bg",
            "image_category" => "Pages",
            "image_path"     => "/images/image_contents/credits_header.jpg",
        ]);
        ImageContent::create([
            "key"            => "feedback_header",
            "name"           => "Feedback Page Header Bg",
            "image_category" => "Pages",
            "image_path"     => "/images/image_contents/feedback_header.jpg",
        ]);
        ImageContent::create([
            "key"            => "admission_header",
            "name"           => "Admission Page Header Bg",
            "image_category" => "Pages",
            "image_path"     => "/images/image_contents/admission_header.jpg",
        ]);


        $this->command->info("Image Contents table seeded!");

    }
}
