<?php

use App\Testimonial;
use Illuminate\Database\Seeder;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        $faker_ar = \Faker\Factory::create('ar_SA');
        Testimonial::create([
            "name_en"  => 'Ahmed Mohammed Al-Hadari',
            "name_ar"  => 'احمد محمد الحضري',
            "quote_en" => 'LIU provides a great atmosphere for learning which is why I learned a great amount in my time there.',
            "quote_ar" => 'الجامعة اللبنانية توفر جو رائع للتعلم ، ولهذا السبب تعلمت الكثير في وقتي الذي امضيته فيها',
            "picture"  => '/images/testimonials/ahmed.png',
        ]);
        Testimonial::create([
            "name_en"  => 'Kamal Abdullah Al-Motwakel',
            "name_ar"  => 'كمال عبدالله المتوكل',
            "quote_en" => 'Even though I\'m currently outside Yemen. I truly wished I could try attending there because a lot of my friends are recommending it',
            "quote_ar" => 'حتى وان كنت خارج اليمن. تمنيت ان اكون جزءاً من الجامعة اللبنانية لكثرة مديح اصدقائي عليها.',
            "picture"  => '/images/testimonials/kamal.png',
        ]);
        Testimonial::create([
            "name_en"  => 'Aiman Mohammed Al-Hadari',
            "name_ar"  => 'ايمن محمد الحضري',
            "quote_en" => 'Studying in LIU is especially fun and informative, I can\'t think of a better place to study in Yemen',
            "quote_ar" => 'الدراسة في الجامعة اللبنانية هي ممتعة ومفيدة. لا استطيع التفكير في مكان افضل للحصول على التعليم الجامعي في اليمن.',
            "picture"  => '/images/testimonials/aimanh.png',
        ]);
        Testimonial::create([
            "name_en"  => 'Aliaa Abbas Alsoswa',
            "name_ar"  => 'علياء عباس السوسوة',
            "quote_en" => 'LIU Yemen has a qualified teaching staff. And they encourage many cultural and charity activities, as well as a wide verity of events.',
            "quote_ar" => 'فيها كادر تعليمي مؤهل. وتفتح المجال لمشاركة الطلاب في العديد من الانشطة الخيرية والثقافية والفعاليات المتنوعة',
            "picture"  => '/images/testimonials/aliaa.png',
        ]);
        $this->command->info("Testimonials table seeded!");
    }
}
