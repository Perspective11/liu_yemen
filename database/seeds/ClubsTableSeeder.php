<?php

use App\Club;
use Illuminate\Database\Seeder;

class ClubsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        $faker_ar = \Faker\Factory::create('ar_SA');
        Club::create([
            'name_en' => 'Medical Club',
            'name_ar' => 'النادي الطبي',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_1.jpg',
        ]);
        Club::create([
            'name_en' => 'IT Club',
            'name_ar' => 'نادي تكنولوجيا المعلومات',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_2.jpg',
        ]);
        Club::create([
            'name_en' => 'Arts Club',
            'name_ar' => 'نادي الفنون',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_3.jpg',
        ]);
        Club::create([
            'name_en' => 'Alumni Club',
            'name_ar' => 'نادي الخريجين',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_4.jpg',
        ]);
        Club::create([
            'name_en' => 'Business Club',
            'name_ar' => 'نادي ادارة الاعمال',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_5.jpg',
        ]);
        Club::create([
            'name_en' => 'Pharmacy Club',
            'name_ar' => 'نادي الصيدلة',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_6.jpg',
        ]);
        Club::create([
            'name_en' => 'Nutrition Club',
            'name_ar' => 'نادي التغذية',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_7.jpg',
        ]);
        Club::create([
            'name_en' => 'Culture Club',
            'name_ar' => 'النادي الثقافي',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_8.jpg',
        ]);
        Club::create([
            'name_en' => 'Charity Club',
            'name_ar' => 'النادي الخيري',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_9.jpg',
        ]);
        Club::create([
            'name_en' => 'Media Club',
            'name_ar' => 'نادي الاعلام',
            'description_en' => $faker->optional()->realText(180),
            'description_ar' => $faker_ar->optional()->realText(180),
            'picture' => '/images/clubs/club_10.jpg',
        ]);

        $this->command->info("Clubs table seeded!");
    }
}
