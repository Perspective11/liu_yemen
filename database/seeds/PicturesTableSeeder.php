<?php

use App\Tag;
use Illuminate\Database\Seeder;

class PicturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        if (env('DEV_SEEDS', false) || true) {

            $pictures = factory(App\Picture::class, 50)->create();
            foreach ($pictures as $picture) {
                $picture->tags()->attach($faker->randomElement(Tag::pluck('id')->toArray()));
            }
        }
        $this->command->info("Pictures table seeded!");

    }
}
