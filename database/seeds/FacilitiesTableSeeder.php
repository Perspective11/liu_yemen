<?php

use App\Facility;
use Illuminate\Database\Seeder;

class FacilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function autoInc($init = 0)
    {
        for ($i = $init; $i < 1000; $i++) {
            yield $i;
        }
    }
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        $faker_ar = \Faker\Factory::create('ar_SA');

        $facility = Facility::create([
            'name_en' => 'Library',
            'name_ar' => 'المكتبة',
            'description_en' => 'A dedicated campus library that holds a lot of useful books and references for all students to benefit from.',
            'description_ar' => 'مكتبة الحرم الجامعي التي تحتوي على كتب ومراجع مفيدة متوفرة لجميع طلابها.',
        ]);
        $pictureInc = $this->autoInc();
        $pictures = factory(App\Picture::class, 6)->make()->each(function ($p) use ($faker, $pictureInc) {
            $pictureInc->next();
            $p->image_path = '/images/facilities/library_' . $pictureInc->current() . '.jpg';
            $p->thumb_path = '/images/facilities/library_' . $pictureInc->current() . '.jpg';
            $p->save();
        });
        $facility->pictures($pictures)->saveMany($pictures);


        $facility = Facility::create([
            'name_en' => 'MBA Suit',
            'name_ar' => 'جناح ماجستير ادارة الاعمال',
            'description_en' => 'a specialized suit for MBA studies.',
            'description_ar' => 'جناح مخصص للدراسات ماجستير ادارة الاعمال',
        ]);
        $pictureInc = $this->autoInc();
        $pictures = factory(App\Picture::class, 7)->make()->each(function ($p) use ($faker, $pictureInc) {
            $pictureInc->next();
            $p->image_path = '/images/facilities/mba_' . $pictureInc->current() . '.jpg';
            $p->thumb_path = '/images/facilities/mba_' . $pictureInc->current() . '.jpg';
            $p->save();
        });
        $facility->pictures($pictures)->saveMany($pictures);


        $facility = Facility::create([
            'name_en' => 'Auditorium',
            'name_ar' => 'مسرح و قاعة محاظرات',
            'description_en' => 'This is where most of our events and talks take place.',
            'description_ar' => 'هذا هو المكان الذي تحدث فيه معظم الفعاليات والمناقشات في الجامعة.',
        ]);
        $pictureInc = $this->autoInc();
        $pictures = factory(App\Picture::class, 8)->make()->each(function ($p) use ($faker, $pictureInc) {
            $pictureInc->next();
            $p->image_path = '/images/facilities/auditorium_' . $pictureInc->current() . '.jpg';
            $p->thumb_path = '/images/facilities/auditorium_' . $pictureInc->current() . '.jpg';
            $p->save();
        });
        $facility->pictures($pictures)->saveMany($pictures);


        return;
        Facility::create([
            'name_en' => 'Student Center',
            'name_ar' => 'المركز الطلابي',
            'description_en' => $faker->realText(180),
            'description_ar' => $faker_ar->realText(180),
        ]);
        Facility::create([
            'name_en' => 'Copy Center and Stationary',
            'name_ar' => 'المكتبة ومركز طبع المستندات',
            'description_en' => $faker->realText(180),
            'description_ar' => $faker_ar->realText(180),
        ]);
        Facility::create([
            'name_en' => 'IT Corner',
            'name_ar' => 'ركن الاي تي',
            'description_en' => $faker->realText(180),
            'description_ar' => $faker_ar->realText(180),
        ]);
        Facility::create([
            'name_en' => 'Clinic',
            'name_ar' => 'عيادة',
            'description_en' => $faker->realText(180),
            'description_ar' => $faker_ar->realText(180),
        ]);
        Facility::create([
            'name_en' => 'Cafeteria',
            'name_ar' => 'كافيتيريا',
            'description_en' => $faker->realText(180),
            'description_ar' => $faker_ar->realText(180),
        ]);
        Facility::create([
            'name_en' => 'Vollyball Fields',
            'name_ar' => 'ملاعب كرة طائرة',
            'description_en' => $faker->realText(180),
            'description_ar' => $faker_ar->realText(180),
        ]);


        $this->command->info("Facilities table seeded!");
    }
}
