<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');
        $this->call(UsersTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(PicturesTableSeeder::class);
        $this->call(ApplicationsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(ClubsTableSeeder::class);
        $this->call(TestimonialsTableSeeder::class);
        $this->call(TeachersTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(CourseTypesTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(MajorsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(FacilitiesTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(ContentsTableSeeder::class);
        $this->call(ImageContentsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        ini_set('memory_limit','256M');

    }
}
