<?php

use App\CourseType;
use Illuminate\Database\Seeder;

class CourseTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseType::create([
            'name_en' => 'Other',
            'name_ar' => 'اخرى',
        ]);
        CourseType::create([
            'name_en' => 'General Requirement',
            'name_ar' => 'متطلب عام',
        ]);
        CourseType::create([
            'name_en' => 'General Elective',
            'name_ar' => 'اختياري عام',
        ]);
        $this->command->info("Course Types table seeded!");

    }
}
