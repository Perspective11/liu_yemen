<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DEV_SEEDS', false)) {
            factory(\App\Contact::class, 50)->create();
        }
        $this->command->info("Contacts table seeded!");
    }
}
