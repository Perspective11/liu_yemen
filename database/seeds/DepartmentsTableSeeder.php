<?php

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'name_en' => 'Pharmacy',
            'name_ar' => 'كلية الصيدلة',
            'picture' => '/images/departments/pharmacy.jpg',
            'icon'    => 'fa-medkit',
        ]);
        Department::create([
            'name_en' => 'Business and Management',
            'name_ar' => 'كلية ادارة الأعمال',
            'picture' => '/images/departments/business.jpg',
            'icon'    => 'fa-briefcase',
        ]);
        Department::create([
            'name_en' => 'Arts and Sciences',
            'name_ar' => 'كلية العلوم والفنون',
            'picture' => '/images/departments/arts.jpg',
            'icon'    => 'fa-paint-brush',
        ]);
        Department::create([
            'name_en' => 'TEFL',
            'name_ar' => 'اللغة الانجليزية',
            'picture' => '/images/departments/tefl.jpg',
            'icon'    => 'fa-book',
        ]);
        Department::create([
            'name_en' => 'Engineering',
            'name_ar' => 'كلية الهندسة',
            'picture' => '/images/departments/engineering.jpg',
            'icon'    => 'fa-cog',
        ]);
        Department::create([
            'name_en' => 'MBA',
            'name_ar' => 'الدراسات العليا',
            'picture' => '/images/departments/mba.jpg',
            'icon'    => 'fa-pencil',
        ]);

        $this->command->info("Departments table seeded!");
    }
}
