<?php

use Illuminate\Database\Seeder;
use App\Event;


class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function autoInc($init = 0)
    {
        for ($i = $init; $i < 1000; $i++) {
            yield $i;
        }
    }
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        $faker_ar = \Faker\Factory::create('ar_SA');
        $event = Event::create([
            'title_en'    => 'Graduation Ceremony 2018',
            'title_ar'    => 'حفل تخرج 2018',
            'body_en'     => 'Get ready for the major event of the year. Our dear graduates will enjoy a huge ceremony where they will celebrate years of learning and hard work. Stay tuned...',
            'body_ar'     => 'استعد لاكبر فعالية للسنة. طلابنا الخريجين الافاضل سوف يتمتعون بفعالية كبيرة يحتفلون بها لاكمالهم سنين من التعليم والعمل الجاد. كونوا على قرب',
            'status'      => 1,
            'location_en' => 'LIU Sana\'a Campus',
            'location_ar' => 'الحرم الجامعي فرع صنعاء',
            'date'        => Carbon\Carbon::create(2018, 10, 4),
            'poster_path' => '/images/events/graduation_poster.jpg',
            'type'        => Event::$TYPE_FEATURED_EVENT,
        ]);
        $pictureInc = $this->autoInc();
        $pictures = factory(App\Picture::class, 5)->make()->each(function ($p) use ($faker, $pictureInc) {
            $pictureInc->next();
            $p->image_path = '/images/events/graduation_' . $pictureInc->current() . '.jpg';
            $p->thumb_path = '/images/events/graduation_' . $pictureInc->current() . '.jpg';
            $p->save();
        });
        $event->pictures($pictures)->saveMany($pictures);

        $event = Event::create([
            'title_en'    => 'Orientation Week Fall 2018',
            'title_ar'    => 'الاسبوع الارشادي خريف 2018',
            'body_en'     => 'A week taken from the first month of the semester. Designed to get new students familiar with everything related to their journey in LIU. They learn about departments, majors, classes, teachers, and policies and so on.',
            'body_ar'     => 'اسبوع يؤخد من اول شهر في السمتسر. مخصص هذا الاسبوع لتعريف وارشاد الطلاب الجدد بكل ما يجب عليهم معرفته في مشوارهم التعليمي في الجامعة اللبنانية. يتضمن ذلك تعريف عن الاقسام ، التخصصات ، المدرسين ، المواد ، واللوائح وما الى ذلك.',
            'status'      => 1,
            'location_en' => 'Auditorium and Yard',
            'location_ar' => 'قاعة محاضرات في الدور السفلي والساحة',
            'date'        => Carbon\Carbon::create(2018, 10, 8),
            'poster_path' => '/images/events/orientation_poster.jpg',
            'type'        => App\Event::$TYPE_FEATURED_EVENT,
        ]);
        $pictureInc = $this->autoInc();
        $pictures = factory(App\Picture::class, 5)->make()->each(function ($p) use ($faker, $pictureInc) {
            $pictureInc->next();
            $p->image_path = '/images/events/orientation_' . $pictureInc->current() . '.jpg';
            $p->thumb_path = '/images/events/orientation_' . $pictureInc->current() . '.jpg';
            $p->save();
        });
        $event->pictures($pictures)->saveMany($pictures);

        $event = Event::create([
            'title_en'    => 'TedxLIUSanaa 2015',
            'title_ar'    => 'تيداكس الجامعة البنانية صنعاء 2018',
            'body_en'     => 'A great event where innovative individuals from around Sana\'a gather to share their stories and inspirational ideas. ',
            'body_ar'     => 'فعالية كبيرة فيها يجتمع العديد من الاشخاص المبدعين حول صنعاء ليشاركوا قصصهم وافكارهم الملهمة.',
            'status'      => 1,
            'location_en' => 'Auditorium',
            'location_ar' => 'قاعة محاضرات في الدور السفلي',
            'date'        => Carbon\Carbon::create(2015, 12, 5),
            'poster_path' => '/images/events/tedx_poster.jpg',
            'type'        => App\Event::$TYPE_FEATURED_EVENT,
        ]);
        $pictureInc = $this->autoInc();
        $pictures = factory(App\Picture::class, 3)->make()->each(function ($p) use ($faker, $pictureInc) {
            $pictureInc->next();
            $p->image_path = '/images/events/tedx_' . $pictureInc->current() . '.jpg';
            $p->thumb_path = '/images/events/tedx_' . $pictureInc->current() . '.jpg';
            $p->save();
        });
        $event->pictures($pictures)->saveMany($pictures);

        if (env('DEV_SEEDS', false)) {

            factory(App\Event::class, 20)->create();

            $events = App\Event::all();
            $pictures = App\Picture::all();
            foreach ($events as $event) {
                $event->pictures()->saveMany($pictures->random($faker->numberBetween(1, 5)));
            }
        }
        $this->command->info("Events table seeded!");
    }
}
