<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'key'     => 'social_links',
            'name_en' => 'Social Media Links',
            'name_ar' => 'قنوات التواصل الاجتماعي',
            'value'   => '{"facebook":"https://www.facebook.com/LIUYemen/","linkedin":"https://www.linkedin.com/company/lebanese-international-university-liu-yemen-/"}',
        ]);

        Setting::create([
            'key'     => 'view_courses',
            'name_en' => 'View Courses Page',
            'name_ar' => 'اظهار صفحة المواد',
            'value'   => 'true',
        ]);
        Setting::create([
            'key'     => 'view_job_apps',
            'value'   => 'true',
        ]);
        Setting::create([
            'key'     => 'nav_phone',
            'value'   => '+967 1 434 360',
        ]);
        Setting::create([
            'key'     => 'nav_email',
            'value'   => 'media@ye.liu.edu.lb',
        ]);
        Setting::create([
            'key'     => 'courses_phone',
            'value'   => '+967 1 434 360',
        ]);
        Setting::create([
            'key'     => 'contact_phone',
            'value'   => '+967 1 434 360',
        ]);
        Setting::create([
            'key'     => 'contact_email',
            'value'   => 'media@ye.liu.edu.lb',
        ]);
        Setting::create([
            'key'     => 'home_metric_1',
            'value'   => '120',
        ]);
        Setting::create([
            'key'     => 'home_metric_2',
            'value'   => '40',
        ]);
        Setting::create([
            'key'     => 'home_metric_3',
            'value'   => '300',
        ]);

        Setting::create([
            'key'     => 'home_metric_4',
            'value'   => '22',
        ]);

        Setting::create([
            'key'     => 'metric_symbol_1',
            'value'   => '',
        ]);
        Setting::create([
            'key'     => 'metric_symbol_2',
            'value'   => '',
        ]);
        Setting::create([
            'key'     => 'metric_symbol_3',
            'value'   => '',
        ]);
        Setting::create([
            'key'     => 'metric_symbol_4',
            'value'   => '',
        ]);

        $appSettings = Setting::pluck('value', 'key')->toArray();
        \Cache::forever('appSettings', $appSettings);
        $this->command->info("Settings table seeded!");

    }
}
