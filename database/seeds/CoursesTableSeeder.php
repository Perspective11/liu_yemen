<?php

use App\Course;
use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('en_US');
        $faker_ar = \Faker\Factory::create('ar_SA');
        $COURSE_TYPE_OTHER = 1;
        $COURSE_TYPE_REQUIREMENT = 2;
        $COURSE_TYPE_ELECTIVE = 3;

        $PHAR_BATCH = 10;
        $BMIS_BATCH = 11;
        $CSIT_BATCH = 12;
        $ARCH_BATCH = 13;
        $BENG_BATCH = 14;
        $BMED_BATCH = 15;
        $EENG_BATCH = 16;
        $TENG_BATCH = 17;
        $IMGT_BATCH = 18;
        $BFIN_BATCH = 19;
        $BAIS_BATCH = 20;
        $NUTR_BATCH = 21;
        $GDES_BATCH = 22;
        $IDES_BATCH = 23;
        $TEFL_BATCH = 24;
        $MBA_BATCH = 25;

        $majors = \App\Major::pluck('id', 'code')->toArray();

        ///////////////////////////
        ///  General Electives
        ///////////////////////////
        ///////////////////////////
        ///  General Electives End ////////////////////
        ///////////////////////////

        ///////////////////////////
        ///  Pharmacy
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BIOC310',
            'name_en'        => 'Medical Biochemistry',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BIOL200',
            'name_en'        => 'General Biology 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BIOL200L',
            'name_en'        => 'General Biology 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BIOL360',
            'name_en'        => 'Human Physiology and Anatomy',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BIOL360L',
            'name_en'        => 'Human Physiology and Anatomy Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BIOL385',
            'name_en'        => 'Microbiology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BIOL385L',
            'name_en'        => 'Microbiology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMED445',
            'name_en'        => 'Pathophysiology',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CHEM200',
            'name_en'        => 'General Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CHEM200L',
            'name_en'        => 'General Chemistry Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CHEM205',
            'name_en'        => 'Quantitative Analysis',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CHEM205L',
            'name_en'        => 'Quantitative Analysis Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CHEM250',
            'name_en'        => 'Organic Chemistry 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CHEM300',
            'name_en'        => 'Organic Chemistry 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CHEM300L',
            'name_en'        => 'Organic Pharmacy Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR200',
            'name_en'        => 'Introduction to Drug Information',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR250',
            'name_en'        => 'Pharmacy Practice, History and Ethics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR300',
            'name_en'        => 'Pharmaceutical Calculations',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR400',
            'name_en'        => 'Medical Chemistry 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR405',
            'name_en'        => 'Pharmaceutical Analysis And Biotechnology',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR405L',
            'name_en'        => 'Pharmaceutical Analysis And Biotechnology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR410',
            'name_en'        => 'Drug Dosage Forms 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR420',
            'name_en'        => 'Physical Pharmacy',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR425',
            'name_en'        => 'Pharmacognosy and Herbal Medicine',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR450',
            'name_en'        => 'Medical Chemistry 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR455',
            'name_en'        => 'Physical Assessment in Pharmacy Practice',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR460',
            'name_en'        => 'Pharmacy Management and Drug Marketing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR465',
            'name_en'        => 'Interpretations of Lab Data',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR470',
            'name_en'        => 'Drug Dosage Form 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR470L',
            'name_en'        => 'Drug Dosage Form 2 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR480',
            'name_en'        => 'Pharmacy Practice Experience 1 (PPEI)',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR505',
            'name_en'        => 'Pharmacology 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR510',
            'name_en'        => 'Biopharmaceutics and Pharmacokinetics',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR515',
            'name_en'        => 'Therapeutics 1 (Neurology and Psychiatry)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR520',
            'name_en'        => 'Therapeutics 2 (Pulmonary and Rheumatology)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR555',
            'name_en'        => 'Pharmacology 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR560',
            'name_en'        => 'Pharmacogenomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR565',
            'name_en'        => 'Therapeutics 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR570',
            'name_en'        => 'Therapeutics 4',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR575',
            'name_en'        => 'Pharmacology 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR580',
            'name_en'        => 'Pharmacy Practice Experience 2 (PPEII)',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR585',
            'name_en'        => 'Pharmacy Seminar',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR590',
            'name_en'        => 'Clinical Immunology',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR606',
            'name_en'        => 'Non-prescription Drugs',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR610',
            'name_en'        => 'Toxicology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR615',
            'name_en'        => 'Therapeutics 5',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR620',
            'name_en'        => 'Therapeutics 6',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR625',
            'name_en'        => 'Pharmacoeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR630',
            'name_en'        => 'Therapeutics 7',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR640',
            'name_en'        => 'Clinical Pharmacy',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR650',
            'name_en'        => 'Pharmacy Dispensing Lab',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHAR656',
            'name_en'        => 'Pharmacy Law',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'MATH245',
            'name_en'        => 'Statistics for Hear Science',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $PHAR_BATCH,
        ]);
        $courses = Course::where('batch', $PHAR_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['PHAR']);
        });
        ///////////////////////////
        ///  Pharmacy End /////////////////////////////
        ///////////////////////////
        ///////////////////////////
        ///  Management Information Systems
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BACC210',
            'name_en'        => 'Principles of Financial Accounting 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC260',
            'name_en'        => 'Principles of Financial Accounting 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BECO100',
            'name_en'        => 'Fundamentals of Economics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT200',
            'name_en'        => 'Introduction to Business Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS300',
            'name_en'        => 'Management Information Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMKT300',
            'name_en'        => 'Marketing Theory and Principles',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BSTA205',
            'name_en'        => 'Introduction to Business Statistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BECO210',
            'name_en'        => 'Introduction to Microeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BECO260',
            'name_en'        => 'Introduction to Macroeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN300',
            'name_en'        => 'Business Finance',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT300',
            'name_en'        => 'Introduction to Business Law',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT315',
            'name_en'        => 'Human Resource Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT380',
            'name_en'        => 'Business Ethics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT490',
            'name_en'        => 'Business Policies and Strategic Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS355',
            'name_en'        => 'Quantitative Methods of Business Decisions',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMTH210',
            'name_en'        => 'Business and Managerial Math',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS310',
            'name_en'        => 'Business Telecommunications',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS320',
            'name_en'        => 'Data Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS360',
            'name_en'        => 'Operations Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS370',
            'name_en'        => 'System Analysis and Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS470',
            'name_en'        => 'Decision Support Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS480',
            'name_en'        => 'Knowledge Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMIS497',
            'name_en'        => 'Business Intelligence Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BSTA305',
            'name_en'        => 'Advanced Business Statistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'CSCI230',
            'name_en'        => 'Visual Basic For Business',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC310',
            'name_en'        => 'Intermediate Financial Accounting 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC320',
            'name_en'        => 'Intermediate Financial Accounting 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC360',
            'name_en'        => 'Cost Control',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC400',
            'name_en'        => 'Accounting Information Systems and Applications',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC430',
            'name_en'        => 'Auditing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC465',
            'name_en'        => 'Tax Accounting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BACC497',
            'name_en'        => 'Advanced Accounting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BECO430',
            'name_en'        => 'International Accounting and Trade',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN350',
            'name_en'        => 'Financial Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN360',
            'name_en'        => 'Financial Reporting and Analysis',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN380',
            'name_en'        => 'Introduction to Islamic Banking',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN400',
            'name_en'        => 'Financial Modeling',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN420',
            'name_en'        => 'Lending Decision',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN430',
            'name_en'        => 'International Banking and Finance',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN450',
            'name_en'        => 'Investment Banking',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BFIN460',
            'name_en'        => 'Financial Policy',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT365',
            'name_en'        => 'Management and Organizing Theory',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT450',
            'name_en'        => 'Commercial Bank Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT470',
            'name_en'        => 'Total Quality Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMGT497',
            'name_en'        => 'Managing Entrepreneurship',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT315',
            'name_en'        => 'Integrated Marketing Communication',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT360',
            'name_en'        => 'Consumer Behavior',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT360',
            'name_en'        => 'Market Research Method',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT420',
            'name_en'        => 'Customer Service Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT450',
            'name_en'        => 'Internet Marketing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT456',
            'name_en'        => 'Sales Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT490',
            'name_en'        => 'Marketing Policies and Strategies',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMKT497',
            'name_en'        => 'Retailing and Merchandising Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'IMGT420',
            'name_en'        => 'International Business Law',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'IMGT430',
            'name_en'        => 'International Business Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'IMGT440',
            'name_en'        => 'Global Strategic Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'IMGT470',
            'name_en'        => 'Global Entrepreneurship',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'IMGT480',
            'name_en'        => 'Competition and Strategy in Global Markets',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'IMKT400',
            'name_en'        => 'International Business Marketing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMIS_BATCH,
        ]);
        $courses = Course::where('batch', $BMIS_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['BMIS']);
        });

        ///////////////////////////
        ///  Management Information Systems End /////////////////////////////
        ///////////////////////////
        ///////////////////////////
        ///  General Requirements
        ///////////////////////////
        $course = Course::create([
            'code'           => 'ARAB200',
            'name_en'        => 'Arabic Language and Literature',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'COMM105',
            'name_en'        => 'Essentials of Mass Communication',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'CSCI100',
            'name_en'        => 'Basic Computing Skills',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'CULT200',
            'name_en'        => 'Introduction to Arab and Islamic Civilizations',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'ENGL150',
            'name_en'        => 'English Composition and Rhetoric',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'ENGL200',
            'name_en'        => 'Advanced English Composition and Rhetoric',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'ENGL207',
            'name_en'        => 'English Reading Skill',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'ENGL250',
            'name_en'        => 'Technical Writing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'ENGL350',
            'name_en'        => 'English Communication Skills',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'HUMN210',
            'name_en'        => 'Human Rights - Global Perspective',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'MATH100',
            'name_en'        => 'College Algebra',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);
        $course = Course::create([
            'code'           => 'POLS440',
            'name_en'        => 'The Arab Israeli Conflict',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_REQUIREMENT,
        ]);

        ///////////////////////////
        ///  General Requirements End //////////////////////////////
        /// ///////////////////////

        $course = Course::create([
            'code'           => 'CSCI202',
            'name_en'        => 'Computer Hardware',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250',
            'name_en'        => 'Introduction to Programming',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250L',
            'name_en'        => 'Introduction to Programming Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI300',
            'name_en'        => 'Intermediate Programming With Objects',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI300L',
            'name_en'        => 'Intermediate Programming With Objects Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI320',
            'name_en'        => 'Software Engineering',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI385',
            'name_en'        => 'Visual Programming',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI392',
            'name_en'        => 'Computer Networks',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI392L',
            'name_en'        => 'Computer Networks Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI395',
            'name_en'        => 'Introduction to Database Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSIT445',
            'name_en'        => 'Website Design, Management, and Application Development',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH120',
            'name_en'        => 'Calculus 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH170',
            'name_en'        => 'Calculus 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH225',
            'name_en'        => 'Linear Algebra With Applications',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS360',
            'name_en'        => 'Operations Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS360',
            'name_en'        => 'Knowledge Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS497',
            'name_en'        => 'Business Intelligence Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI410',
            'name_en'        => 'Web Programming',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI430',
            'name_en'        => 'Introduction to Operating Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI430L',
            'name_en'        => 'Introduction to Operating Systems Lab',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSIT390',
            'name_en'        => 'DBMS Development and Administration',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSIT400',
            'name_en'        => 'Information System Development and Implementation',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSIT460',
            'name_en'        => 'Application Development Using Database Technology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSIT475',
            'name_en'        => 'Advanced System Administration',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSIT480',
            'name_en'        => 'System and Network Security',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);

        $course = Course::create([
            'code'           => 'CSCI375',
            'name_en'        => 'Algorithms',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI450',
            'name_en'        => 'Introduction to Theory of Computation',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI460',
            'name_en'        => 'Introduction Computer Graphics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI490',
            'name_en'        => 'Introduction to Artificial Intelligence',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI375',
            'name_en'        => 'Geographic Information Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI380',
            'name_en'        => 'Remote Sensing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $CSIT_BATCH,

        ]);

        $courses = Course::where('batch', $CSIT_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['CSIT']);
        });

        ///////////////////////////
        ///////////////////////////
        ///   Architecture Engineering (ARCH)
        ///////////////////////////
        $course = Course::create([
            'code'           => 'ARCH150',
            'name_en'        => 'Surveying for Architects',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH300',
            'name_en'        => 'Architectural Graphic (Architectural Drawing)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH310',
            'name_en'        => 'Building Materials (Materials Techniques and Building Codes)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH340',
            'name_en'        => 'Computer Aided Design 1 - 2D (Computer Aided Design 1-2D AutoCad)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH350',
            'name_en'        => 'Interior Architecture (Interior Design Studio 1 – for Architecture)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS200',
            'name_en'        => 'Foundation Drawing 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS205',
            'name_en'        => 'Geometry for the Studio',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS250',
            'name_en'        => 'Foundation Drawing 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS260',
            'name_en'        => 'Design Fundamentals - 2D',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS270',
            'name_en'        => 'Design Fundamentals - 3D',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS320',
            'name_en'        => 'Rendering and Perspective Techniques',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH110',
            'name_en'        => 'Pre-Calculus',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH120',
            'name_en'        => 'Calculus 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH210',
            'name_en'        => 'Architectural Design Studio 1',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH220',
            'name_en'        => 'Architectural Design Studio 2',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ARCH311',
            'name_en'        => 'Architectural Design Studio 3',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH280',
            'name_en'        => 'History of Ancient Architecture (History of Ancient Architecture & Art)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH315',
            'name_en'        => 'Building Construction 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH320',
            'name_en'        => 'Architectural Design Studio 4',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH325',
            'name_en'        => 'Building Construction 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH330',
            'name_en'        => 'History of Modern Architecture (History of Modern Architecture & Art)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH341',
            'name_en'        => 'Structural Analysis',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH345',
            'name_en'        => 'Structural Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH351',
            'name_en'        => 'History of Islamic & Local Architecture',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH360',
            'name_en'        => 'Theory of Architecture 1',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH380',
            'name_en'        => 'Computer Aided Design 2 - 3D -Autodesk Architecture or Revit',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH400',
            'name_en'        => 'Architectural Modeling',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH410',
            'name_en'        => 'Architectural Design Studio 5',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH411',
            'name_en'        => 'Working Drawings',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH420',
            'name_en'        => 'Urban Design Studio 6',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH430',
            'name_en'        => 'Computer Rendering for Architectural Design -3DMAX /MAYA',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH440',
            'name_en'        => 'Landscape Architecture (Landscape Design)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH460',
            'name_en'        => 'Theory of Architecture 2',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH461',
            'name_en'        => 'Urban Planning',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH470',
            'name_en'        => 'Lighting and HVAC',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH471',
            'name_en'        => 'Environment Control 1',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH472',
            'name_en'        => 'Environment Control 2',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH480',
            'name_en'        => 'Professional Practice for Architecture',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH481',
            'name_en'        => 'Conservation of Historic Sites',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH491',
            'name_en'        => 'Steel Structure',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH500',
            'name_en'        => 'Reinforced Concrete',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH520',
            'name_en'        => 'Architecture Senior Project’s Program',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH530',
            'name_en'        => 'Architecture Senior Project Design',
            'credits'        => 6,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARCH591',
            'name_en'        => 'Housing Issues, Policy & Design',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS210',
            'name_en'        => 'Color Theory - painting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS350',
            'name_en'        => 'Photography',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $ARCH_BATCH,

        ]);
        $courses = Course::where('batch', $ARCH_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['ARCH']);
        });

        ///////////////////////////
        ///   Architecture Engineering End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        ///  BENG - Electronic Engineering / Emphasis On Biomedical
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BIOL125',
            'name_en'        => 'Basic Biology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM125',
            'name_en'        => 'Basic Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250',
            'name_en'        => 'Introduction to Programming',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250L',
            'name_en'        => 'Introduction to Programming Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IENG300',
            'name_en'        => 'Engineering Project Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH120',
            'name_en'        => 'Calculus 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH170',
            'name_en'        => 'Calculus 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH220',
            'name_en'        => 'Calculus 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH225',
            'name_en'        => 'Linear Algebra with Applications',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH270',
            'name_en'        => 'Ordinary Differential Equations',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH310',
            'name_en'        => 'Probability & Statistics for Scientists & Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH360',
            'name_en'        => 'Advanced Engineering Math',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH375',
            'name_en'        => 'Numerical Methods for Scientists & Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MENG225',
            'name_en'        => 'Engineering Drawing & CAD',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MENG250',
            'name_en'        => 'Mechanics 1 (Statics)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'MENG300',
            'name_en'        => 'Mechanics 2 (Dynamics 1)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'PHYS100',
            'name_en'        => 'College Physics 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'PHYS150',
            'name_en'        => 'College Physics 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG300',
            'name_en'        => 'Fundamentals of Digital Logic Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG350',
            'name_en'        => 'Digital Logic Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG352L',
            'name_en'        => 'Digital Logic Circits Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG250',
            'name_en'        => 'Electric Circuits 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG250L',
            'name_en'        => 'Electric Circuits 1 Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG300',
            'name_en'        => 'Electric Circuits 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG300L',
            'name_en'        => 'Electric Circuits 2 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG304',
            'name_en'        => 'Biology for Biomedical Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG350',
            'name_en'        => 'Electronic Circuits 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG350L',
            'name_en'        => 'Electronic Circuits 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG354',
            'name_en'        => 'Physiology for Biomedical Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG385',
            'name_en'        => 'Signals and Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG400',
            'name_en'        => 'Electronic Circuits 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG400L',
            'name_en'        => 'Electronic Circuits 2 Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG404',
            'name_en'        => 'Biophysics and Bioelectricity',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG414',
            'name_en'        => 'Biocompatibility',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG424',
            'name_en'        => 'Medical Instrumentation 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG424L',
            'name_en'        => 'Medical Instrumentation 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG430',
            'name_en'        => 'Electromagnetic Fields and Waves',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG464P',
            'name_en'        => 'Practical Field Training On Medical Equipment',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG474',
            'name_en'        => 'Medical Imaging 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG494',
            'name_en'        => 'Senior Project',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BENG_BATCH,

        ]);
        $courses = Course::where('batch', $BENG_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['BENG']);
        });

        ///////////////////////////
        ///   Biomedical Engineering End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        /// Biomedical Science (BMED)
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BIOC310',
            'name_en'        => 'Medical Biochemistry',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL200',
            'name_en'        => 'General Biology 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL200L',
            'name_en'        => 'General Biology I Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL275',
            'name_en'        => 'Cell and Molecular Biology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL275L',
            'name_en'        => 'Cell and Molecular Biology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL360',
            'name_en'        => 'Human Physiology & Anatomy',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL360L',
            'name_en'        => 'Human Physiology & Anatomy Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL365',
            'name_en'        => 'Genetics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL385',
            'name_en'        => 'Microbiology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL385L',
            'name_en'        => 'Microbiology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL415',
            'name_en'        => 'Endocrinology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED205',
            'name_en'        => 'Biophysics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED255',
            'name_en'        => 'Epidemiology & Biostatistics ',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED310',
            'name_en'        => 'Basic Health Care and Medical Terminology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED330',
            'name_en'        => 'Medical Lab Organization and Professional Ethics ',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BMED370L',
            'name_en'        => 'Parasitology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED380',
            'name_en'        => 'Introduction to Quality Management and Accreditation',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED440',
            'name_en'        => 'criminology and forensic science',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM100',
            'name_en'        => 'Freshman Chemistry 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM150',
            'name_en'        => 'Freshman Chemistry 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM150L',
            'name_en'        => 'Freshman Chemistry 2 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM200',
            'name_en'        => 'General Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM255',
            'name_en'        => 'Basic Organic Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM255L',
            'name_en'        => 'Basic Organic Chemistry Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL425',
            'name_en'        => 'Immunology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED360',
            'name_en'        => 'Hematology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED360L',
            'name_en'        => 'Hematology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED370',
            'name_en'        => 'Parasitology',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED425',
            'name_en'        => 'Serology and Blood Banking',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED425L',
            'name_en'        => 'Serology and Blood Banking Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED430',
            'name_en'        => 'Medical Microbiology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED430L',
            'name_en'        => 'Medical Microbiology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED435L',
            'name_en'        => 'Clinical Hematology and Hemostasis',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);

        $course = Course::create([
            'code'           => 'BMED445',
            'name_en'        => 'Pathophysiology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED445L',
            'name_en'        => 'Clinical Case Conference',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);

        $course = Course::create([
            'code'           => 'BMED450',
            'name_en'        => 'Clinical Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED450L',
            'name_en'        => 'Clinical Chemistry Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED460',
            'name_en'        => 'Histopathology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED470',
            'name_en'        => 'Clinical Mycology and Virology',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMED485',
            'name_en'        => 'Clinical Assessment and Techniques',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MEDL399',
            'name_en'        => 'Medical Laboratory Internship 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MEDL499',
            'name_en'        => 'Medical Laboratory Internship II',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BMED_BATCH,

        ]);
        $courses = Course::where('batch', $BMED_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['BMED']);
        });

        ///////////////////////////
        ///  Biomedical Science (BMED)  End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        ///  Electrical Engineering (EENG)
        ///////////////////////////
        $course = Course::create([
            'code'           => 'CHEM125',
            'name_en'        => 'Basic Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250',
            'name_en'        => 'Introduction to Programming',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250L',
            'name_en'        => 'Introduction to Programming Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IENG300',
            'name_en'        => 'Engineering Project Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH120',
            'name_en'        => 'Calculus 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH170',
            'name_en'        => 'Calculus 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH220',
            'name_en'        => 'Calculus 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH225',
            'name_en'        => 'Linear Algebra with Applications',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH270',
            'name_en'        => 'Ordinary Differential Equations',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH310',
            'name_en'        => 'Probability & Statistics for Scientists & Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH360',
            'name_en'        => 'Advanced Engineering Math',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH375',
            'name_en'        => 'Numerical Methods for Scientists & Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MENG225',
            'name_en'        => 'Engineering Drawing & CAD',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MENG250',
            'name_en'        => 'Mechanics 1 (Statics)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHYS100',
            'name_en'        => 'College Physics 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'PHYS150',
            'name_en'        => 'College Physics 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG300',
            'name_en'        => 'Fundamentals of Digital Logic Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG350',
            'name_en'        => 'Digital Logic Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG352L',
            'name_en'        => 'Digital Logic Circits Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG405',
            'name_en'        => 'Microprocessor Architecture & Organization',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG405L',
            'name_en'        => 'Microprocessor Architecture and Organization Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG250',
            'name_en'        => 'Electric Circuits 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG250L',
            'name_en'        => 'Electric Circuits 1 Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG300',
            'name_en'        => 'Electric Circuits 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG300L',
            'name_en'        => 'Electric Circuits 2 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG350',
            'name_en'        => 'Electronic Circuits 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG350L',
            'name_en'        => 'Electronic Circuits 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG385',
            'name_en'        => 'Signals and Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG400',
            'name_en'        => 'Electronic Circuits 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG400L',
            'name_en'        => 'Electronic Circuits 2 Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG410',
            'name_en'        => 'Power Electronics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG430',
            'name_en'        => 'Electromagnetic Fields and Waves',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG435',
            'name_en'        => 'Control Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG435L',
            'name_en'        => 'Control Systems Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG440',
            'name_en'        => 'Electric Machines 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG445',
            'name_en'        => 'Power Transmission & Distribution',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG460',
            'name_en'        => 'Introduction to Power Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG487',
            'name_en'        => 'Digital Signal Processing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG487L',
            'name_en'        => 'Digital Signal Processing Lab',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG495',
            'name_en'        => 'Senior Project',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG530',
            'name_en'        => 'Electric Machines 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG530L',
            'name_en'        => 'Electric Machines 2 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $EENG_BATCH,

        ]);
        $courses = Course::where('batch', $EENG_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['EENG']);
        });

        ///////////////////////////
        ///  Electrical Engineering (EENG) End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        /// Communication Engineering (TENG)
        ///////////////////////////
        $course = Course::create([
            'code'           => 'CHEM125',
            'name_en'        => 'Basic Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250',
            'name_en'        => 'Introduction to Programming',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI250L',
            'name_en'        => 'Introduction to Programming Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IENG300',
            'name_en'        => 'Engineering Project Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH120',
            'name_en'        => 'Calculus 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH170',
            'name_en'        => 'Calculus 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH220',
            'name_en'        => 'Calculus 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH225',
            'name_en'        => 'Linear Algebra with Applications',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH270',
            'name_en'        => 'Ordinary Differential Equations',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH310',
            'name_en'        => 'Probability & Statistics for Scientists & Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH360',
            'name_en'        => 'Advanced Engineering Math',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI202',
            'name_en'        => 'Computer Hardware',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CSCI300',
            'name_en'        => 'Intermediate Programming with Objects',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MENG225',
            'name_en'        => 'Engineering Drawing & CAD',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MENG250',
            'name_en'        => 'Mechanics 1 (Statics)',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'PHYS100',
            'name_en'        => 'College Physics 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'PHYS150',
            'name_en'        => 'College Physics 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG300',
            'name_en'        => 'Fundamentals of Digital Logic Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG350',
            'name_en'        => 'Digital Logic Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG352L',
            'name_en'        => 'Digital Logic Circits Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG405',
            'name_en'        => 'Microprocessor Architecture & Organization',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG405L',
            'name_en'        => 'Microprocessor Architecture and Organization Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG250',
            'name_en'        => 'Electric Circuits 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG250L',
            'name_en'        => 'Electric Circuits 1 Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG300',
            'name_en'        => 'Electric Circuits 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG300L',
            'name_en'        => 'Electric Circuits 2 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG350',
            'name_en'        => 'Electronic Circuits 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG350L',
            'name_en'        => 'Electronic Circuits 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG385',
            'name_en'        => 'Signals and Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG400',
            'name_en'        => 'Electronic Circuits 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG400L',
            'name_en'        => 'Electronic Circuits 2 Lab.',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG360',
            'name_en'        => 'Applied Algorithms for Engineers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG430',
            'name_en'        => 'Electromagnetic Fields and Waves',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG410',
            'name_en'        => 'Computer & Network Security',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG415',
            'name_en'        => 'Communication Networks',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CENG440',
            'name_en'        => 'Introduction to Database',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG450',
            'name_en'        => 'RF Electronics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG457',
            'name_en'        => 'Communication Systems 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG487',
            'name_en'        => 'Digital Signal Processing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG457L',
            'name_en'        => 'Communication Systems 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG495',
            'name_en'        => 'Senior Project',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'EENG480',
            'name_en'        => 'Electromagnetic Wave Propagation',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TENG_BATCH,

        ]);
        $courses = Course::where('batch', $TENG_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['TENG']);
        });

        ///////////////////////////
        /// Communication Engineering (TENG) End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        ///  IMGT - International Business Management
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BACC210',
            'name_en'        => 'Principles of Financial Accounting 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => 1,

        ]);
        $course = Course::create([
            'code'           => 'BACC260',
            'name_en'        => 'Principles of Financial Accounting 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO100',
            'name_en'        => 'Fundamentals of Economics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO210',
            'name_en'        => 'Introduction to Microeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO260',
            'name_en'        => 'Introduction to Macroeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN300',
            'name_en'        => 'Business Finance',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT200',
            'name_en'        => 'Introduction to Business Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT300',
            'name_en'        => 'Introduction to Business Law',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT315',
            'name_en'        => 'Human Resource Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT380',
            'name_en'        => 'Business Ethics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT490',
            'name_en'        => 'Business Policies & Strategic Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS300',
            'name_en'        => 'Management Information Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS355',
            'name_en'        => 'Quantitative Methods of Business Decisions',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMKT300',
            'name_en'        => 'Marketing Theory and Principles',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMTH210',
            'name_en'        => 'Business and Managerial Math',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BSTA205',
            'name_en'        => 'Introduction to Business Statistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO430',
            'name_en'        => 'International Economics and Trade',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN430',
            'name_en'        => 'International Banking and Finance',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT450',
            'name_en'        => 'Commercial Bank Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT470',
            'name_en'        => 'Total Quality Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT497',
            'name_en'        => 'Managing Entrepreneurship',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS360',
            'name_en'        => 'Operations Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS400',
            'name_en'        => 'E-Business',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IMGT420',
            'name_en'        => 'International Business Law',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IMGT430',
            'name_en'        => 'International Business Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IMKT400',
            'name_en'        => 'International Business Marketing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IMGT_BATCH,

        ]);
        $courses = Course::where('batch', $IMGT_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['IMGT']);
        });

        ///////////////////////////
        ///   International Business Management End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        ///  BAIS - Accounting Information System
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BACC210',
            'name_en'        => 'Principles of Financial Accounting 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC260',
            'name_en'        => 'Principles of Financial Accounting 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO100',
            'name_en'        => 'Fundamentals of Economics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO210',
            'name_en'        => 'Introduction to Microeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO260',
            'name_en'        => 'Introduction to Macroeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN300',
            'name_en'        => 'Business Finance',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT200',
            'name_en'        => 'Introduction to Business Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT300',
            'name_en'        => 'Introduction to Business Law',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT315',
            'name_en'        => 'Human Resource Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT380',
            'name_en'        => 'Business Ethics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT490',
            'name_en'        => 'Business Policies & Strategic Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS300',
            'name_en'        => 'Management Information Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS355',
            'name_en'        => 'Quantitative Methods of Business Decisions',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMKT300',
            'name_en'        => 'Marketing Theory and Principles',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMTH210',
            'name_en'        => 'Business and Managerial Math',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BSTA205',
            'name_en'        => 'Introduction to Business Statistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC310',
            'name_en'        => 'Intermediate Financial Accounting 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC320',
            'name_en'        => 'Intermediate Financial Accounting 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC350',
            'name_en'        => 'Managerial Accounting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC360',
            'name_en'        => 'Cost Control',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC400',
            'name_en'        => 'Accounting Information Systems and Applications',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC430',
            'name_en'        => 'Auditing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC431',
            'name_en'        => 'Auditing 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC465',
            'name_en'        => 'Tax Accounting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC497',
            'name_en'        => 'Advanced Accounting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT470',
            'name_en'        => 'Total Quality Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS320',
            'name_en'        => 'Data Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BSTA305',
            'name_en'        => 'Advanced Business Statistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BAIS_BATCH,

        ]);

        $courses = Course::where('batch', $BAIS_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['BAIS']);
        });

        ///////////////////////////
        ///   Accounting Information System End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        ///  Banking & Finance (BFIN)
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BACC210',
            'name_en'        => 'Principles of Financial Accounting 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC260',
            'name_en'        => 'Principles of Financial Accounting 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO100',
            'name_en'        => 'Fundamentals of Economics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO210',
            'name_en'        => 'Introduction to Microeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BECO260',
            'name_en'        => 'Introduction to Macroeconomics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN300',
            'name_en'        => 'Business Finance',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT200',
            'name_en'        => 'Introduction to Business Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT300',
            'name_en'        => 'Introduction to Business Law',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT315',
            'name_en'        => 'Human Resource Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT380',
            'name_en'        => 'Business Ethics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT490',
            'name_en'        => 'Business Policies & Strategic Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS300',
            'name_en'        => 'Management Information Systems',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMIS355',
            'name_en'        => 'Quantitative Methods of Business Decisions',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMKT300',
            'name_en'        => 'Marketing Theory and Principles',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMTH210',
            'name_en'        => 'Business and Managerial Math',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'BSTA205',
            'name_en'        => 'Introduction to Business Statistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BACC310',
            'name_en'        => 'Intermediate Financial Accounting 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN350',
            'name_en'        => 'Financial Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN360',
            'name_en'        => 'Financial Reporting and Analysis',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN380',
            'name_en'        => 'Introduction to Islamic Banking',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN400',
            'name_en'        => 'Financial Modeling',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN420',
            'name_en'        => 'Lending Decision',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN430',
            'name_en'        => 'International Banking and Finance',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BFIN450',
            'name_en'        => 'Investment Banking',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT450',
            'name_en'        => 'Commercial Bank Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BSTA305',
            'name_en'        => 'Advanced Business Statistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $BFIN_BATCH,

        ]);
        $courses = Course::where('batch', $BFIN_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['BFIN']);
        });
        ///////////////////////////
        ///   Banking & Finance (BFIN) End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        ///  NUTR - Nutrition & Dietetics
        ///////////////////////////
        $course = Course::create([
            'code'           => 'BIOL125',
            'name_en'        => 'Basic Biology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL200',
            'name_en'        => 'General Biology 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BIOL200L',
            'name_en'        => 'General Biology 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'BMGT200',
            'name_en'        => 'Introduction to Business Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM100',
            'name_en'        => 'Freshman Chemistry 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM125',
            'name_en'        => 'Basic Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM150',
            'name_en'        => 'Freshman Chemistry 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM200',
            'name_en'        => 'General Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM200L',
            'name_en'        => 'General Chemistry Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM255',
            'name_en'        => 'Basic Organic Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CHEM255L',
            'name_en'        => 'Basic Organic Chemistry Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'CLNC400',
            'name_en'        => 'Clinic Training',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'FDSC300',
            'name_en'        => 'Technology of Food Products',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'FDSC345',
            'name_en'        => 'Food Microbiology 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'FDSC345L',
            'name_en'        => 'Food Microbiology 1 Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'FDSC370',
            'name_en'        => 'Food Chemistry',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'FDSC370L',
            'name_en'        => 'Food Chemistry Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'MATH245',
            'name_en'        => 'Statistics for Health Sciences',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR250',
            'name_en'        => 'Basic Nutrition',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR295',
            'name_en'        => 'Basic Human Physiology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR295L',
            'name_en'        => 'Basic Human Physiology Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR310',
            'name_en'        => 'Biochemistry for Nutrition',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR460',
            'name_en'        => 'Sport Nutrition',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR465',
            'name_en'        => 'Nutrient Drug Interations',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'FDSC420',
            'name_en'        => 'Food Processing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'FDSC420L',
            'name_en'        => 'Food Preservation and Processing Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'FDSC460',
            'name_en'        => 'Food Service Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR300',
            'name_en'        => 'Human Nutrition',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR350',
            'name_en'        => 'Nutritional Assessment and Counseling',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR360',
            'name_en'        => 'Pathological Basis of Disease',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR400',
            'name_en'        => 'Nutrition Through Life Span',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR410',
            'name_en'        => 'Therapeutic Nutrition',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR410L',
            'name_en'        => 'Therapeutic Nutrition Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR450',
            'name_en'        => 'Community Nutrition',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR475',
            'name_en'        => 'Inborn Errors of Metabolism',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR475L',
            'name_en'        => 'Inborn Errors of Metabolism Lab',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR480',
            'name_en'        => 'Tutorial in Nutrition',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR490',
            'name_en'        => 'Nutrition Seminar',
            'credits'        => 1,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR500',
            'name_en'        => 'Clinical Training & Practice 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'NUTR550',
            'name_en'        => 'Clinical Training & Practice 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $NUTR_BATCH,

        ]);
        $courses = Course::where('batch', $NUTR_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['NUTR']);
        });

        ///////////////////////////
        ///  NUTR - Nutrition & Dietetics End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        /// GDES - Graphic Design
        ///////////////////////////
        $course = Course::create([
            'code'           => 'ARTS200',
            'name_en'        => 'Foundation Drawing 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS205',
            'name_en'        => 'Geometry for the Studio',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS210',
            'name_en'        => 'Color Theory - Painting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS250',
            'name_en'        => 'Foundation Drawing 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS260',
            'name_en'        => 'Design Fundamentals - 2D',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS270',
            'name_en'        => 'Design Fundamentals - 3D',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS280',
            'name_en'        => 'History of Ancient Art',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS320',
            'name_en'        => 'Rendering and Perspective Techniques',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS330',
            'name_en'        => 'History of Modern Art',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS350',
            'name_en'        => 'Photography',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES300',
            'name_en'        => 'Typography 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES310',
            'name_en'        => 'Illustration I - Graphic Design Studio 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES315',
            'name_en'        => 'History of Graphic Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES350',
            'name_en'        => 'Typography II & Art of Calligraphy',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES365',
            'name_en'        => 'Illustration II - Story Board',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'GDES425A',
            'name_en'        => 'Adobe Media',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES320',
            'name_en'        => 'Graphic Design Software 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES360',
            'name_en'        => 'Graphic Design Studio 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES370',
            'name_en'        => 'Graphic Design Software 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES410',
            'name_en'        => 'Graphic Design Studio 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES420',
            'name_en'        => 'Web Page Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES425',
            'name_en'        => 'Animation 3-D',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES430',
            'name_en'        => 'Visual Perception',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES440',
            'name_en'        => 'Packaging',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES460',
            'name_en'        => 'Graphic Design Studio 4',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES480',
            'name_en'        => 'Professional Practice',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES495',
            'name_en'        => 'Graphic Design Senior Project',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $GDES_BATCH,

        ]);
        $courses = Course::where('batch', $GDES_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['GDES']);
        });
        ///////////////////////////
        ///  GDES - Graphic Design End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        /// IDES - Interior Design
        ///////////////////////////
        $course = Course::create([
            'code'           => 'ARTS200',
            'name_en'        => 'Foundation Drawing 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS205',
            'name_en'        => 'Geometry for the Studio',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS210',
            'name_en'        => 'Color Theory - Painting',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS250',
            'name_en'        => 'Foundation Drawing 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS260',
            'name_en'        => 'Design Fundamentals - 2D',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS270',
            'name_en'        => 'Design Fundamentals - 3D',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS280',
            'name_en'        => 'History of Ancient Art',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS320',
            'name_en'        => 'Rendering and Perspective Techniques',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS330',
            'name_en'        => 'History of Modern Art',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'ARTS350',
            'name_en'        => 'Photography',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES320',
            'name_en'        => 'Graphic Design Software 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'GDES425A',
            'name_en'        => 'Adobe Media',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES300',
            'name_en'        => 'Architectural Drawing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES310',
            'name_en'        => 'Materials - Techniques and Building Codes',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES330',
            'name_en'        => 'CAD 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'IDES340',
            'name_en'        => 'Interior Design Studio 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES430A',
            'name_en'        => 'Computer Rendering 2 - 3D max 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES360',
            'name_en'        => 'Lighting and HVAC',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES370',
            'name_en'        => 'Furniture History and Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES380',
            'name_en'        => 'CAD 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES390',
            'name_en'        => 'Interior Design Studio 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES400',
            'name_en'        => 'Architectural Modeling',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES420',
            'name_en'        => 'Landscape Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES430',
            'name_en'        => 'Computer Rendering for Interior Design',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES440',
            'name_en'        => 'Interior Design Studio 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES460',
            'name_en'        => 'Interior Design Studio 4',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES480',
            'name_en'        => 'Professional Practice',
            'credits'        => 2,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $course = Course::create([
            'code'           => 'IDES495',
            'name_en'        => 'Interior Design Senior Project',
            'credits'        => 4,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $IDES_BATCH,

        ]);
        $courses = Course::where('batch', $IDES_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['IDES']);
        });
        ///////////////////////////
        ///  IDES - Interior Design  End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        /// TEFL - Begin
        ///////////////////////////
        $course = Course::create([
            'code'           => 'EDIT250',
            'name_en'        => 'Educational Technology for Teachers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC205',
            'name_en'        => 'Language Acquisition',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC310',
            'name_en'        => 'Achievement Motivation',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC320',
            'name_en'        => 'Teaching of Reading for Elementary School',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC350',
            'name_en'        => 'Methods of Teaching',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC351',
            'name_en'        => 'Methodology 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC365',
            'name_en'        => 'Learners Psychology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC370',
            'name_en'        => 'Teaching Oral Communication',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC375',
            'name_en'        => 'Semantics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC405',
            'name_en'        => 'Student Teaching Practicum 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC430',
            'name_en'        => 'Teaching Writing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC485',
            'name_en'        => 'Student Teaching Practicum 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL206',
            'name_en'        => 'English Listening and Speaking Skills',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL207',
            'name_en'        => 'English Reading Skills',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL207A',
            'name_en'        => 'Intermediate English Reading Skills',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL207B',
            'name_en'        => 'Advanced English Reading Skills',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL215',
            'name_en'        => 'Pre-Intermediate English Grammar',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL351',
            'name_en'        => 'Sociolinguistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC420',
            'name_en'        => 'Morphology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC450',
            'name_en'        => 'Classroom Management',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL205',
            'name_en'        => 'Introduction to English Literature',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL220',
            'name_en'        => 'Modern English Grammar',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL230',
            'name_en'        => 'English Syntax for TEFL Teachers',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL330',
            'name_en'        => 'Advanced English Grammar',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL360',
            'name_en'        => 'Introduction to Linguistics',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL410',
            'name_en'        => 'Reading in World Literature',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL430',
            'name_en'        => 'History of English Language',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL440',
            'name_en'        => 'English Phonology',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ELC101',
            'name_en'        => 'English Remedial Level 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ELC102',
            'name_en'        => 'English Remedial Level 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ELC103',
            'name_en'        => 'English Remedial Level 3',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC260',
            'name_en'        => 'Children Literature',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC360',
            'name_en'        => 'Language Testing',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'EDUC435',
            'name_en'        => 'Teaching Grammar communicatively',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL206A',
            'name_en'        => 'Integrated English Skills',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL209',
            'name_en'        => 'English Short Stories',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL210',
            'name_en'        => 'American Literature 1',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $course = Course::create([
            'code'           => 'ENGL310',
            'name_en'        => 'American Literature 2',
            'credits'        => 3,
            'course_type_id' => $COURSE_TYPE_OTHER,
            'batch'          => $TEFL_BATCH,
        ]);
        $courses = Course::where('batch', $TEFL_BATCH)->each(function ($c) use ($majors) {
            $c->majors()->attach($majors['TEFL']);
        });

        ///////////////////////////
        ///  TEFL  End //////////////////////////////
        /// ///////////////////////

        ///////////////////////////
        ///  MBA Begin //////////////////////////////
        /// ///////////////////////


        ///////////////////////////
        ///  MBA  End //////////////////////////////
        /// ///////////////////////
        $this->command->info("Courses table seeded!");
        $this->command->info("Courses <=> Majors table seeded!");
    }
}
