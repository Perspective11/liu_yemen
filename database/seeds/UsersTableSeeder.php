<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env('DEV_SEEDS', false)) {
            factory(App\User::class, 20)->create();

        }
        User::Create([
            'name' => 'Aiman Ali',
            'email' => 'aiman.noman@otekit.com',
            'password' => bcrypt('PassCode123'),
        ]);
        User::Create([
            'name' => 'Abdulwahab Atif',
            'email' => 'abdulwahab.atif@ye.liu.edu.lb',
            'password' => bcrypt('liu_admin_abdulwahab'),
        ]);
        $this->command->info("Users table seeded!");
    }
}
