<?php

use Faker\Generator as Faker;

$faker = \Faker\Factory::create('en_US');
$faker_ar = \Faker\Factory::create('ar_SA');

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

function autoIncrement($init = 0)
{
    for ($i = $init; $i < 1000; $i++) {
        yield $i;
    }
}

$factory->define(App\User::class, function (\Faker\Generator $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Contact::class, function (\Faker\Generator $faker) {
    return [
        'name'       => $faker->name,
        'email'      => $faker->email,
        'subject'    => $faker->optional()->words(3, true),
        'message'    => $faker->paragraphs(2, true),
        'created_at' => $faker->dateTimeThisMonth,
    ];
});

$factory->define(App\Application::class, function (\Faker\Generator $faker) {
    return [
        'name'               => $faker->name,
        'email'              => $faker->email,
        'level_of_education' => $faker->optional()->words(3, true),
        'field_of_study'     => $faker->optional()->jobTitle,
        'notes'              => $faker->optional()->sentence,
        'resume'             => $faker->url,
        'created_at'         => $faker->dateTimeThisMonth,
    ];
});
$eventIncrement = autoIncrement();

$factory->define(App\Event::class, function (\Faker\Generator $faker) use ($eventIncrement) {
    $faker = \Faker\Factory::create('en_US');
    $faker_ar = \Faker\Factory::create('ar_SA');
    $eventIncrement->next();

    return [
        'club_id'     => $faker->optional(0.9)->numberBetween(1, 10),
        'title_en'    => $faker->realText(100),
        'title_ar'    => $faker_ar->realText(100),
        'body_en'     => $faker->realText(1000),
        'body_ar'     => $faker_ar->realText(1000),
        'location_en' => $faker->optional()->words(3, true),
        'location_ar' => $faker_ar->optional()->realText(25),
        'date'        => $faker->dateTimeBetween('-6 months','3 months'),
        'status'      => (int) $faker->boolean(90),
        'type'       => $faker->numberBetween(0, 1),
        'created_at'  => $faker->dateTimeThisMonth,
        'poster_path' => '/images/events/poster_'.$eventIncrement->current().'.jpg',
    ];
});

$postIncrement = autoIncrement(8);
$factory->define(App\Post::class, function (\Faker\Generator $faker) use ($postIncrement) {
    $faker = \Faker\Factory::create('en_US');
    $faker_ar = \Faker\Factory::create('ar_SA');
    $postIncrement->next();

    return [
        'title_en'   => $faker->realText(100),
        'title_ar'   => $faker_ar->realText(100),
        'body_en'    => $faker->realText(1000),
        'body_ar'    => $faker_ar->realText(1000),
        'status'     => (int) $faker->boolean(90),
        'type'       => $faker->numberBetween(0, 4),
        'created_at' => $faker->dateTimeBetween('-6 months'),
        'picture'    => '/images/posts/post_'.$postIncrement->current().'.jpg',
    ];
});

$courseIncrement = autoIncrement();
$factory->define(App\Course::class, function (\Faker\Generator $faker) use ($courseIncrement) {
    $faker = \Faker\Factory::create('en_US');
    $faker_ar = \Faker\Factory::create('ar_SA');
    $courseIncrement->next();

    $courseTypesArray = \App\CourseType::pluck('id')->toArray();
    return [
        'code'           => $faker->safeHexColor,
        'name_en'        => $faker->realText(30),
        'name_ar'        => $faker_ar->optional()->realText(30),
        'credits'        => $faker_ar->optional()->numberBetween(1, 5),
        "course_type_id" => $faker->randomElement($courseTypesArray),
        "order_number"   => $courseIncrement->current(),
    ];
});

$pictureIncrement = autoIncrement();

$factory->define(App\Picture::class, function (\Faker\Generator $faker) use ($pictureIncrement) {
    $faker = \Faker\Factory::create('en_US');
    $faker_ar = \Faker\Factory::create('ar_SA');
    $pictureIncrement->next();

    return [
        'name'       => $faker->words(3, true),
        'image_path' => '/images/pictures/picture_'.$pictureIncrement->current().'.jpg',
        'thumb_path' => '/images/pictures/picture_'.$pictureIncrement->current().'.jpg',
        'created_at' => $faker->dateTimeThisMonth,
        'temp_uuid'  => $faker->uuid,
    ];
});


