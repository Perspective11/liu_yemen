<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content_category')->nullable()->index();
            $table->string('key')->unique()->index();
            $table->string('name_en')->nullable();
            $table->string('name_ar')->nullable();
            $table->text('value_en');
            $table->text('value_ar')->nullable();
            $table->integer('type')->default(0); // 0 for normal string 1 for WYSIWYG
            $table->integer('max_char')->default(500); // max string length
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
