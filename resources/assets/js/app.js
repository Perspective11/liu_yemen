
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(function() {
	$('[data-bg]').each(function (e){
		$(this).css('background-image', 'url(' + $(this).data('bg') + ')');
		if($(this).attr('data-bgsize')){
            $(this).css('background-size', $(this).attr('data-bgsize'));
        }
        if($(this).attr('data-bgposition')){
            $(this).css('background-position', $(this).attr('data-bgposition'));
        }
        if($(this).attr('data-bgrepeat')){
            $(this).css('background-repeat', $(this).attr('data-bgrepeat'));
        }
        if($(this).attr('data-bgattachment')){
            $(this).css('background-attachment', $(this).attr('data-bgattachment'));
        }
		})
    $('[data-bgs]').each(function (e){
        $(this).css('background', $(this).data('bgs'));
    })
});
