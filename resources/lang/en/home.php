<?php

return [
    'read-more'         => 'Read More',
    'continue-reading'  => 'Continue Reading',
    'contact'           => 'Contact Us',
    'get-details'       => 'See Details',
    'search-for-course' => 'Search For a Course',
    'course-name'       => 'Course Name',
    'all-courses'       => 'All Courses',
    'call-us'           => 'Call Us',
    'about-liu-yemen'   => 'About LIU Yemen',
    'latest-events'     => 'Latest Events',
    'all-events'        => 'All Events',
    'our-blog'          => 'Our Blog',
    'all-blog'          => 'All Blog',
    'our-gallery'       => 'Check Our Gallery',
    'become-a-teacher'  => 'Become a Teacher',
    'share'             => 'Share',
    'tweet'             => 'Tweet',
    'left'              => 'left',
    'right'             => 'right',
];
