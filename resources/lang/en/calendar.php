<?php

return [
    'calendar'            => 'Calendar',
    'events-calendar'     => 'Events Calendar',
    'view-all-events'     => 'View All Evens',
    'no-events-available' => 'No Events Available',
    'club'                => 'Club',
    'location'            => 'Location',
];
