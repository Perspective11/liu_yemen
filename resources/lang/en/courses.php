<?php

return [
    'courses'              => 'Courses',
    'courses-with-majors'  => 'Courses With Majors',
    'view-all-courses'     => 'View All Courses',
    'all'                  => 'All',
    'all-courses'          => 'All Courses',
    'general-electives'    => 'General Electives',
    'general-requirements' => 'General Requirements',
    'no-courses-available' => 'No Courses Available',
    'sort'                 => 'Sort',
    'majors'               => 'Majors',
    'select-a-major'       => 'Select A Major',
    'asc'                  => 'Ascending',
    'desc'                 => 'Descending',
    'code'                 => 'Code',
    'name'                 => 'Name',
    'credits'              => 'Credits',
    'type'                 => 'Type',
];
