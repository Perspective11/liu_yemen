<?php

return [
    'announcements'        => 'Announcements',
    'search-here'          => 'Search Here',
    'recent-announcements' => 'Recent Announcement',
    'archive'              => 'Archive',
    'filters'              => 'Filters',
    'date'                 => 'Date',
];
