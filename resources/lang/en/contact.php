<?php

return [
    'your_name'          => 'Name',
    'your_email'         => 'Email Address',
    'subject'            => 'Subject',
    'your_message'       => 'Message',
    'send_message'       => 'Send',
    'leave_us_a_message' => 'Leave Us a Message',
    'dont_hesitate'      => 'Don\'t Hesitate to Give Us a Call',
    'find_us'            => 'You Will Also Find Us With',
    'google_maps'        => 'And this is our google maps location',
    'address'            => 'Address',
    'phone'              => 'Phone',
    'notif_sent'         => 'Contact Form Was Sent!',
    'notif_not_sent'     => 'Contact Form Was NOT Sent!',
];
