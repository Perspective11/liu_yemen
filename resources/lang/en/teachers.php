<?php

return [
    'teaching-staff'       => 'Teaching Staff',
    'our-full-time'        => 'Our Full-time Instructors',
    'become-an-instructor' => 'Become an LIU Instructor',
    'fill-in-your-info'    => 'Fill In Your Info',
    'level-of-education'   => 'Level Of Education',
    'filed-of-study'       => 'Field Of Study',
    'notes'                => 'Notes',
    'upload-your-cv'       => 'Upload Your CV:',
    'submit'               => 'Submit',
    'notif_sent'           => 'Application Form Was Sent!',
    'notif_not_sent'       => 'Application Form Was NOT Sent!',
];
