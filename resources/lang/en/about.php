<?php

return [
    'about-us'                    => 'About Us',
    'what-we-offer'               => 'What We Offer',
    'what-people-say'             => 'What People Say About Us',
    'meet-our-instructors'        => 'Meet Our Instructors',
    'view-departments-and-majors' => 'View Departments and Majors',
];
