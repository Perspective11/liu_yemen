<?php

return [
    'blog'         => 'Blog',
    'search-here'  => 'Search Here',
    'recent-posts' => 'Recent Post',
    'archive'      => 'Archive',
    'filters'      => 'Filters',
    'date'         => 'Date',
    'search'       => 'Search',
    'search-btn'   => 'Go!',
];
