<?php

return [
    'announcements'      => 'Announcements',
    'liu'                => 'Lebanese International University',
    'links'              => 'Links',
    'contact'            => 'Contact',
    'copyright'          => '2018 © All Rights Reserved ',
    'otek'               => 'OteK Solutions',
    'designed-by'        => 'Built and Designed By',
    'credits'            => 'Credits',
    'privacy-policy'     => 'Privacy Policy',
    'feedback'           => 'Feedback',
    'credits-sub'        => 'A thank you note to the contributors',
    'feedback-sub'       => 'Send us your suggestions or complaints',
];
