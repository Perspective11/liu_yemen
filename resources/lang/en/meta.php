<?php

return [
    'website-title'     => 'LIU Yemen',
    'website-sub-title' => 'An official introductory portal to everything about Lebanese International University in Yemen, Sana\'a.',
];
