<?php

return [
    'events'        => 'Events',
    'event-poster'  => 'Event Poster',
    'search-here'   => 'Search Here',
    'recent-events' => 'Recent Events',
    'archive'       => 'Archive',
    'clubs'         => 'Clubs',
    'filters'       => 'Filters',
    'club'          => 'Club',
    'date'          => 'Date',
    'time'          => 'Time',
    'upcoming'      => 'Upcoming',
    'past'          => 'Past',
];
