<?php

return [
    'your_name'          => 'أسمك',
    'your_email'         => 'البريد الألكتروني',
    'subject'            => 'الموضوع',
    'your_message'       => 'الرسالة',
    'send_message'       => 'أرسل الرسالة',
    'leave_us_a_message' => 'اترك لنا رسالة',
    'dont_hesitate'      => 'لا تتردد في التواصل معنا',
    'find_us'            => 'سوف تجدنا ايضا في',
    'google_maps'        => 'وهذا موقعنا في خرائط غوغل!',
    'address'            => 'العنوان',
    'phone'              => 'الهاتف',
    'notif_sent'         => 'تم الارسال بنجاح!',
    'notif_not_sent'     => 'لم يتم الارسال!',
];
