<?php

return [
    'announcements'        => 'اشعارات',
    'search-here'          => 'ابحث هنا',
    'recent-announcements' => 'احدث اشعارات',
    'archive'              => 'ارشيف',
    'filters'              => 'بحث بحسب',
    'date'                 => 'التاريخ',
];
