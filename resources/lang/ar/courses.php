<?php

return [
    'courses'              => 'المواد',
    'courses-with-majors'  => 'المواد مع التخصصات',
    'view-all-courses'     => 'عرض جميع المواد',
    'all'                  => 'الجميع',
    'all-courses'          => 'جميع المواد',
    'general-electives'    => 'اختياري عام',
    'general-requirements' => 'متطلب عام',
    'no-courses-available' => 'لا توجد مواد',
    'sort'                 => 'ترتيب',
    'majors'               => 'التخصصات',
    'select-a-major'       => 'اختر التخصص',
    'asc'                  => 'تصاعديا',
    'desc'                 => 'تناليا',
    'code'                 => 'الكود',
    'name'                 => 'الاسم',
    'credits'              => 'النقاط',
    'type'                 => 'النوع',
];
