<?php

return [
    'events'        => 'الفعاليات',
    'event-poster'  => 'ملصق الفعالية',
    'search-here'   => 'ابحث هنا',
    'recent-events' => 'اخر الفعاليات',
    'archive'       => 'الأرشيف',
    'clubs'         => 'النادي',
    'filters'       => 'حصر النتائج',
    'club'          => 'النادي',
    'date'          => 'التاريخ',
    'time'          => 'الوقت',
    'upcoming'      => 'قادم',
    'past'          => 'مضى',
];
