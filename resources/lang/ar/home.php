<?php

return [
    'read-more'         => 'إقرأ المزيد',
    'continue-reading'  => 'تابع القراءة',
    'contact'           => 'تواصل بنا',
    'get-details'       => 'تفاصيل اكثر',
    'search-for-course' => 'ابحث عن كورس',
    'course-name'       => 'اسم الكورس',
    'all-courses'       => 'جميع الكورسات',
    'call-us'           => 'إتصل بنا',
    'about-liu-yemen'   => 'عن الجامعة اللبنانية في اليمن',
    'latest-events'     => 'اخر الفعاليات',
    'all-events'        => 'جميع الفعاليات',
    'our-blog'          => 'مدونتنا',
    'all-blog'          => 'كل المدونة',
    'our-gallery'       => 'القي نظرة على المعرض',
    'become-a-teacher'  => 'كن مدرساً معنا',
    'share'             => 'مشاركة',
    'tweet'             => 'تغريد',
    'left'              => 'right',
    'right'             => 'left',
];
