<?php

return [
    'announcements'      => 'اخطارات',
    'liu'                => 'الجامعة اللبنانية الدولية',
    'links'              => 'انتقال',
    'contact'            => 'تواصل',
    'copyright'          => '2018 © جميع الحقوق محفوظة',
    'otek'               => 'شركة اوتِك',
    'designed-by'        => 'تطوير وتصميم',
    'credits'            => 'شكر وعرفان',
    'privacy-policy'     => 'اتفاقية الخصوصية',
    'feedback'           => 'تقييم',
    'credits-sub'        => 'كلمة شكر للمساهمين',
    'feedback-sub'       => 'ارسل لنا اقتراح او شكوى',
];
