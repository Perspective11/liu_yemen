<?php

return [
    'calendar'            => 'التقويم',
    'events-calendar'     => 'تقويم الفعاليات',
    'view-all-events'     => 'عرض جميع الفعاليات',
    'no-events-available' => 'لا توجد فعاليات',
    'club'                => 'النادي',
    'location'            => 'الموقع',
];
