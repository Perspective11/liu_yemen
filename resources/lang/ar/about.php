<?php

return [
    'about-us'                    => 'عنا',
    'what-we-offer'               => 'الخدمات اللتي نوفرها',
    'what-people-say'             => 'ماذا يُقال عنا',
    'meet-our-instructors'        => 'قابل مدرسينا',
    'view-departments-and-majors' => 'عرض التخصصات والاقسام',
];
