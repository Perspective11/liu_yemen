<?php

return [
    'teaching-staff'       => 'طاقم التدريس',
    'our-full-time'        => 'مدريسن ذو الدوام الكامل',
    'become-an-instructor' => 'انظم الى طاقم مدرسين الجامعة اللبنانية',
    'fill-in-your-info'    => 'ادخل بياناتك',
    'level-of-education'   => 'المستوى التعليمي',
    'filed-of-study'       => 'التخصص',
    'notes'                => 'ملاحظلات',
    'upload-your-cv'       => 'ارفق سيرتك الذاتية:',
    'submit'               => 'ادراج',
    'notif_sent'           => 'تم الارسال بنجاح!',
    'notif_not_sent'       => 'لم يتم الارسال!',
];
