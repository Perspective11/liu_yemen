<header class="header-section">
    <div class="top-bar hidden-sm hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-left-bar">
                        <ul class="contact-wrapper">
                            <li><i class="fa fa-phone" aria-hidden="true"></i> {{ $appSettings['nav_phone'] }} </li>
                            <li><i class="fa fa-envelope" aria-hidden="true"></i> {{ $appSettings['nav_email'] }}</li>
                        </ul>
                    </div> <!-- header-left-bar -->
                </div>

                <div class="col-sm-6">
                    <div class="header-right-bar text-right">
                        <a href="{{ LaravelLocalization::getLocalizedURL(__('nav.lang-locale')) }}
                                " class="country">@lang('nav.lang-string')</a>

                        <ul class="social-icon inline-block">
                            @foreach (\App\Setting::links() as $social)
                                <li><a href="{{ $social['link'] }}"><i class="fa fa-{{ $social['icon'] }}" aria-hidden="true"></i></a></li>
                            @endforeach
                        </ul>

                        <ul class="access inline-block">
                            <li><a href="http://liuyemen.com" data-toggle="tooltip" title="@lang('nav.tooltip-registration')">Student Login</a></li>
                        </ul>
                    </div> <!-- header-right-bar -->
                </div>
            </div> <!-- top-bar -->
        </div>
    </div> <!-- top-bar -->

    <nav class="navbar navbar-inverse hidden-sm hidden-xs">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/"><img src="/images/logo.jpg" alt="image"></a>
            </div>

            <div class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    @php
                    $path = request()->getPathInfo();
                    @endphp
                    <li class="{{ MyHelper::urlActive('/', $path, true)}}"><a href="/">@lang('nav.home')</a>
                    </li>

                    <li class="dropdown {{ MyHelper::urlActive(['/about', '/campus-map', '/contact'], $path)}}"><a href="javascript:void(0)">@lang('nav.about-us') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li class="{{ MyHelper::urlActive('/about', $path)}}"><a href="/about">@lang('nav.introduction')</a></li>
                            <li class="{{ MyHelper::urlActive('/campus-map', $path)}}"><a href="/campus-map">@lang('nav.campus-map')</a></li>
                            <li class="{{ MyHelper::urlActive('/contact', $path)}}"><a href="/contact">@lang('nav.contact-info')</a></li>
                        </ul>
                    </li>

                    <li class="dropdown {{ MyHelper::urlActive(['/majors', '/courses', '/teachers', '/teachers/create'], $path) }}"><a href="javascript:void(0)">@lang('nav.academic') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li class="{{ MyHelper::urlActive('/admission', $path)}}"><a href="/admission">@lang('nav.admission')</a></li>
                            <li class="{{ MyHelper::urlActive('/majors', $path)}}"><a href="/majors">@lang('nav.majors')</a></li>
                            @if ($appSettings['view_courses'] == 'true')
                                <li class="{{ MyHelper::urlActive('/courses', $path)}}"><a href="/courses">@lang('nav.courses')</a></li>
                            @endif
                            <li class="{{ MyHelper::urlActive('/teachers', $path, true)}}"><a href="/teachers">@lang('nav.teachers')</a></li>
                            @if ($appSettings['view_job_apps'] == 'true')
                                <li class="{{ MyHelper::urlActive('/teachers/create', $path)}}"><a href="/teachers/create">@lang('nav.apply')</a></li>
                            @endif
                        </ul>
                    </li>

                    <li class="{{ MyHelper::urlActive('/facilities', $path)}}"><a href="/facilities">@lang('nav.facilities')</a></li>

                    <li class="{{ MyHelper::urlActive('/gallery', $path)}}"><a href="/gallery">@lang('nav.gallery')</a></li>
                    <li class="dropdown {{ MyHelper::urlActive(['/events', '/calendar'], $path)}}"><a href="javascript:void(0)">@lang('nav.student-center') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li class="{{ MyHelper::urlActive('/events', $path)}}"><a href="/events">@lang('nav.events')</a></li>
                            <li class="{{ MyHelper::urlActive('/calendar', $path)}}"><a href="/calendar">@lang('nav.calendar')</a></li>
                        </ul>
                    </li>

                    <li class="dropdown {{ MyHelper::urlActive(['/blog', '/announcements'], $path)}}"><a href="javascript:void(0)">@lang('nav.blog') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li class="{{ MyHelper::urlActive('/blog', $path)}}"><a href="/blog">@lang('nav.posts')</a></li>
                            <li class="{{ MyHelper::urlActive('/announcements', $path)}}"><a href="/announcements">@lang('nav.announcements')</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div> <!-- container -->
    </nav>

    <div class="navbar-header inline-block">
        <a class="navbar-brand" href="index.html"><img src="/images/logo_side.jpg" alt="image"></a>
    </div>
</header> <!-- header-section -->