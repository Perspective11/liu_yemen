<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'LIU Yemen') }}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="OteK">
<meta name="description" content="@lang('meta.website-sub-title')">
<meta property="og:title" content="@lang('meta.website-title')">
<meta property="og:description" content="@lang('meta.website-sub-title') {{ env('APP_URL') . '/' . $lang }}">
<meta property="og:image" content="{{ env('APP_URL') . '/images/site_intro_' . $lang . '.jpg'  }}"/>
<meta property="og:image:url" content="{{ env('APP_URL') . '/images/site_intro_' . $lang . '.jpg'  }}"/>
<meta property="og:image:secure_url" content="{{ env('APP_URL') . '/images/site_intro_' . $lang . '.jpg'  }}"/>
<meta property="og:image:type" content="image/jpeg"/>
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:title" content="@lang('meta.website-title')">
<meta property="twitter:description" content="@lang('meta.website-sub-title')">
<meta property="twitter:image" content="{{ env('APP_URL') . '/images/site_intro_' . $lang . '.jpg'  }}">
<meta name="language" content="{{ $lang }}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<!-- animated-css -->
{{--<link href="/css/animate.min.css" rel="stylesheet" type="text/css">--}}
<!-- font-awesome-css -->
{{--<link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">--}}

@if ($lang == 'ar')
    <style>
        @import url(//fonts.googleapis.com/earlyaccess/droidarabickufi.css);
    </style>
@endif


<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<script src="{{ asset('js/app.js') }}"></script>