@php
    $path = request()->getPathInfo();
@endphp
<!-- Off-Canvas View Only -->
<span class="menu-toggle navbar visible-xs visible-sm"><i class="fa fa-bars" aria-hidden="true"></i></span>

<div id="offcanvas-menu" class="visible-xs visible-sm">

    <span class="close-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
    <a href="{{ LaravelLocalization::getLocalizedURL(__('nav.lang-locale')) }}" class="country">@lang('nav.lang-string')</a>
    <div class="clearfix"></div>
    <ul class="menu-wrapper">
        <li>
            <a class="{{ MyHelper::urlActive('/', $path, true)}}" href="/">@lang('nav.home')</a>
        </li><!-- end of li -->

        <li>
            <a class="dropmenu {{ MyHelper::urlActive(['/about', '/campus-map', '/contact'], $path)}}" href="#">@lang('nav.about-us') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            <ul class="dropDown sub-menu">
                <li><a class="{{ MyHelper::urlActive('/about', $path)}}" href="/about">@lang('nav.introduction')</a></li>
                <li><a class="{{ MyHelper::urlActive('/campus-map', $path)}}" href="/campus-map">@lang('nav.campus-map')</a></li>
                <li><a class="{{ MyHelper::urlActive('/contact', $path)}}" href="/contact">@lang('nav.contact-info')</a></li>
            </ul><!-- end of dropdown -->
        </li><!-- end of li -->


        <li>
            <a class="dropmenu {{ MyHelper::urlActive(['/majors', '/courses', '/teachers', '/teachers/create'], $path) }}" href="#">@lang('nav.academic') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            <ul class="dropDown sub-menu">
                <li><a class="{{ MyHelper::urlActive('/majors', $path)}}" href="/majors">@lang('nav.majors')</a></li>
                @if ($appSettings['view_courses'] == 'true')
                    <li><a class="{{ MyHelper::urlActive('/courses', $path)}}" href="/courses">@lang('nav.courses')</a></li>
                @endif
                <li><a class="{{ MyHelper::urlActive('/teachers', $path, true)}}" href="/teachers">@lang('nav.teachers')</a></li>
                @if ($appSettings['view_job_apps'] == 'true')
                    <li><a class="{{ MyHelper::urlActive('/teachers/create', $path)}}" href="/teachers/create">@lang('nav.apply')</a></li>
                @endif
            </ul><!-- end of dropdown -->
        </li><!-- end of li -->

        <li><a class="{{ MyHelper::urlActive('/facilities', $path)}}" href="/facilities">@lang('nav.facilities')</a></li>
        <li><a class="{{ MyHelper::urlActive('/gallery', $path)}}" href="/gallery">@lang('nav.gallery')</a></li>

        <li>
            <a class="dropmenu {{ MyHelper::urlActive(['/events', '/calendar'], $path)}}" href="#">@lang('nav.student-center') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            <ul class="dropDown sub-menu">
                <li><a class="{{ MyHelper::urlActive('/events', $path)}}" href="/events">@lang('nav.events')</a></li>
                <li><a class="{{ MyHelper::urlActive('/calendar', $path)}}" href="/calendar">@lang('nav.calendar')</a></li>
            </ul><!-- end of dropdown -->
        </li><!-- end of li -->

        <li>
            <a class="dropmenu {{ MyHelper::urlActive(['/blog', '/announcements'], $path)}}" href="#">@lang('nav.blog') <i class="fa fa-angle-down" aria-hidden="true"></i></a>
            <ul class="dropDown sub-menu">
                <li><a class="{{ MyHelper::urlActive('/blog', $path)}}" href="/blog">@lang('nav.posts')</a></li>
                <li><a class="{{ MyHelper::urlActive('/announcements', $path)}}" href="/announcements">@lang('nav.announcements')</a></li>
            </ul><!-- end of dropdown -->
        </li><!-- end of li -->
    </ul> <!-- menu-wrapper -->
</div>
<!-- Off-Canvas View Only -->
