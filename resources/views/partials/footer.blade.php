<footer class="footer-section" data-bg="{{ $images['footer_section']->image_path }}">
    <div class="footer-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <div class="footer-wrapper">
                        <span class="footer-logo">
                            <img src="/images/logo.png" alt="image">
                            <h2>@lang('footer.liu')</h2>
                        </span>

                        <p>{{ $contents['footer_about_liu']['value_' . $lang] }}</p>

                        <ul class="social-icon">
                            @foreach (\App\Setting::links() as $social)
                                <li><a href="{{ $social['link'] }}"><i class="fa fa-{{ $social['icon'] }}" aria-hidden="true"></i></a></li>
                            @endforeach
                        </ul> <!-- social-icon -->
                    </div> <!-- footer-wrapper -->
                </div>

                <div class="col-sm-9 col-md-4">
                    <div class="footer-wrapper ml-30">
                        <h3>@lang('footer.announcements')</h3>
                        <div class="media">
                            @foreach ($footerAnnouncements as $announcement)
                                <div class="media-wrapper">
                                    <span class="caption pull-left"><img src="{{ $announcement->picture }}" alt=""></span>

                                    <div class="wrapper-content">
                                        <h5><a href="{{ $announcement->path() }}">{{ str_limit( $announcement['title_' . $lang], 70)  }}</a></h5>

                                        <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $announcement->created_at->diffForHumans() }}</span>
                                    </div>
                                </div> <!-- wrapper-option -->
                            @endforeach
                        </div> <!-- media -->
                    </div> <!-- footer-wrapper -->
                </div>

                <div class="col-sm-3 col-md-2">
                    <div class="footer-wrapper last-wrapper">
                        <h3>@lang('footer.links')</h3>

                        <ul class="wrapper-option">
                            <li><a href="/">@lang('nav.home')</a></li>
                            <li><a href="/about">@lang('nav.about-us')</a></li>
                            <li><a href="/contact">@lang('footer.contact')</a></li>
                            <li><a href="/courses">@lang('nav.courses')</a></li>
                            <li><a href="/majors">@lang('nav.majors')</a></li>
                            <li><a href="/gallery">@lang('nav.gallery')</a></li>
                        </ul> <!-- wrapper-option -->
                    </div> <!-- footer-wrapper -->
                </div>

                <div class="col-sm-12 col-md-3">
                    <div class="footer-wrapper last-wrapper">
                        <h3>@lang('footer.contact')</h3>

                        <form class="subscribeForm" method="post" action="/{{ $lang }}/store-contact">
                            {{ csrf_field() }}
                            <div class="form-group text-left {{ $errors->has('footer_name') ? ' has-error' : '' }}">
                                <input name="name" type="text" class="form-control" id="inputName11" placeholder="@lang('contact.your_name')" value="{{ old('footer_name') }}">
                            </div>

                            <div class="form-group text-left email-field {{ $errors->has('footer_email') ? ' has-error' : '' }}">
                                <input name="email" class="footerSearchBar" placeholder="@lang('contact.your_email')"  value="{{ old('footer_email') }}" id="footerSearchBar" type="email">
                            </div>

                            <div class="form-group text-left {{ $errors->has('footer_message') ? ' has-error' : '' }}">
                                <textarea name="message" class="form-control textarea" rows="4" placeholder="@lang("contact.your_message")">{{ old('footer_message') }}</textarea>
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" name="footer_submit" class="subscribeBtn btn-primary">@lang("contact.send_message")</button>
                            </div>
                        </form>
                    </div> <!-- footer-wrapper -->
                    @include('errors.errors', ['error_message_bag' => 'footer'])
                </div>
            </div>
        </div>
    </div> <!-- footer-container -->

    <div class="copy-right">
        <div class="container">
            <ul class="inline-block">
                <li><a href="/credits">@lang('footer.credits')</a></li>
                <li><a href="/privacy-policy">@lang('footer.privacy-policy')</a></li>
            </ul>

            <p class="pull-right otek-p">
                @lang('footer.designed-by') <a target="_blank" href="otekit.com">@lang('footer.otek')</a>
                <a target="_blank"  class="oteklink" href="otekit.com">
                    <img id="oteklogo" src="/images/otek.png" alt="">
                </a>
            </p>
        </div>
    </div> <!-- copy-right -->
</footer> <!-- footer-section -->

