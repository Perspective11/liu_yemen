@extends('layouts.app')
@section('content')
    <section class="header-title" data-bg="{{ $images['gallery_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('gallery.gallery')</h2>
        </div>
    </section> <!-- header-title header-overlay -->

    <section class="gallery-section section-padding" data-bg="{{ $images['gallery_section']->image_path }}">
        <div class="container-fluid text-center">
            <div class="section-title text-body-center">
                <h2>@lang('gallery.here-are-some')</h2>
                <p>{{ $contents['teachers_subtitle_desc']['value_' . $lang] }}</p>
            </div> <!-- section-title -->

            <div class="portfolio gallery-grid">
                <div class="row">
                    <ul class="portfolio-sorting gallery-button list-inline text-center">
                        <li><a href="#" data-group="all" class="filter-btn active">@lang('gallery.all')</a></li>
                        @foreach ($tags as $tag)
                            <li><a class="filter-btn" href="#" data-group="{{ 'tag' . $tag['id'] }}">{{ $tag['name_' . $lang] }}</a></li>
                        @endforeach
                    </ul> <!--end portfolio sorting -->
                    <div id="lightBox" class="gallery-wrapper">
                        <ul class="portfolio-items gallery-list courses list-unstyled" id="grid">
                            @foreach ($gallery as $picture)
                                <li class="col-xs-12 col-sm-6 col-md-4 col-lg-3 no-margin no-padding" data-groups='["{{ 'tag' . $picture['tag_id'] }}"]'>
                                    <figure class="portfolio-item gallery-caption">
                                        <img src="{{ $picture['thumb_path'] ?: $picture['image_path'] }}" alt="{{ $picture['name'] }}">

                                        <div class="hover-view"><a href="{{ $picture['image_path'] }}"><i class="fa fa-search-plus"></i></a></div>
                                    </figure>
                                </li>
                            @endforeach
                        </ul> <!--end portfolio grid -->
                    </div> <!-- gallery-wrapper -->
                </div> <!--end row -->
            </div>
        </div> <!-- container -->
    </section> <!-- gallery-section -->
@endsection