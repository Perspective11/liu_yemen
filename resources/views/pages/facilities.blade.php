@extends('layouts.app')
@section('top-ex')
    <!-- owl-carrosel-css -->
    <link href="/plugins/owl-carrosel/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="/plugins/owl-carrosel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <section class="header-title" data-bg="{{ $images['facilities_header_title']->image_path }}" >
        <div class="container">
            <h2 class="title">@lang('facilities.facilities')</h2>
        </div>
    </section> <!-- header-title header-overlay -->

    <section class="facil-section faculty-section section-padding" data-bg="{{ $images['facilities_facil_section']->image_path }}">
        <div class="container">
            @foreach ($facilities as $facility)
                <div class="faculty-container">
                    <div class="container-fluid facil-title">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="title">
                                    <h3>{{ str_limit($facility['name_' . $lang], 30)  }}</h3>
                                </div>
                                <div class="sub-title"><p>{{ str_limit($facility['description_' . $lang], 100)}}</p></div>
                            </div>
                            <div class="col-md-8 col-sm-12 facil-bg" data-bg="{{ $facility->getPicture() }}">
                            </div>
                        </div>
                    </div>
                    @if ($facility->pictures()->count() > 1)
                        <div class="container-fluid facil-slider">
                            <div class="clearfix"></div>
                            <div class="lightbox subject-carousel owl-carousel owl-theme" data-dir="{{ $dir }}">
                                @foreach ($facility->pictures as $picture)
                                    @if (! $loop->index)
                                        @continue
                                    @endif
                                    <div class="item">
                                        <figure class="portfolio-item gallery-caption">
                                            <img src="{{ $picture->thumb_path }}" alt="">

                                            <div class="hover-view"><a href="{{ $picture->image_path }}"><i
                                                            class="fa fa-search-plus"></i></a></div>
                                        </figure>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div> <!-- faculty-container -->
            @endforeach
            <div class="pagination-wrapper row text-center">
                <div class="col-xs-12">
                    {{ $facilities->links() }}
                </div>
            </div>
        </div>


    </section> <!-- faculty-section -->


@endsection