@extends('layouts.app')
@section('content')
    <section class="header-title" data-bg="{{ $images['feedback_header']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('campus-map.title')</h2>
            <span class="sub-title">@lang('campus-map.subtitle')</span>
        </div>
    </section> <!-- header-title -->
@endsection