@extends('layouts.app')
@section('content')
    <section class="header-title mb-50" data-bg="{{ $images['privacy_policy_header']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('footer.privacy-policy')</h2>
        </div>
    </section> <!-- header-title -->
    <section class="mb-50">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="privacy-body ltr-body-left">
                        {!! $contents['privacy_policy']['value_' . $lang] !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection