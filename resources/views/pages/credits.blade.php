@extends('layouts.app')
@section('content')
    <section class="header-title mb-50 header-overlay" data-bg="{{ $images['credits_header']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('footer.credits')</h2>
            <span class="sub-title">@lang('footer.credits-sub')</span>
        </div>
    </section> <!-- header-title -->
    <section class="mb-50">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="credits-body rtl-right-body">
                        {!! $contents['credits']['value_' . $lang] !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection