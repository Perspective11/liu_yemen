@extends('layouts.app')
@section('content')
    <section class="header-title " style="background-size: contain; padding-bottom: 40%" data-bg="{{ $images['majors_header_title']->image_path }}">
    </section> <!-- header-title header-overlay -->



    <section class="department-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-content">
                        <h3>@lang('majors.our_departments')</h3>
                        <div class="text-body">{!! $contents['majors_departments_desc']['value_' . $lang] !!}</div>
                    </div> <!-- section-content -->
                </div>

                <div class="col-md-6">
                    <div id="department-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="{{ $images['majors_slide_1']->image_path }}" alt="">
                            </div>

                            <div class="item">
                                <img src="{{ $images['majors_slide_2']->image_path }}" alt="">
                            </div>
                        </div>

                        <a class="left carousel-control" href="#department-carousel" role="button" data-slide="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>

                        <a class="right carousel-control" href="#department-carousel" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div> <!-- department-carousel -->
                </div>
            </div>
        </div>
    </section> <!-- department-section -->



    <section class="faculty-section section-padding" data-bg="{{ $images['majors_faculty_section']->image_path }}">
        <div class="container">
            @for ($i = 0; $i < $departments->count(); $i++)
                @php
                    $department = $departments[$i];
                    $majorsCount = $department->majors()->count();
                    $majorsIncrement = 0;
                    $majorsLoop = $majorsCount >= 3? $majorsCount / 3 : 1;
                @endphp
                <div class="faculty-container">
                    <i class="dept-icon fa {{ $department['icon'] }}"></i>

                    <div class="row">
                    <div class="">
                        <div class="col-md-3 col-sm-6">
                            <span class="caption"><img src="{{ $department->picture }}" alt=""></span>
                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="title">
                                <h3>{{ $department['name_' . $lang] }}</h3>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <ul class="content-wrapper">
                                @for ($j = 0; $j < $majorsLoop; $j++)
                                <li>
                                    <ul>
                                        @for ($k = 0; $k < 3; $k++)
                                            @if ($majorsIncrement === $majorsCount)
                                                @break
                                            @endif
                                            <li><a href="{{ '/courses?major=' . $department->majors[$majorsIncrement]['name_en'] }}"><i class="fa fa-check" aria-hidden="true"></i> {{ $department->majors[$majorsIncrement]['name_' . $lang] }}</a></li>
                                            @php
                                                $majorsIncrement ++ ;
                                            @endphp
                                        @endfor
                                    </ul>
                                </li>
                                @endfor
                            </ul> <!-- content-wrapper -->
                        </div>
                    </div> <!-- faculty-wrapper -->
                </div>
            </div> <!-- faculty-container -->
            @endfor
        </div>
    </section> <!-- faculty-section -->
@endsection