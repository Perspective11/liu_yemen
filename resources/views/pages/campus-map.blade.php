@extends('layouts.app')
@section('content')
    <section class="campus-map header-title bg-p-left-h" data-bg="{{ $lang == 'ar' ? '/images/campus_map_ar.jpg' : '/images/campus_map.jpg'}}">
        <div class="container">
            <h2 class="title">@lang('campus-map.title')</h2>
            <span class="sub-title">@lang('campus-map.subtitle')</span>
        </div>
    </section> <!-- header-title -->
@endsection