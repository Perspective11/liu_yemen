@extends('layouts.app')
@section('content')
    <section class="header-title mb-50" data-bg="{{ $images['admission_header']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('nav.admission')</h2>
        </div>
    </section> <!-- header-title -->
    <section class="mb-50">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="credits-body rtl-right-body">
                        {!! $contents['admission']['value_' . $lang] !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection