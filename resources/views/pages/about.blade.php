@extends('layouts.app')
@section('content')

    <section class="header-title header-overlay" data-bg="{{ $images['about_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('about.about-us')</h2>
        </div>
    </section> <!-- header-title -->


    <section class="about-section section-padding">
        <div class="container text-center">
            <div class="section-title">
                <h2>@lang('about.what-we-offer')</h2>
                <p class="text-center">{{ $contents['about_what_we_offer_desc']['value_' . $lang] }}</p>
            </div> <!-- section-title -->

            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-4 col-sm-6">
                        <div class="about-wrapper">
                            <span class="caption"><img src="{{ $post->picture }}" alt="{{ $post->name_en }}"></span>

                            <div class="wrapper-content">
                                <h4>{{ $post['name_' . $lang] }}</h4>
                                <div class="post-body text-body text-body-center">{!! $post['body_' . $lang] !!}</div>

                                <a href="{{ $post->path() }}" class="btn btn-default">@lang('home.get-details')</a>
                            </div>
                        </div> <!-- about-wrapper -->
                    </div>
                @endforeach
            </div>
        </div>
    </section> <!-- about-section -->


    <section class="testimonial-section section-padding" data-bg="{{ $images['about_testimonials_section']->image_path }}">
        <div class="container-fluid text-center">
            <div class="section-title">
                <h2>@lang('about.what-people-say')</h2>
            </div> <!-- section-title -->

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="testimonial-tab">
                        <div class="tab-content">
                            @foreach($testimonials as $testimonial)
                                <div role="tabpanel" class="tab-pane fade in {{ $loop->index ? '':'active' }}" id="testimonial-{{ $testimonial->id }}">
                                    <span class="caption"><img src="/images/quote.png" alt=""></span>
                                    <p class="text-center">{{ $testimonial['quote_' . $lang] }}</p>
                                </div>
                            @endforeach
                        </div>

                        <ul class="nav nav-tabs" role="tablist">
                            @foreach ($testimonials as $testimonial)
                                <li role="presentation" class="{{ $loop->index ? '':'active' }}"><a href="#testimonial-{{ $testimonial->id }}" aria-controls="#testimonial-{{ $testimonial->id }}" role="tab" data-toggle="tab"><img src="{{ $testimonial->picture }}" alt=""></a></li>
                            @endforeach
                        </ul>
                    </div> <!-- testimonial-tab -->
                </div>
            </div>
        </div>
    </section> <!-- testimonial-section -->

    <section class="history-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="history-wrapper">
                        {!! $contents['about_body_large']['value_' . $lang] !!}
                    </div> <!-- history-wrapper -->
                </div>

                <div class="col-md-6">
                    <div class="text-center">
                        <img style="width: 100%; height: auto" src="{{ $images['about_history_img']->image_path }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- history-section -->


    <section class="app-section" data-bg="{{ $images['about_meet_teachers_section']->image_path }}">
        <div class="container text-center text-body-center">
            <h2>{{ $contents['about_we_offer_best_title']['value_' . $lang] }}</h2>
            <span class="sub-title">{{ $contents['about_we_offer_best_subtitle']['value_' . $lang] }}</span>

            <span class="caption"><a href="/teachers" class="btn btn-primary">@lang('about.meet-our-instructors')</a></span>
            <br>
            <span class="caption"><a href="/majors" class="btn btn-primary">@lang('about.view-departments-and-majors')</a></span>
        </div>
    </section> <!-- app-section -->



    <section class="comment-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 hidden-sm hidden-xs">
                    <div class="caption wow fadeInLeft"><img src="{{ $images['about_employee_quote']->image_path }}" alt=""></div>
                </div>

                <div class="col-md-7 col-md-offset-1">
                    <div class="wrapper-content">
                        <span class="quote"><img src="/images/quote-2.png" alt=""></span>
                        <div class="clear-fix"></div>
                        <div class="text-body">{!! $contents['about_quote_body']['value_' . $lang] !!}</div>

                        <h5>{{ $contents['about_quote_person']['value_' . $lang] }}</h5>
                        <span class="title">{{ $contents['about_quote_title']['value_' . $lang] }}</span>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- comment-section -->
@endsection