@extends('layouts.app')
@section('content')
    <section class="header-title header-overlay" data-bg="{{ $images['contact_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('home.contact')</h2>
        </div>
    </section> <!-- header-title header-overlay -->



    <section class="contact-section section-padding">
        <div class="container text-center text-body-center">
            <div class="section-title">
                <h2>@lang('contact.dont_hesitate')</h2>
                <p>{{ $contents['contact_subtitle_desc']['value_' . $lang] }}</p>
            </div> <!-- section-title -->

            <div class="container">
                <div class="row ">
                    <div class="col-sm-4">
                        <div class="contact-wrapper">
                            <span class="caption"><img src="/images/contact/i1.png" alt=""></span>
                            <h4>@lang('contact.address')</h4>
                            <span class="contact">{{ $contents['site_info_address']['value_' . $lang] }}</span>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="contact-wrapper ltr">
                            <span class="caption"><img src="/images/contact/i2.png" alt=""></span>
                            <h4>@lang('contact.your_email')</h4>
                            <span class="contact">{{ $appSettings['contact_email'] }}</span>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="contact-wrapper ltr">
                            <span class="caption"><img src="/images/contact/i3.png" alt=""></span>
                            <h4>@lang('contact.phone')</h4>
                            <span class="contact">{{ $appSettings['contact_phone'] }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- contact-section -->



    <section class="map-section" data-bg="{{ $images['contact_map_section']->image_path }}">
        <div id="googleMap"></div>

        <div class="container">
            <div class="form-section">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title text-center">
                            <h2 class="text-center">@lang('contact.leave_us_a_message')</h2>
                            <hr>
                        </div> <!-- section-title -->

                        <form class="support-form text-left" method="post" action="/{{ $lang }}/store-contact">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <input name="name" type="text" class="form-control" id="inputName15" placeholder="@lang('contact.your_name')" value="{{ old('name') }}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group email-field {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input name="email" class="domainSearchBar form-control" placeholder="@lang('contact.your_email')" value="{{ old('email') }}" id="domainSearchBar" type="email">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                                <input name="subject" type="text" class="form-control" id="inputName" placeholder="@lang("contact.subject")" value="{{ old('subject') }}">
                            </div>

                            <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                <textarea name="message" class="form-control" placeholder="@lang("contact.your_message")" rows="6">{{ old('message') }}</textarea>
                            </div>

                            <div class="form-group text-center">
                                <button class="btn btn-primary subscribeBtn" name="contact_submit" type="submit">@lang("contact.send_message")</button>
                            </div>
                        </form>
                        @include('errors.errors')
                    </div>
                </div>
            </div> <!-- mail-section -->
        </div>
    </section> <!-- map-section -->
@endsection

@section('top-ex')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_API_KEY') }}" type="text/javascript"></script>
@endsection
@section('bottom-ex')
    <script>
        $(function() {
            init();
            function init() {
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,
                    scrollwheel: false,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(15.2886633, 44.20271709999997), // liu-yemen

                    // This is where you would paste any style found on Snazzy Maps.
                    styles:[{"featureType": "all","elementType": "labels.text.fill","stylers": [{"saturation": 36},{"color": "#333333"},{"lightness": 40}]},
                        {"featureType": "all","elementType": "labels.text.stroke","stylers": [{"visibility": "on"},{"color": "#ffffff"},{"lightness": 16}]},
                        {"featureType": "all","elementType": "labels.icon","stylers": [{"visibility": "off"}]},
                        {"featureType": "administrative","elementType": "geometry.fill","stylers": [{"color": "#fefefe"},{"lightness": 20}]},
                        {"featureType": "administrative","elementType": "geometry.stroke","stylers": [{"color": "#fefefe"},{"lightness": 17},{"weight": 1.2}]},
                        {"featureType": "landscape","elementType": "geometry","stylers": [{"color": "#f5f5f5"},{"lightness": 20}]},
                        {"featureType": "poi","elementType": "geometry","stylers": [{"color": "#f5f5f5"},{"lightness": 21}]},
                        {"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#dedede"},{"lightness": 21}]},
                        {"featureType": "road.highway","elementType": "geometry.fill","stylers": [{"color": "#ffffff"},{"lightness": 17}]},
                        {"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#ffffff"},{"lightness": 29},{"weight": 0.2}]},
                        {"featureType": "road.arterial","elementType": "geometry","stylers": [{"color": "#ffffff"},{"lightness": 18}]},
                        {"featureType": "road.local","elementType": "geometry","stylers": [{"color": "#ffffff"},{"lightness": 16}]},
                        {"featureType": "transit","elementType": "geometry","stylers": [{"color": "#f2f2f2"},{"lightness": 19}]},
                        {"featureType": "water","elementType": "geometry","stylers": [{"color": "#e9e9e9"},{"lightness": 17}]}]};

                // Get the HTML DOM element that will contain your map
                var mapElement = document.getElementById('googleMap');
                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(15.2886633, 44.20271709999997),
                    map: map,
                    title: 'LIU Sana\'a Campus',
                    icon: '/images/contact/university.png'
                });
            }
        });
    </script>
@endsection