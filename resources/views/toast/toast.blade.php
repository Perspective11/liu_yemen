@if (Session::has('toast'))
    @php
        $toast = Session::get('toast');
    @endphp
    <script>
        $(document).ready(function () {
            iziToast.settings({});
            @if(is_array($toast))
            @php
                $message = $toast[0];
                $class = isset($toast[1])? $toast[1] : 'success';
                $title = isset($toast[2])? $toast[2] : ucfirst($class);
            @endphp
            iziToast.{{ $class }}({
                title: '{{ $title }}',
                message: '{{ $message }}',
            });
            @else
            iziToast.info({
                title: '@lang('toast.success')',
                message: '{{ $toast  }}'
            });
            @endif
        });

    </script>
@endif


