@extends('layouts.app')
@section('content')


    <section class="header-title header-overlay" data-bg="/images/event_gallery.jpg">
        <div class="container">
            <h2 class="title">Event Gallery</h2>
        </div>
    </section> <!-- header-title -->




    <section class="gallery-section section-padding">
        <div class="container text-center">
            <div class="portfolio gallery-grid">
                <div class="row">
                    <ul class="portfolio-sorting gallery-button list-inline text-center">
                        <li><a href="#" data-group="all" class="filter-btn active">All</a></li>
                        <li><a class="filter-btn" href="#" data-group="people">Campus</a></li>
                        <li><a class="filter-btn" href="#" data-group="simpsons">Library</a></li>
                        <li><a class="filter-btn" href="#" data-group="futurama">Class</a></li>
                    </ul> <!--end portfolio sorting -->

                    <div id="lightBox" class="gallery-wrapper">
                        <ul class="portfolio-items courses list-unstyled" id="grid">
                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (1).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (1).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (2).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (2).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (4).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (4).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (1).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (1).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (8).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (8).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (3).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (3).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (6).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (6).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (7).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (7).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (5).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (5).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (2).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (2).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (4).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (4).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (11).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (11).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (8).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (8).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (3).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (3).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (6).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (6).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (7).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (7).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (5).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (5).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (2).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (2).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (4).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (4).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (11).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (11).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (8).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (8).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (3).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (3).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (6).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (6).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (7).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (7).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (5).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (5).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (2).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (2).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (4).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (4).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (12).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (12).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (8).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (8).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (3).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (3).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (6).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (6).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (7).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (7).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (5).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (5).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (2).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (2).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (4).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (4).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (1).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (1).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (8).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (8).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (3).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (3).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (6).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (6).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (7).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (7).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (5).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (5).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (2).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (2).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (4).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (4).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (1).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (1).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (8).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (8).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (3).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (3).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (6).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (6).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (7).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (7).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (5).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (5).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (2).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (2).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (4).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (4).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["simpsons"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (1).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (1).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["people"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (8).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (8).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (3).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (3).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (6).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (6).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>

                            <li class="col-sm-3" data-groups='["futurama"]'>
                                <figure class="portfolio-item gallery-caption">
                                    <img src="/images/event_gallery/event (7).jpg" alt="">

                                    <div class="hover-view"><a href="/images/event_gallery/event (7).jpg"><i class="fa fa-search-plus"></i></a></div>
                                </figure>
                            </li>
                        </ul> <!--end portfolio grid -->
                    </div> <!-- gallery-wrapper -->
                </div> <!--end row -->
            </div>
        </div> <!-- container -->
    </section> <!-- gallery-section -->


@endsection