@extends('layouts.app')
@section('content')
    <section class="header-title header-overlay" data-bg="{{ $images['events_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('events.events')</h2>
        </div>
    </section> <!-- header-title header-overlay -->
    @if (request()->keys())
        <div class="menu-bar">
            <div class="container text-body">
                <h3>@lang('events.filters')  </h3>
                <ul class="menu-feature">
                    @if (request()->filled('date'))
                        <li><strong>@lang('events.date'):</strong> {{ request('date') }}</li>
                    @endif
                    @if (request()->filled('club'))
                        <li><strong>@lang('events.club'):</strong> {{ request('club') }}</li>
                    @endif
                    @if (request()->filled('search'))
                        <li><strong>@lang('posts.search'):</strong> {{ request('search') }}</li>
                    @endif
                    @if (request()->filled('time'))
                        <li><strong>@lang('events.time'):</strong> {{ request('time') == 'upcoming' ? __('events.upcoming') : (request('time') == 'past' ? __('events.past') : '') }}</li>
                    @endif
                </ul>
            </div>
        </div> <!-- menu-bar -->
    @endif

    <section class="event-list-section section-padding" data-bg="{{ $images['events_events_list']->image_path }}">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    @foreach ($events as $event)
                        <div class="event-wrapper">
                            <div class="event-img hidden-xs" style="" data-bg="{{ $event->poster_path }}">

                            </div>
                            <div class="wrapper-container">
                                <div class="date text-center">{{ $event->date->day }} <span class="month">{{ $event->date->shortEnglishMonth }}</span></div>
                                <div class="wrapper-content">
                                    <h4><a href="{{ $event->path() }}">{{ str_limit($event['title_' . $lang], 40)  }}</a></h4>
                                    <div class="event-info">
                                        @if ($event['location_' . $lang])
                                            <span class="location small-text"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $event['location_' . $lang] }}</span>
                                        @endif
                                        @if ($event->club)
                                            <span class="small-text"><a onclick="insertParam('club', '{{ $event->club->name_en }}')"><i class="fa fa-suitcase" aria-hidden="true"></i> {{ $event->club['name_' . $lang] }}</a></span>
                                        @endif
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="event-body">{!! $event['body_' . $lang] !!}</div>

                                    <a href="{{ $event->path() }}" class="btn btn-primary">@lang('home.get-details')</a>
                                </div>
                            </div> <!-- wrapper-content -->
                        </div>
                    @endforeach
                </div>
                <div class="col-md-3">
                    @include('events.nav')
                </div>
            </div>
            <div class="pagination-wrapper row text-center">
                <div class="col-xs-12">
                    {{ $events->links() }}
                </div>
            </div>
        </div>

    </section> <!-- event-list-section -->

@endsection

@section('bottom-ex')
    <script>
        function insertParam(key, value) {
            key = encodeURI(key);
            value = encodeURI(value);

            var kvp = document.location.search.substr(1).split('&');

            var i = kvp.length;
            var x;
            while (i--) {
                x = kvp[i].split('=');

                if (x[0] == key) {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }

            if (i < 0) {
                kvp[kvp.length] = [key, value].join('=');
            }

            //this will reload the page, it's likely better to store this until finished
            document.location.search = kvp.join('&');
        }
    </script>
@endsection