@extends('layouts.app')
@section('content')

    <section class="header-title header-overlay" data-bg="{{ $images['single_event_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('events.events')</h2>
        </div>
    </section> <!-- header-title header-overlay -->

    <section class="event-single-section section-padding" data-bg="{{ $images['single_event_section']->image_path }}">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="event-left-bar">
                        <span class="date"><i class="fa fa-calendar" aria-hidden="true"></i> {{ $singleEvent->date->format('F j, Y') }}</span>
                        @if ($singleEvent->pictures)
                            <div id="single-event-carousel" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @foreach ($singleEvent->pictures as $picture)
                                        <li data-target="#single-event-carousel" data-slide-to="{{ $loop->index }}" class="{{ $loop->index ? '' : 'active' }}"></li>
                                    @endforeach
                                </ol>

                                <div class="carousel-inner" role="listbox">
                                    @foreach ($singleEvent->pictures as $picture)
                                        <div class="item {{ $loop->index ? '' : 'active' }}">
                                            <img src="{{ $picture->image_path }}" alt="{{ $picture->name }}">
                                        </div>
                                    @endforeach
                                </div>
                            </div> <!-- single-event-carousel -->
                        @endif

                        <div class="event-content">
                            <h3><a href="{{ $singleEvent->path() }}">{{ $singleEvent['title_' . $lang] }}</a></h3>

                            <div class="event-body">
                                {!! $singleEvent['body_' . $lang] !!}
                            </div>

                            <ul class="post">
                                <li><i class="fa fa-calendar" aria-hidden="true"></i> {{ $singleEvent->date->diffForHumans() }}</li>
                                @if ($singleEvent['location_' . $lang])
                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $singleEvent['location_' . $lang] }}</li>
                                @endif
                                @if ($singleEvent->club)
                                    <li><a href="{{ '/events?' . http_build_query(['club' => $singleEvent->club->name_en]) }}"><i class="fa fa-suitcase" aria-hidden="true"></i> {{ $singleEvent->club['name_' . $lang] }}</a></li>
                                @endif
                                <li><a href="{{ '/events?' . http_build_query(['time' => $singleEvent->getTime()]) }}"><i class="fa fa-calendar-times-o" aria-hidden="true"></i> {{ $singleEvent->getTime(true) }}</a></li>
                            </ul>
                        </div> <!-- event-content -->
                        <div class="widget">
                            <aside class="widget-social pull-right">
                                <ul class="list-inline clearfix">
                                    <li>
                                        <a href="https://www.facebook.com/share.php?u={{ env('APP_URL') . '/' . $lang . '/events/' . $singleEvent->id }}&title={{ urlencode($singleEvent['title_' . $lang]) }}"
                                           class="facebook facebook-bg"> <i class="fa fa-facebook" aria-hidden="true"></i> @lang('home.share')</a></li>
                                    <li>
                                        <a href="https://twitter.com/intent/tweet?status={{ urlencode( $singleEvent['title_' . $lang] ) }}+{{ env('APP_URL') . '/' . $lang . '/events/' . $singleEvent->id }}"
                                           class="twitter twitter-bg"> <i class="fa fa-twitter" aria-hidden="true"></i> @lang('home.tweet')</a>
                                    </li>
                                </ul>
                            </aside>
                        </div> <!-- widget -->

                    </div> <!-- event-left-bar -->
                </div>

                <div class="col-md-3">
                    @include('events.nav')
                </div> <!-- col-md-3 -->
            </div>
        </div>
    </section> <!-- news-section -->


@endsection