<div class="news-right-bar ml-30">
    <form class="widget-search rtl" action="/events">
        <input type="text" class="form-control" name="search" placeholder="@lang('posts.search-here')" value="{{ request('search') }}">
        <input type="submit" class="btn btn-default btn-sm" value=" @lang('posts.search-btn')">
    </form> <!-- widget-search -->

    @if (isset($singleEvent->poster_path))
        <div class="widget-poster">
            <h4 class="text-uppercase">@lang('events.event-poster')</h4>
            <div class="event-poster"><img src="{{ $singleEvent->poster_path }}" alt=""></div>
        </div>
    @endif

    <div class="widget-post">
        <h4 class="text-uppercase">@lang('events.recent-events')</h4>

        <ul>
            @foreach ($featuredEvents as $event)
                <li>
                    <div class="thumb pull-left">
                        <a href="{{ $event->path() }}"><img src="{{ $event->poster_path }}" alt="image"></a>
                    </div>

                    <div class="post-desk">
                        <h5><a href="{{ $event->path() }}">{{ str_limit($event['title_' . $lang], 40)  }}</a></h5>
                        <span class="date text-uppercase">{{ $event->date->diffForHumans() }}</span>
                        <div class="clearfix"></div>
                    </div> <!-- post-desk -->
                </li>
            @endforeach
        </ul>
    </div> <!-- widget-post -->



    <div class="widget-archive">
        <h4 class="text-uppercase">@lang('events.archive')</h4>
        <ul>
            @foreach ($groupedEvents as $key => $events)
                <li><a href="{{ '/events?' . http_build_query(['date' => str_replace(' ', '', $key)])  }}">{{ $key }}</a>
                    <span class="count push-down">{{ $events->count() }}</span>
                </li>
            @endforeach
        </ul>
    </div> <!-- widget-archive -->
    <div class="widget-archive">
        <h4 class="text-uppercase">@lang('events.time')</h4>
        <ul>
            <li><a href="{{ '/events?' . http_build_query(['time' => 'upcoming']) }}">@lang('events.upcoming')</a>
                <span class="count push-down">{{ $upcomingCount }}</span>
            </li>
            <li><a href="{{ '/events?' . http_build_query(['time' => 'past']) }}">@lang('events.past')</a>
                <span class="count push-down">{{ $pastCount }}</span>
            </li>
        </ul>
    </div> <!-- widget-archive -->

    <div class="widget-tag">
        <h4 class="text-uppercase">@lang('events.clubs')</h4>

        <ul>
            @foreach ($clubs as $club)
                <li><a href="{{ '/events?' . http_build_query(['club' => $club->name_en]) }}">{{ $club['name_' . $lang] }}</a> <span class="count">{{ $club->events_count }}</span></li>
            @endforeach
        </ul>
    </div>
</div> <!-- blog-right-bar -->
