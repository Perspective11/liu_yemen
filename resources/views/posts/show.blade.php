@extends('layouts.app')
@section('content')
    <section class="header-title header-overlay" data-bg="{{ $images['single_post_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('posts.blog')</h2>
        </div>
    </section> <!-- header-title header-overlay -->

    <section class="news-section section-padding" data-bg="{{ $images['single_post_section']->image_path}}" >
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="single-news">
                        <div class="news-wrapper">
                            <img src="{{ $singlePost->picture }}" alt="image">

                            <div class="wrapper-content">
                                <span class="title">{{ $singlePost->created_at->diffForHumans() }}</span>

                                <h3>{{ $singlePost['title_' . $lang] }}</h3>

                                <div class="post-body">
                                    {!! $singlePost['body_' . $lang] !!}
                                </div>
                            </div> <!-- wrapper-content -->
                        </div> <!-- news-wrapper -->
                        <div class="widget">
                            <aside class="widget-social pull-right">
                                <ul class="list-inline clearfix">
                                    <li>
                                        <a href="https://www.facebook.com/share.php?u={{ env('APP_URL') . '/' . $lang . '/blog/' . $singlePost->id }}&title={{ urlencode($singlePost['title_' . $lang]) }}"
                                           class="facebook facebook-bg"> <i class="fa fa-facebook" aria-hidden="true"></i> @lang('home.share')</a></li>
                                    <li>
                                        <a href="https://twitter.com/intent/tweet?status={{ urlencode( $singlePost['title_' . $lang] ) }}+{{ env('APP_URL') . '/' . $lang . '/blog/' . $singlePost->id }}"
                                           class="twitter twitter-bg"> <i class="fa fa-twitter" aria-hidden="true"></i> @lang('home.tweet')</a>
                                    </li>
                                </ul>
                            </aside>
                        </div> <!-- widget -->
                    </div> <!-- single-news -->
                </div>

                <div class="col-md-3">
                    @include('posts.nav')
                </div> <!-- col-md-3 -->
            </div>
        </div>
    </section> <!-- news-section -->

@endsection