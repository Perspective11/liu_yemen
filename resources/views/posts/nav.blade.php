<div class="news-right-bar ml-30">
    <form class="widget-search rtl" action="/blog">
        <input type="text" class="form-control" name="search" placeholder="@lang('posts.search-here')" value="{{ request('search') }}">
        <input type="submit" class="btn btn-default btn-sm" value="@lang('posts.search-btn')">
    </form> <!-- widget-search -->

    <div class="widget-post">
        <h4 class="text-uppercase">@lang('posts.recent-posts')</h4>

        <ul>
            @foreach ($allPosts as $post)
                <li>
                    <div class="thumb pull-left">
                        <a href="{{ $post->path() }}"><img src="{{ $post->picture }}" alt="image"></a>
                    </div>

                    <div class="post-desk">
                        <h5><a href="{{ $post->path() }}">{{ str_limit($post['title_' . $lang], 40)  }}</a></h5>
                        <span class="date text-uppercase">{{ $post->created_at->diffForHumans() }}</span>
                        <div class="clearfix"></div>
                    </div> <!-- post-desk -->
                </li>
                @if ($loop->index >= 2)
                    @break
                @endif
            @endforeach
        </ul>
    </div> <!-- widget-post -->



    <div class="widget-archive">
        <h4 class="text-uppercase">@lang('posts.archive')</h4>
        <ul>
            @foreach ($groupedPosts as $key => $posts)
                <li><a href="{{ '/blog?' . http_build_query(['date' => str_replace(' ', '', $key)])  }}">{{ $key }}</a>
                    <span class="count push-down">{{ $posts->count() }}</span>
                </li>
            @endforeach
        </ul>
    </div> <!-- widget-archive -->
</div> <!-- blog-right-bar -->
