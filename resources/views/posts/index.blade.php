@extends('layouts.app')
@section('content')
    <section class="header-title header-overlay" data-bg="{{ $images['posts_header_title']->image_path }}" >
        <div class="container">
            <h2 class="title">@lang('posts.blog')</h2>
        </div>
    </section> <!-- header-title header-overlay -->

    @if (request()->keys())
        <div class="menu-bar">
            <div class="container text-body">
                <h3>@lang('posts.filters')  </h3>
                <ul class="menu-feature">
                    @if (request()->filled('date'))
                        <li><strong>@lang('posts.date'):</strong> {{ request('date') }}</li>
                    @endif
                    @if (request()->filled('search'))
                        <li><strong>@lang('posts.search'):</strong> {{ request('search') }}</li>
                    @endif
                </ul>
            </div>
        </div> <!-- menu-bar -->
    @endif


    <section class="news-section section-padding" data-bg="{{ $images['posts_section']->image_path }}">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="news-left-bar">
                        <div class="news-wrapper">
                            @foreach ($posts as $post)
                                <img src="{{ $post->picture }}" alt="image">

                                <div class="wrapper-content">
                                    <span class="title">{{ $post->created_at->diffForHumans() }}</span>
                                    <h3>{{ str_limit($post['title_' . $lang] ,40) }}</h3>
                                    <div class="post-body">{!! $post['body_' . $lang] !!}</div>
                                    <span class="link"><a href="{{ $post->path() }}">@lang('home.continue-reading') <i class="fa fa-long-arrow-@lang('home.right')" aria-hidden="true"></i></a></span>
                                </div> <!-- wrapper-content -->
                            @endforeach
                        </div> <!-- news-wrapper -->

                        <div class="pagination-wrapper row text-center">
                            <div class="col-xs-12">
                                {{ $posts->links() }}
                            </div>
                        </div>
                    </div> <!-- news-left-bar -->
                </div>

                <div class="col-md-3">
                    @include('posts.nav')
                </div> <!-- col-md-3 -->
            </div>
        </div>
    </section> <!-- news-section -->
@endsection