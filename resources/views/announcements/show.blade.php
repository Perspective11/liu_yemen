@extends('layouts.app')
@section('content')
    <section class="header-title header-overlay" {{ $images['single_announcement_header_title']->image_path }}>
        <div class="container">
            <h2 class="title">@lang('announcements.announcements')</h2>
        </div>
    </section> <!-- header-title header-overlay -->

    <section class="news-section section-padding" {{ $images['single_announcement_section']->image_path }}>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="single-news">
                        <div class="news-wrapper">
                            <img src="{{ $singleAnnouncement->picture }}" alt="image">

                            <div class="wrapper-content">
                                <span class="title">{{ $singleAnnouncement->created_at->diffForHumans() }}</span>

                                <h3>{{ $singleAnnouncement['title_' . $lang] }}</h3>

                                <div class="post-body">
                                    {{ $singleAnnouncement['body_' . $lang] }}
                                </div>
                            </div> <!-- wrapper-content -->
                        </div> <!-- news-wrapper -->

                        <div class="widget">
                            <aside class="widget-social pull-right">
                                <ul class="list-inline clearfix">
                                    <li>
                                        <a href="https://www.facebook.com/share.php?u={{ env('APP_URL') . '/' . $lang . '/announcements/' . $singleAnnouncement->id }}&title={{ urlencode($singleAnnouncement['title_' . $lang]) }}"
                                           class="facebook facebook-bg"> <i class="fa fa-facebook" aria-hidden="true"></i> @lang('home.share')</a></li>
                                    <li>
                                        <a href="https://twitter.com/intent/tweet?status={{ urlencode( $singleAnnouncement['title_' . $lang] ) }}+{{ env('APP_URL') . '/' . $lang . '/announcements/' . $singleAnnouncement->id }}"
                                           class="twitter twitter-bg"> <i class="fa fa-twitter" aria-hidden="true"></i> @lang('home.tweet')</a>
                                    </li>
                                </ul>
                            </aside>
                        </div> <!-- widget -->
                    </div> <!-- single-news -->
                </div>

                <div class="col-md-3">
                    @include('announcements.nav')
                </div> <!-- col-md-3 -->
            </div>
        </div>
    </section> <!-- news-section -->

@endsection