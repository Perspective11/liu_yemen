<div class="news-right-bar ml-30">
    <form class="widget-search rtl" action="/announcements">
        <input type="text" class="form-control" name="search" placeholder="@lang('posts.search-here')" value="{{ request('search') }}">
        <input type="submit" class="btn btn-default btn-sm" value="@lang('posts.search-btn')">
    </form> <!-- widget-search -->

    <div class="widget-post">
        <h4 class="text-uppercase">@lang('announcements.recent-announcements')</h4>

        <ul>
            @foreach ($allAnnouncements as $announcement)
                <li>
                    <div class="thumb pull-left">
                        <a href="{{ $announcement->path() }}"><img src="{{ $announcement->picture }}" alt="image"></a>
                    </div>

                    <div class="post-desk">
                        <h5><a href="{{ $announcement->path() }}">{{ str_limit($announcement['title_' . $lang], 40)  }}</a></h5>
                        <span class="date text-uppercase">{{ $announcement->created_at->diffForHumans() }}</span>
                        <div class="clearfix"></div>
                    </div> <!-- post-desk -->
                </li>
                @if ($loop->index >= 2)
                    @break
                @endif
            @endforeach
        </ul>
    </div> <!-- widget-post -->



    <div class="widget-archive">
        <h4 class="text-uppercase">@lang('announcements.archive')</h4>
        <ul>
            @foreach ($groupedAnnouncements as $key => $announcements)
                <li><a href="{{ '/announcements?' . http_build_query(['date' => str_replace(' ', '', $key)])  }}">{{ $key }}</a>
                    <span class="count push-down">{{ $announcements->count() }}</span>
                </li>
            @endforeach
        </ul>
    </div> <!-- widget-archive -->
</div> <!-- announcements-right-bar -->
