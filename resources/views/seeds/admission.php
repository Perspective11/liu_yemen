<h2>Here&#39;s a walk through the admission process in LIU Yemen.</h2>

<h3>In order to join LIU as a Bachelor&#39;s student, you need to pass certain criteria:</h3>

<ul>
    <li>
        <p>A highschool certificate&nbsp;</p>
    </li>
    <li>
        <p>Passing an exam mostly testing the English language</p>
    </li>
    <li>
        <p>If you are a transfer student, all documents of courses taken from the previous university will be required</p>
    </li>
</ul>

<p>If you scored low on the English language test, you will have to take as many English courses as needed before starting on the Bachelor&#39;s courses.</p>

<p>&nbsp;</p>

<h3>In case you are applying for a master degree in MBA, these are the criteria:</h3>

<ul>
    <li>
        <p>A Bachelor&#39;s degree&nbsp;</p>
    </li>
    <li>
        <p>A GPA of 3.0 or more</p>
    </li>
    <li>
        <p>Passing an exam that tests the knowledge of the business concepts and related field</p>
    </li>
</ul>

<p>If you score low on the test, certain pre-MBA courses will have to be taken before starting the MBA courses.</p>
