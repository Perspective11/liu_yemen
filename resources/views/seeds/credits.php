<h2>A word of gratitude to those who helped make this project happen</h2>

<p>&nbsp;</p>

<h3><strong>Website Developed and Designed by</strong>:</h3>

<h3><a href="https://www.facebook.com/otekit/">OteK Solutions</a>, which is a small tech company founded by LIU Sana&#39;a graduates.</h3>

<h3>Main team member in this project is Aiman Noman.<br />
    <br />
    <br />
    <strong>Data Gathering and Entry and Logistics by:</strong></h3>

<ul>
    <li>
        <h3>Student Center Manager: Abdulwahab Atif</h3>
    </li>
    <li>
        <h3>IT Club Coordinator: Ehab Al-Sabri</h3>
    </li>
    <li>
        <h3>Mohammed Al Aghbari</h3>
    </li>
</ul>

<p>&nbsp;</p>

<h3><strong>Photographs Taken Mainly By The Talented:</strong></h3>

<ul>
    <li>
        <h3>Weam Alsaqqaf</h3>
    </li>
    <li>
        <h3>Shohdi Al-Sofi</h3>
    </li>
</ul>

<h3>&nbsp;</h3>

<h3><strong>Special thanks to all the&nbsp;friendly staff members of LIU. Chief among them are:</strong></h3>

<ul>
    <li>
        <h3>Dean of the IT Department: Ahmed Taqi</h3>
    </li>
    <li>
        <h3>LIU Yemen President: Dr. Rida Hazimi.</h3>
    </li>
</ul>

<p>&nbsp;</p>

<h2>Thank you all.</h2>

<h2>&nbsp;</h2>
