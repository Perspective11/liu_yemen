@extends('layouts.app')
@section('top-ex')
    <!-- owl-carrosel-css -->
    <link href="/plugins/owl-carrosel/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="/plugins/owl-carrosel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    <!-- Revulation Slider CSS -->
    <link rel="stylesheet" type="text/css" href="/plugins/rs-plugin/css/settings.css" media="screen" />
@endsection
@section('content')
    <!-- slider-section start-->
    <section class="slider-section">
        <h2 class="hidden">Title</h2>

        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7">
                        <!-- MAIN IMAGE -->
                        <img src="{{ $images['slider_1']->image_path }}"  alt="SlideOne"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption large-text lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="300"
                             data-speed="600"
                             data-start="1200"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 3;">
                            {!! $contents['first_slide_main']['value_' . $lang] !!}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption small-text lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="470"
                             data-speed="600"
                             data-start="1500"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 3;">
                            {!! $contents['first_slide_subtitle']['value_' . $lang] !!}
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption link-button lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="600"
                             data-speed="600"
                             data-start="1800"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 4;">

                            <a href="/contact" class="btn btn-primary">@lang('home.contact')</a>
                            <a href="/majors" class="btn btn-default">@lang('home.get-details')</a>

                        </div>
                    </li>
                    <!-- SLIDE  -->

                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7">
                        <!-- MAIN IMAGE -->
                        <img src="{{ $images['slider_2']->image_path }}"  alt="SlideOne"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption large-text lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="300"
                             data-speed="600"
                             data-start="1200"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 3;">
                            {!! $contents['second_slide_main']['value_' . $lang] !!}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption small-text lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="470"
                             data-speed="600"
                             data-start="1500"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 3;">
                            {!! $contents['second_slide_subtitle']['value_' . $lang] !!}
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption link-button lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="600"
                             data-speed="600"
                             data-start="1800"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 4;">

                            <a href="/contact" class="btn btn-primary">@lang('home.contact')</a>
                            <a href="/majors" class="btn btn-default">@lang('home.get-details')</a>

                        </div>
                    </li>
                    <!-- SLIDE  -->

                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="7">
                        <!-- MAIN IMAGE -->
                        <img src="{{ $images['slider_3']->image_path }}"  alt="SlideOne"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption large-text lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="300"
                             data-speed="600"
                             data-start="1200"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 3;">
                            {!! $contents['third_slide_main']['value_' . $lang] !!}
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption small-text lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="470"
                             data-speed="600"
                             data-start="1500"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 3;">
                            {!! $contents['third_slide_subtitle']['value_' . $lang] !!}

                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption link-button lfb ltt tp-resizeme"
                             data-x="{{ $lang == 'ar'? 'right' : 'left'}}"
                             data-hoffset="{{ $lang == 'ar'? '-' : ''}}10"
                             data-responsive_offset="on"
                             data-y="600"
                             data-speed="600"
                             data-start="1800"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 4;">

                            <a href="/contact" class="btn btn-primary">@lang('home.contact')</a>
                            <a href="/majors" class="btn btn-default">@lang('home.get-details')</a>

                        </div>
                    </li>
                    <!-- SLIDE  -->
                </ul>
            </div>
        </div>
    </section>
    <!-- slider-section end-->



    <section class="subject-section">
        <div class="container">
            <div class="subject-carousel owl-carousel owl-theme" data-dir="{{ $dir }}">
                @foreach ($sliderPosts as $post)
                    <div class="item">
                        <div class="subject-wrapper">
                            <div class="caption">
                                <img src="{{ $post->picture }}" alt="">

                                <div class="hover-show">
                                    <span class="hover"><a href="{{ $post->path() }}">@lang('home.read-more')</a></span>
                                    <span class="border-one"></span>
                                    <span class="border-two"></span>
                                </div>
                            </div>

                            <div class="wrapper-content">
                                <h3><a href="{{ $post->path() }}">{{ str_limit($post['title_' . $lang], 40)  }}</a></h3>
                                <div class="post-body">{!! $post['body_' . $lang] !!}</div>
                            </div>
                        </div> <!-- subject-wrapper -->
                    </div>
                @endforeach
            </div>
        </div>
    </section> <!-- subject-section -->


    <section class="course-section section-padding" data-bg="{{ $images['home_course_section']->image_path }}">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="course-left-bar">
                        <h3>@lang('home.search-for-course')</h3>

                        <form class="subscribeForm" action="/courses" target="_blank">
                            <input name="search" class="courseSearchBar" value="@lang('home.course-name')" onblur="if(this.value=='')this.value='@lang("home.course-name")'" onfocus="if(this.value=='@lang("home.course-name")')this.value=''" id="courseSearchBar" type="text">

                            <div class="search pull-right">
                                <button type="submit" class="subscribeBtn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </form>

                        <div class="contact">
                            <h4>{{ $contents['home_courses_cant_decide']['value_' . $lang] }}</h4>

                            <h5>@lang('home.call-us'): <span class="ltr inline-block">{{ $appSettings['courses_phone'] }}</span></h5>
                        </div>

                        <div class="button">
                            <span class="btn btn-default"><a href="/courses"><i class="fa fa-book" aria-hidden="true"></i> @lang('home.all-courses')</a></span>
                            <span class="btn btn-default"><a href="/contact"><i class="fa fa-envelope" aria-hidden="true"></i> @lang('home.contact')</a></span>
                        </div>
                    </div> <!-- course-left-bar -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="course-right-bar">
                        <div class="panel-group" id="promote-accordion" role="tablist" aria-multiselectable="true">
                            @for ($i = 0; $i < $departments->count(); $i++)
                                @php
                                $department = $departments[$i];
                                $majorsCount = $department->majors()->count();
                                $majorsIncrement = 0;
                                $majorsLoop = $majorsCount >= 3? $majorsCount / 3 : 1;
                                @endphp
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading{{ $i }}">
                                        <h4 class="panel-title">
                                            <a class="{{ $i ? 'collapsed' : '' }}" role="button" data-toggle="collapse" data-parent="#promote-accordion" href="#collapse{{ $i }}" aria-expanded="{{ $i ? 'true' : 'false' }}" aria-controls="collapse{{$i}}">
                                                {{ $department['name_' . $lang] }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{ $i }}" class="panel-collapse collapse {{ $i ?: 'in' }}" role="tabpanel" aria-labelledby="heading{{ $i }}">
                                        <div class="panel-body">
                                            <ul class="panel-wrapper">
                                                @for ($j = 0; $j < $majorsLoop; $j++)
                                                    <li>
                                                        <ul>
                                                            @for ($k = 0; $k < 3; $k++)
                                                                @if ($majorsIncrement === $majorsCount)
                                                                    @break
                                                                @endif
                                                                <li><a href="{{ '/courses?major=' . $department->majors[$majorsIncrement]['name_en'] }}" target="_blank"><i class="fa fa-check" aria-hidden="true"></i> {{ $department->majors[$majorsIncrement]['name_' . $lang] }}</a></li>
                                                                @php
                                                                $majorsIncrement ++ ;
                                                                @endphp
                                                            @endfor
                                                        </ul>
                                                    </li>
                                                @endfor
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div> <!-- promote-accordion -->
                    </div> <!-- course-right-bar -->
                </div>
            </div>
        </div>
    </section> <!-- course-section -->


    <section class="about-us-section section-padding">
        <div class="container">
            <div class="section-title text-center">
                <h2>@lang('home.about-liu-yemen')</h2>
                <div class="text-center">{!!   $contents['home_about_liu_desc']['value_' . $lang] !!}</div>
            </div> <!-- section-title -->

            <div class="row">
                <div class="col-md-8">
                    <div class="section-container">
                        @php
                            $servicesCount = $services->count();
                            $servicesIncrement = 0;
                            $servicesLoop = $servicesCount > 2 ? $servicesCount / 2 : 1;
                        @endphp
                        @for ($j = 0; $j < $servicesLoop; $j++)
                        <div class="row">
                            @for ($k = 0; $k < 2; $k++)
                                @if ($servicesIncrement === $servicesCount)
                                    @break
                                @endif
                                    <div class="col-sm-6">
                                        <div class="section-wrapper">
                                            <span class="icon pull-left"><i class="fa {{ $services[$servicesIncrement]->icon }}" aria-hidden="true"></i></span>

                                            <div class="content-bar">
                                                <h4>{{ $services[$servicesIncrement]['title_' . $lang] }}</h4>

                                                <p>{{ $services[$servicesIncrement]['description_' . $lang] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @php
                                    $servicesIncrement ++ ;
                                @endphp
                            @endfor
                        </div>
                            @if ($servicesIncrement === $servicesCount)
                                @break
                            @endif
                        @endfor

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="counting-section text-center">
                                    <div class="counting-pusher">
                                        <div class="counting-wrapper">
                                            <div class="count-description">
                                                <span class="timer">{{ $appSettings['home_metric_4'] }}</span>{{ isset($appSettings['metric_symbol_4']) ? $appSettings['metric_symbol_4'] : '' }}
                                            </div>
                                            <p>{{ $contents['home_count_4']['value_' . $lang] }}</p>
                                        </div>  <!-- counting-wrapper -->

                                        <div class="counting-wrapper">
                                            <div class="count-description">
                                                <span class="timer">{{ $appSettings['home_metric_3'] }}</span>{{ isset($appSettings['metric_symbol_3']) ? $appSettings['metric_symbol_3'] : '' }}
                                            </div>
                                            <p>{{ $contents['home_count_3']['value_' . $lang] }}</p>
                                        </div>  <!-- counting-wrapper -->

                                        <div class="counting-wrapper">
                                            <div class="count-description">
                                                <span class="timer">{{ $appSettings['home_metric_2'] }}</span>{{ isset($appSettings['metric_symbol_2']) ? $appSettings['metric_symbol_2'] : '' }}
                                            </div>
                                            <p>{{ $contents['home_count_2']['value_' . $lang] }}</p>
                                        </div>  <!-- counting-wrapper -->

                                        <div class="counting-wrapper">
                                            <div class="count-description">
                                                <span class="timer">{{ $appSettings['home_metric_1'] }}</span>{{ isset($appSettings['metric_symbol_1']) ? $appSettings['metric_symbol_1'] : '' }}
                                            </div>
                                            <p>{{ $contents['home_count_1']['value_' . $lang] }}</p>
                                        </div>  <!-- counting-wrapper -->
                                    </div> <!-- /.counting-pusher -->
                                </div> <!-- counting-section -->
                            </div>
                        </div>
                    </div> <!-- section-container -->
                </div>
                <div class="col-md-4">
                    <span class="cpation hidden-sm hidden-xs wow fadeInRight"><img src="{{ $images['home_about_us_students']->image_path }}" alt=""></span>
                </div>
            </div>
        </div>
    </section> <!-- about-us-section -->



    <section class="latest-event-section section-padding" data-bg="{{ $images['home_latest_events_section']->image_path }}">
        <div class="container">
            <div class="section-title">
                <h2>@lang('home.latest-events')</h2>
                <span class="small-text"><a href="/events">@lang('home.all-events')</a></span>
            </div> <!-- section-title -->

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div id="event-carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @foreach ($events as $event)
                                <div class="item {{ $loop->index ?: 'active' }}"> {{-- to add the active class only at the first iteration --}}
                                    <div class="event-wrapper">
                                        <div class="event-img" style="" data-bg="{{ $event->getPicture(true) }}">

                                        </div>
                                        <div class="wrapper-container">
                                            <div class="date pull-left text-center">{{ $event->date->day }} <span class="month">{{ $event->date->shortEnglishMonth }}</span></div>

                                            <div class="wrapper-content">
                                                <h4><a href="{{ $event->path() }}">{{ str_limit($event['title_' . $lang], 40)  }}</a></h4>

                                                <div class="event-info">
                                                    @if ($event['location_' . $lang])
                                                        <span class="location small-text"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $event['location_' . $lang] }}</span>
                                                    @endif
                                                    @if ($event->club)
                                                        <span class="small-text"><i class="fa fa-suitcase" aria-hidden="true"></i> {{ $event->club['name_' . $lang] }}</span>
                                                    @endif
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="event-body">{!! $event['body_' . $lang] !!}</div>

                                                <a href="{{ $event->path() }}" class="btn btn-primary">@lang('home.get-details')</a>
                                            </div>
                                        </div> <!-- wrapper-content -->
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#event-carousel" role="button" data-slide="prev"><img src="/images/left-arrow.png" alt=""></a>

                        <a class="right carousel-control" href="#event-carousel" role="button" data-slide="next"><img src="/images/right-arrow.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- latest-event-section -->

    <section class="blog-section section-padding">
        <div class="container">
            <div class="section-title">
                <h2>@lang('home.our-blog')</h2>
                <span class="small-text"><a href="/blog">@lang('home.all-blog')</a></span>
            </div> <!-- section-title -->
            @php
            $firstPost = $featuredPosts->first()
            @endphp
            <div class="row">
                @if ($firstPost)
                    <div class="col-md-4">
                        <div class="blog-left-bar">
                        <span class="caption">
                            <img src="{{ $firstPost->picture }}" alt="">
                        </span>

                            <div class="content-bar">
                                <h3><a href="{{ $firstPost->path() }}">{{ str_limit( $firstPost['title_' . $lang], 40) }}</a></h3>

                                <ul class="post">
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $firstPost->created_at->diffForHumans() }}</li>
                                </ul>

                                <div class="post-body">
                                    {!! $firstPost['body_' . $lang]!!}
                                </div>
                                <a class="link" href="{{ $firstPost->path() }}">@lang('home.read-more')</a>
                                <div class="clearfix"></div>
                            </div>
                        </div> <!-- blog-left-bar -->
                    </div>
                @endif
                <div class="col-md-5">
                    <div class="blog-middle-bar">
                        @for ($i = 1; $i < $featuredPosts->count(); $i++)
                            <div class="media">
                                <div class="media-left pull-left">
                                    <img class="post-img" src="{{ $featuredPosts[$i]->picture }}" alt="">
                                </div>
                                <div class="media-content">
                                    <h3><a href="{{ $featuredPosts[$i]->path() }}">{{ str_limit( $featuredPosts[$i]['title_' . $lang], 40)  }}</a></h3>

                                    <ul class="post">
                                        <li><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $featuredPosts[$i]->created_at->diffForHumans() }}</li>
                                    </ul>
                                </div>
                            </div> <!-- media -->

                        @endfor
                    </div> <!-- blog-middle-bar -->
                </div>

                <div class="col-md-3 hidden-sm hidden-xs no-padding">
                    <div class="blog-right-bar">
                        <img src="{{ $images['home_blog_right_bar']->image_path }}" alt="">
                    </div> <!-- blog-right-bar -->
                </div>
            </div>
        </div>
    </section> <!-- blog-section -->

    <section class="instagram-section">
        <div class="container-fluid">
            <div class="section-title text-center">
                <h2><a href="/gallery">@lang('home.our-gallery')</a></h2>
            </div> <!-- section-title -->

            <div class="instagram-carousel owl-carousel owl-theme">
                @foreach ($gallery as $picture)
                    <div class="item"><img src="{{ $picture->thumb_path }}" alt="{{ $picture['name_' . $lang] }}" class="img-responsive"></div>
                @endforeach
            </div> <!-- instagram-carousel -->
        </div>
    </section> <!-- instagram-section -->

    @if ($appSettings['view_job_apps'] == 'true')
        <section class="joining-section" data-bg="{{ $images['home_joining_section']->image_path }}">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 hidden-xs">
                    <div class="caption">
                        <img src="{{ $images['home_joining_section_employee']->image_path }}" alt="">
                    </div>
                </div>

                <div class="col-md-9 col-sm-8">
                    <div class="wrapper-content">
                        <h3>{!! $contents['home_join_us']['value_' . $lang] !!}</h3>

                        <a href="/teachers/create" class="btn">@lang('home.become-a-teacher')</a>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- joining-section -->
    @endif

@endsection