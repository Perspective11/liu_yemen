@section('top-ex')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="/css/datatables-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <style>

    </style>

@endsection
<div class='courses'>
    <table id="courses-table" class="hover order-column responsive" width="100%">
        <thead>
        <tr>
            <th>@lang('courses.code')</th>
            <th>@lang('courses.name')</th>
            <th>@lang('courses.credits')</th>
            <th class="min-desktop">@lang('courses.type')</th>
            <th class="never">Category</th>
            <th class="never">Order Number</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($courses as $course)
            <tr>
                <td class="">{{ $course->code }}</td>
                <td class="">{{ isset($course['name_' . $lang]) ? $course['name_' . $lang] : $course->name_en }}</td>
                <td class="">{{ $course->credits }}</td>
                <td>{{ $course->courseType? $course->courseType->name_en : '' }}</td>
                <td>{{ $course->getDataCategory() }}</td>
                <td>{{ $course->order_number }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>




@section('bottom-ex')
    <script>
        $(document).ready( function () {
            var coursesTable = $('#courses-table').DataTable({
                responsive: {
                    details: false
                },
                "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "@lang('courses.all')"] ],
                "pageLength": -1,
                //"autoWidth": false,
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                    },
                    {
                        "targets": [ 1 ],
                        responsivePriority: 1
                    },
                    {
                        "targets": [ 2 ],
                    },
                    {
                        "targets": [ 3 ],
                    },
                    {
                        "targets": [ 4 ],
                    },
                    {
                        "targets": [ 5 ],
                    }
                ],
                "order": [[ 5, 'asc' ]],
                @if($lang == 'ar')
                "language": {
                    "sProcessing":   "جارٍ التحميل...",
                    "sLengthMenu":   "أظهر _MENU_ من المدخلات",
                    "sZeroRecords":  "لم يتم العثور على أية سجلات",
                    "sInfo":         "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
                    "sInfoEmpty":    "يعرض 0 إلى 0 من أصل 0 سجل",
                    "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
                    "sInfoPostFix":  "",
                    "sSearch":       "ابحث:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "الأول",
                        "sPrevious": "السابق",
                        "sNext":     "التالي",
                        "sLast":     "الأخير"
                    }
                }
                @endif
            });

            var majorsSelect = $(".select-majors").select2({
                placeholder: "@lang('courses.select-a-major')",
                width: '100%'
            });

            $(".select-majors").on('change', function ($e) {
                var searchValue = '';
                if ($(this).val() != 'all'){
                    searchValue = $(this).val();
                }
                coursesTable
                    .column( 4 )
                    .search( searchValue )
                    .draw();
            })
            @if(request('major'))
            majorsSelect.val('{{ request('major') }}'); // Select the option with a value of '1'
            majorsSelect.trigger('change'); // Notify any JS components that the value changed
            @endif
            @if(request('search'))
            coursesTable
                .search( '{{ request('search') }}' )
                .draw();
            @endif

            $('.sort-asc').on('click', function ($e) {
                console.log('asc');
                coursesTable
                    .order( [ 5, 'asc' ] )
                    .draw();
            })
            $('.sort-desc').on('click', function ($e) {
                console.log('desc');
                coursesTable
                    .order( [ 5, 'desc' ] )
                    .draw();
            })
        } );

    </script>
@endsection