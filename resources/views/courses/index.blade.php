@extends('layouts.app')
@section('content')
<!-- === BEGIN CONTENT === -->

<section class="mb-50 header-title" data-bg="{{ $images['courses_header_title']->image_path }}" >
    <div class="container">
        <h2 class="title">@lang('courses.courses')</h2>
    </div>
</section> <!-- header-title -->

<div class="container courses-section" data-bg="{{ $images['courses_course_section']->image_path }}">
    <div class="row">
        <div class="col-xs-12 mb-50">
            <h3>@lang('courses.courses-with-majors')</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="majors">@lang('courses.majors')</label>
                <select class="form-control select-majors" id="majors">
                    <option value="all"> @lang('courses.all-courses')</option>
                    <option value="General Requirement"> @lang('courses.general-requirements')</option> 
                    <option value="General Elective"> @lang('courses.general-electives')</option>
                    @foreach($majors as $major)
                        <option value="{{ $major->code }}"> {{ $major['name_' . $lang] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group sort-buttons pull-right">
                <label for="buttons">@lang('courses.sort')</label>
                <div id="buttons" class="btn-group-xs" role="group" aria-label="...">
                    <a class="sort-button sort-asc btn btn-default">@lang('courses.asc')</a>
                    <a class="sort-button sort-desc btn btn-default">@lang('courses.desc')</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="row">
        <div class="courses-container col-xs-12 mb-50">
            @if (! $courses->count())
                <p class="text-center">@lang('courses.no-courses-available')</p>
            @else
                @include('courses.list', compact('courses'))
            @endif
        </div>
    </div>
</div>

<!-- === END CONTENT === -->
@endsection