@extends('layouts.app')
@section('content')
    <section class="header-title header-overlay" data-bg="{{ $images['teachers_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('teachers.teaching-staff')</h2>
        </div>
    </section> <!-- header-title -->


    <section class="gallery-section staff-section section-padding">
        <div class="container text-center">
            <div class="section-title text-body-center mb-50">
                <h2>@lang('teachers.our-full-time')</h2>
                <p>{{ $contents['teachers_subtitle_desc']['value_' . $lang] }}</p>
            </div> <!-- section-title -->

            <div class="portfolio gallery-grid">
                <div class="row">

                    <div id="lightBox" class="gallery-wrapper">
                        <ul class="portfolio-items courses list-unstyled" id="grid">
                            @foreach ($teachers as $teacher)
                                <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <div class="item-container text-body-center">
                                        <div class="img-div">
                                            <img src="{{ $teacher->picture }}" alt="">
                                        </div>
                                        <h3>{{ $teacher['name_' . $lang] }}</h3>
                                        <hr>
                                        <p>{{ $teacher['title_' . $lang] }}</p>
                                    </div>
                                </li>
                            @endforeach
                        </ul> <!--end portfolio grid -->
                    </div> <!-- gallery-wrapper -->
                </div> <!--end row -->
            </div>
        </div> <!-- container -->
    </section> <!-- gallery-section -->


@endsection