@extends('layouts.app')
@section('content')
    <section class="header-title mb-50" data-bg="{{ $images['apply_teachers_header_title']->image_path }}">
        <div class="container">
            <h2 class="title">@lang('teachers.become-an-instructor')</h2>
        </div>
    </section> <!-- header-title -->
    <section class="teacher-form" data-bg="{{ $images['apply_teacher_form']->image_path }}">
        <div class="container">
            <div class="form-section">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-title text-center text-body-center">
                            <h2>@lang('teachers.fill-in-your-info')</h2>
                            <hr>
                        </div> <!-- section-title -->

                        <form class="support-form text-left" action="/{{ $lang }}//teachers/store" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group {{ $errors->has('app_name') ? ' has-error' : '' }}">
                                        <input type="text" name="app_name" class="form-control" id="inputName15" placeholder="@lang('contact.your_name')" value="{{ old('app_name') }}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group email-field {{ $errors->has('app_email') ? ' has-error' : '' }}">
                                        <input class="form-control" name="app_email" placeholder="@lang('contact.your_email')" type="email" value="{{ old('app_email') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group {{ $errors->has('app_level_of_education') ? ' has-error' : '' }}">
                                        <input type="text" name="app_level_of_education" class="form-control" id="inputLevel" placeholder="@lang('teachers.level-of-education')" value="{{ old('app_level_of_education') }}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group {{ $errors->has('app_field_of_study') ? ' has-error' : '' }}">
                                        <input class="form-control" name="app_field_of_study" placeholder="@lang('teachers.filed-of-study')" value="{{ old('app_field_of_study') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('app_notes') ? ' has-error' : '' }}">
                                <textarea class="form-control" name="app_notes" placeholder="@lang('teachers.notes')" rows="4">{{ old('app_notes') }}</textarea>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p class="pull-right formLbl">@lang('teachers.upload-your-cv')</p>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-file form-group {{ $errors->has('app_resume') ? ' has-error' : '' }}">
                                        <input type="file" class="form-control" id="inputFile" name="app_resume">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button class="btn btn-primary subscribeBtn" type="submit">@lang('teachers.submit')</button>
                            </div>
                        </form>
                        @include('errors.errors')
                    </div>
                </div>
            </div> <!-- mail-section -->
        </div>
    </section>


@endsection