<!DOCTYPE html>
<html lang="{{ $lang }}">
<head>
    @include('partials.head')
    @if(Session::has('toast'))
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/css/iziToast.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/js/iziToast.min.js"></script>
    @endif
    @yield('top-ex')
</head>
<body class="homePageThree {{ $lang }}">
{{--    @include('partials.preloader')--}}
    @include('partials.nav')
    @yield('content')
    @include('partials.footer')
    @include('partials.foot')
    @include('partials.offcanvas-nav')
    <div id="toTop" class="hidden-xs">
        <i class="fa fa-chevron-up"></i>
    </div> <!-- totop -->
    @yield('bottom-ex')
    @include('toast.toast')
</body>
</html>
