@extends('layouts.app')
@section('content')
<!-- === BEGIN CONTENT === -->

<section class="header-title mb-50" data-bg="{{ $images['calendar_header_title']->image_path }}">
    <div class="container">
        <h2 class="title">@lang('calendar.calendar')</h2>
    </div>
</section> <!-- header-title -->

<div class="container calendar-section" data-bg="{{ $images['calendar_section']->image_path }}">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <h3>@lang('calendar.events-calendar')</h3>
        </div>
    </div>
    <div class="row">
        <div class="calendar-container col-md-offset-1 col-md-10">
            @if (! $eventCount)
                <p class="text-center">@lang('calendar.no-events-available')</p>
            @else
                @include('calendar.calendar')
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <a href="{{ url('/events') }}" class="btn btn-default">@lang('calendar.view-all-events')</a>
        </div>
    </div>
</div>

<!-- === END CONTENT === -->
@endsection