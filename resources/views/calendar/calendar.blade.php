@section('top-ex')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css">
    @if ($lang == 'ar')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/ar.js"></script>
    @endif

@endsection


<div id='calendar'></div>


@section('bottom-ex')
    <script>
        $('#calendar').fullCalendar({
            eventRender: function (event, element) {
                var content = '';
                if (event.club){
                    content += '<p><strong>@lang("calendar.club"):</strong> ' + event.club + '</p>';
                }
                if (event.location){
                    content += '<p><strong>@lang("calendar.location"):</strong> ' + event.location + '</p>';
                }
                element.popover({
                    title: event.title,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body',
                    html: 'true',
                    animation: 'true',
                    content: content,
                });
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            eventSources: [
                {
                    url: '/{{ $lang }}/calendar/getEvents',
                }
            ],
        })
    </script>
@endsection