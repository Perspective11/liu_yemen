@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>User Details</h3>
    <div class="">
        <a href="{{ $user->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $user->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i class="fa fa-times"></i> Delete</button>
        </form>
        @if ($user->child() && $user->child()->file_path)
            <form class="form-inline inline" method="post" action="{{ $user->path(true) . '/downloadResume' }}"
                  target="_blank">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-download"></i> Download CV</button>
            </form>
        @endif
        @if ($user->isGraduate())
            <a href="{{ $user->path(true) . '/uploadResumeShow' }}" class="btn bg-purple btn-sm"><i
                        class="fa fa-upload"></i> Upload CV</a>
        @endif

    </div>
@endsection


@section("content")
    <div class="row">
        @if($user->child() && $user->child()->pictures()->count())
            <div class="col-md-3">
                <div class="form-group">
                    <label for="image">Image:</label>
                    <img src="{{$user->child()->pictures()->count()? $user->child()->pictures()->first()->path :''}}"
                         class="img-responsive img-rounded" alt="">
                </div>
            </div>
        @endif
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">User</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">Username:</label>
                        <p id="name">{{$user->name}}</p>
                    </div>
                    @if($user->childName())
                        <div class="form-group">
                            <label for="childName">Company/Graduate Name:</label>
                            <p id="childName">{{ $user->childName() }}</p>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <p id="email">{{$user->email}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="role">Role:</label>
                        <p id="role">{{$user->roles->first()->name}}</p>
                    </div>
                    <div class="form-group">
                        <label for="created_at">Joined At:</label>
                        <p id="created_at">{{$user->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$user->updated_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        @php
                            $class = $user->user_status ? 'success' : 'danger';
                            $status = $user->user_status ? 'Deactivate' : 'Activate';
                        @endphp
                        <label for="user_status">User Status:</label><br>
                        <a href="{{ $user->path(true) . '/activation' }}"
                           class="btn btn-xs post-status btn-{{ $class}}">{{ $status }}</a>
                    </div>
                    @if($user->child())
                        @if($user->child()->links())
                            <div class="form-group">
                                <label for="links">Links:</label>
                                <div class='link-icons'>
                                    @foreach($user->child()->links() as $link)
                                        <a href="{{ $link['link'] }}" class="link-icon">
                                            <i style="color:{{$link['color']}}"
                                               class="fa fa-lg fa-{{$link['icon']}}"></i>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <hr>
    @if($user->isGraduate())
        @include('admin.user.graduate-info')
    @elseif($user->isCompany())
        @include('admin.user.company-info')
    @endif
@endsection
@section('footer-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this user?',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection