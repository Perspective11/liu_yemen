@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Announcement Details</h3>
    <div class="">
        <a href="{{ $announcement->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $announcement->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i
                        class="fa fa-times"></i> Delete
            </button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="image">Image:</label>
                <img src="{{ $announcement->getPicture() }}"
                     class="img-responsive img-rounded" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="form-group">
                        <label for="created_at">Posted At:</label>
                        <p id="created_at">{{$announcement->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$announcement->updated_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        @php
                            $class = $announcement->status ? 'success' : 'danger';
                            $status = $announcement->status ? 'Unpublish' : 'Publish';
                        @endphp
                        <label for="status">Status:</label><br>
                        <a href="{{ $announcement->path(true) . '/activation' }}"
                           class="btn btn-xs announcement-status btn-{{ $class}}">{{ $status }}</a>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Announcement</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="title_en">Title:</label>
                        <p id="title_en">{{$announcement->title_en}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body_en">Body:</label>
                        <p id="body_en">{!! $announcement->body_en !!}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-6">
            <div class="box box-primary rtl">
                <div class="box-header with-border">
                    <h3 class="box-title">المنشور</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="title_ar">العنوان:</label>
                        <p id="title_ar">{{$announcement->title_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body_ar">النص:</label>
                        <p id="body_ar">{!! $announcement->body_ar !!}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
    <hr>
@endsection

@section('event')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this announcement',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection