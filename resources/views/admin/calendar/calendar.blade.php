@section('top-ex')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.css">
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
@endsection


<div id='calendar'></div>


@section('bottom-ex')
    <script>
        $('#calendar').fullCalendar({
            eventRender: function (event, element) {
                var content = '';
                if (event.club){
                    content += '<strong>Club:</strong> ' + event.club + '<br />';
                }
                if (event.location){
                    content += '<strong>Location:</strong> ' + event.location + '<br />';
                }
                element.popover({
                    title: event.title,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body',
                    html: 'true',
                    animation: 'true',
                    content: content,
                });
            },
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            eventSources: [
                {
                    url: '/admin/calendar/getEvents',
                }
            ],
            editable: true,
            eventDrop: function (event, delta, revertFunc) {
                $.ajax({
                    type: "POST",
                    url: '/admin/calendar/updateEvent',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'id': event.id,
                        'date': event.start.format(),
                    },
                    success: function (result) {
                        var date = moment(result.date);
                        swal("Update Successful!",
                            "Event '" + event.title + "'\nHave been rescheduled to:\n" + date.format("dddd, MMM Do YYYY") + ".",
                            "success")
                    },
                    error: function (ex) {
                        swal({
                            title: "Error!",
                            text: ex.msg,
                            type: "error",
                            confirmButtonText: "Cool"
                        });
                        revertFunc();
                    },
                }); // end ajax
            }, // end event drop

        })
    </script>
@endsection