@extends('adminlte::page')

@section('title', 'LIU Yemen')

@section('top-ex')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
@endsection
@section('content_header')
    <h1>Dashboard</h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-purple"><i class="fa fa-user-circle"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Users</span>
                    <span class="info-box-number">{{ number_format(\App\User::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Users --}}

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-teal"><i class="fa fa-sitemap"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Departments</span>
                    <span class="info-box-number">{{ number_format(\App\Department::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Departments --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-light-blue"><i class="fa fa-graduation-cap"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Majors</span>
                    <span class="info-box-number">{{ number_format(\App\Major::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Majors --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-blue"><i class="fa fa-book"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Courses</span>
                    <span class="info-box-number">{{ number_format(\App\Course::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Courses --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-maroon"><i class="fa fa-black-tie"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Teachers</span>
                    <span class="info-box-number">{{ number_format(\App\Teacher::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Teachers --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-olive"><i class="fa fa-building"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Facilities</span>
                    <span class="info-box-number">{{ number_format(\App\Facility::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Facilities --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <!-- Apply any bg-* class to to the icon to color it -->
                <span class="info-box-icon bg-green-active"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Clubs</span>
                    <span class="info-box-number">{{ number_format(\App\Club::count()) }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Clubs --}}
    </div>
    <hr>
    <div class="row filled-info-boxes">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange">
                <span class="info-box-icon"><i class="fa fa-bullhorn"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Announcements</span>
                    <span class="info-box-number">{{ number_format(\App\Announcement::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Announcement::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Announcement::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Announcements --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-maroon">
                <span class="info-box-icon"><i class="fa fa-file-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Posts</span>
                    <span class="info-box-number">{{ number_format(\App\Post::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Post::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Post::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Posts --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-calendar-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Events</span>
                    <span class="info-box-number">{{ number_format(\App\Event::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Event::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Event::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Events --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-purple">
                <span class="info-box-icon"><i class="fa fa-envelope-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Contact Messages</span>
                    <span class="info-box-number">{{ number_format(\App\Contact::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Contact::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Contact::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Contact Messages --}}
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-files-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Job Applications</span>
                    <span class="info-box-number">{{ number_format(\App\Application::count()) }}</span>
                    <!-- The progress section is optional -->
                    <div class="progress">
                        <div class="progress-bar" style="width: {{ \App\Application::increasePercentage() }}%"></div>
                    </div>
                    <span class="progress-description">{{ \App\Application::increasePercentage() }}% Increase in 30 Days</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>{{-- Job Applications --}}
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">LIU Yemen Website Activity</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <p class="text-center">
                                <strong>Site Activity ({{ Carbon\Carbon::now()->year -1 . ' - ' . Carbon\Carbon::now()->year }})</strong>
                            </p>

                            <div class="chart" style="height: 250px">
                                <!-- Sales Chart Canvas -->
                                <canvas id="activityChart" style="height: 180px;"></canvas>
                            </div>
                            <!-- /.chart-responsive -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Some Numbers Describing The Past Month</strong>
                            </p>

                            <div class="progress-group">
                                @php
                                    $postsStatus = \App\Post::publishedPercentage();
                                @endphp
                                <span class="progress-text">Published Posts</span>
                                <span class="progress-number"><b>{{ $postsStatus['publishedCount'] }}</b>/{{ $postsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua" style="width: {{ $postsStatus['count']? $postsStatus['publishedCount'] /  $postsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $eventsStatus = \App\Event::publishedPercentage();
                                @endphp
                                <span class="progress-text">Published Events</span>
                                <span class="progress-number"><b>{{ $eventsStatus['publishedCount'] }}</b>/{{ $eventsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: {{ $eventsStatus['count']? $eventsStatus['publishedCount'] /  $eventsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                @php
                                    $announcementsStatus = \App\Announcement::publishedPercentage();
                                @endphp
                                <span class="progress-text">Published Announcements</span>
                                <span class="progress-number"><b>{{ $announcementsStatus['publishedCount'] }}</b>/{{ $announcementsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-warning" style="width: {{ $announcementsStatus['count']? $announcementsStatus['publishedCount'] /  $announcementsStatus['count'] * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Total Published Objects</span>
                                <span class="progress-number"><b>{{ $announcementsStatus['publishedCount'] +  $postsStatus['publishedCount'] + $eventsStatus['publishedCount']}}</b>/{{ $announcementsStatus['count'] + $postsStatus['count'] + $eventsStatus['count'] }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar bg-purple" style="width: {{ ($announcementsStatus['count'] + $eventsStatus['count'] + $postsStatus['count'])? ($announcementsStatus['publishedCount'] + $eventsStatus['publishedCount'] + $postsStatus['publishedCount']) /  ($announcementsStatus['count'] + $eventsStatus['count'] + $postsStatus['count']) * 100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <hr>
    <div class="row">
        <div class="col-md-6">
            <!-- LIST -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Contact Messages</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="container-fluid">
                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Message</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse (\App\Contact::latest()->take(10)->get() as $contact)
                                <tr>
                                    <td>{{ str_limit($contact->name, 15) }}</td>
                                    <td>{{ str_limit($contact->message, 50) }}</td>
                                    <td>{{ $contact->created_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <p>No Contact Messages Available</p>
                            @endforelse
                            <!-- /.users-list -->
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/contacts') }}" class="uppercase">View All Contact Messages</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!--/.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Applications</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="container-fluid">
                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Field Of Study</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse (\App\Application::latest()->take(10)->get() as $application)
                                <tr>
                                    <td>{{ str_limit($application->name, 15) }}</td>
                                    <td>{{ str_limit($application->email, 40) }}</td>
                                    <td>{{ str_limit($application->field_of_study, 15) }}</td>
                                    <td>{{ $application->created_at->diffForHumans() }}</td>
                                </tr>
                            @empty
                                <p>No Contact Messages Available</p>
                            @endforelse
                            <!-- /.users-list -->
                            </tbody>
                        </table>
                    </div>
                    <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/applications') }}" class="uppercase">View All Applications</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!--/.box -->
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Posts</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @forelse ($recentPosts as $post)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $post->getPicture() }}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="{{ $post->path(true) }}"
                                       class="product-title">{{ str_limit($post->title_en , 30) }}
                                        <span class="label label-warning pull-right">
                                            {{ $post->created_at->diffForHumans() }}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {!! $post->body_en !!}
                                    </span>

                                </div>
                            </li>
                        @empty
                            <p>No Posts Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/posts') }}" class="uppercase">View All Posts</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Upcoming Events</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @forelse ($recentEvents as $event)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $event->getPicture() }}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="{{ $event->path(true) }}"
                                       class="product-title">{{ str_limit($event->title_en , 30) }}
                                        <span class="label label-danger pull-right">
                                            {{ $event->date->format('M jS')}}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {!! $event->body_en !!}
                                    </span>
                                </div>
                            </li>
                        @empty
                            <p>No Events Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/events') }}" class="uppercase">View All Events</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Recent Announcements Posts</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @forelse ($recentAnnouncements as $announcement)
                            <li class="item">
                                <div class="product-img">
                                    <img src="{{ $announcement->getPicture() }}" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <a href="{{ $announcement->path(true) }}"
                                       class="product-title">{{ str_limit($announcement->title_en , 30) }}
                                        <span class="label label-warning pull-right">
                                            {{ $announcement->created_at->diffForHumans() }}
                                        </span>
                                    </a>
                                    <span class="product-description">
                                        {!! $announcement->body_en !!}
                                    </span>
                                </div>
                                @empty
                                    <p>No Announcements Available</p>
                        @endforelse
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="{{ url('/admin/announcements') }}" class="uppercase">View All Announcements</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            var ctx = document.getElementById("activityChart").getContext('2d');
            var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: MONTHS,
                    datasets: [{
                        label: "Posts",
                        backgroundColor: 'rgba(0, 192, 240, 0.5)',
                        borderColor: 'rgba(0, 192, 240, 0.9)',
                        borderWidth: 1,
                        fill: false,
                        hoverBorderColor: 'rgba(0, 192, 240, 1)',
                        hoverBackgroundColor: 'rgba(0, 192, 240, 0.7)',
                        data: [
                            {{ \App\Post::increaseDuringMonth(1) }},
                            {{ \App\Post::increaseDuringMonth(2) }},
                            {{ \App\Post::increaseDuringMonth(3) }},
                            {{ \App\Post::increaseDuringMonth(4) }},
                            {{ \App\Post::increaseDuringMonth(5) }},
                            {{ \App\Post::increaseDuringMonth(6) }},
                            {{ \App\Post::increaseDuringMonth(7) }},
                            {{ \App\Post::increaseDuringMonth(8) }},
                            {{ \App\Post::increaseDuringMonth(9) }},
                            {{ \App\Post::increaseDuringMonth(10) }},
                            {{ \App\Post::increaseDuringMonth(11) }},
                            {{ \App\Post::increaseDuringMonth(12) }}
                        ],
                    }, {
                        label: "Events",
                        fill: false,
                        borderWidth: 1,
                        backgroundColor: 'rgba(221, 75, 57, 0.5)',
                        borderColor: 'rgba(221, 75, 57, 0.9)',
                        hoverBorderColor: 'rgba(221, 75, 57, 1)',
                        hoverBackgroundColor: 'rgba(221, 75, 57, 0.7)',
                        data: [
                            {{ \App\Event::increaseDuringMonth(1) }},
                            {{ \App\Event::increaseDuringMonth(2) }},
                            {{ \App\Event::increaseDuringMonth(3) }},
                            {{ \App\Event::increaseDuringMonth(4) }},
                            {{ \App\Event::increaseDuringMonth(5) }},
                            {{ \App\Event::increaseDuringMonth(6) }},
                            {{ \App\Event::increaseDuringMonth(7) }},
                            {{ \App\Event::increaseDuringMonth(8) }},
                            {{ \App\Event::increaseDuringMonth(9) }},
                            {{ \App\Event::increaseDuringMonth(10) }},
                            {{ \App\Event::increaseDuringMonth(11) }},
                            {{ \App\Event::increaseDuringMonth(12) }}
                        ],
                    }, {
                        label: "Announcements",
                        fill: false,
                        borderWidth: 1,
                        backgroundColor: 'rgba(245, 162, 32, 0.5)',
                        borderColor: 'rgba(245, 162, 32, 0.9)',
                        hoverBorderColor: 'rgba(245, 162, 32, 1)',
                        hoverBackgroundColor: 'rgba(245, 162, 32, 0.7)',
                        data: [
                            {{ \App\Announcement::increaseDuringMonth(1) }},
                            {{ \App\Announcement::increaseDuringMonth(2) }},
                            {{ \App\Announcement::increaseDuringMonth(3) }},
                            {{ \App\Announcement::increaseDuringMonth(4) }},
                            {{ \App\Announcement::increaseDuringMonth(5) }},
                            {{ \App\Announcement::increaseDuringMonth(6) }},
                            {{ \App\Announcement::increaseDuringMonth(7) }},
                            {{ \App\Announcement::increaseDuringMonth(8) }},
                            {{ \App\Announcement::increaseDuringMonth(9) }},
                            {{ \App\Announcement::increaseDuringMonth(10) }},
                            {{ \App\Announcement::increaseDuringMonth(11) }},
                            {{ \App\Announcement::increaseDuringMonth(12) }}
                        ],
                    },
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                }

            });
        });
    </script>
@endsection