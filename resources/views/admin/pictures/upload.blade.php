@push('top-stack')
    <link href="/plugins/fine-uploader/fine-uploader-new.min.css" rel="stylesheet">
    <script src="/plugins/fine-uploader/fine-uploader.min.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
    @include('partials.fineuploader-template')
@endpush
    <div id="uploadPictureImages" class="uploadPictureImages"></div>
@push('bottom-stack')
    <script type="text/javascript">
        $(function() {
            var uploadPictureImages = new qq.FineUploader({
                element: document.getElementById('uploadPictureImages'),
                debug: {{ env('APP_DEBUG') ?: 'true' }},
                request: {
                    inputName: 'image',
                    endpoint: '{{ url('/admin/pictures/uploadImagesStore') }}',
                    customHeaders: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                },
                deleteFile: {
                    enabled: true,
                    endpoint: '{{ url('/admin/pictures/deleteImage') }}',
                    customHeaders: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    method: 'POST',
                    params: {
                    }
                },
                multiple: true,
                showMessage: function(message) { swal(message); },
                @if(request()->filled('cat'))
                    session : {
                        endpoint: '{{ url('/admin/pictures/listPicturesByCat') }}',
                        params: {
                            cat: '{{ request('cat') }}'
                        }
                    },
                @endif
                @if(request()->filled('picture'))
                    session : {
                        endpoint: '{{ url('/admin/pictures/listPictureById') }}',
                        params: {
                            picture_id: '{{ request('picture') }}'
                        }
                    },
                @endif

                validation: {
                    acceptFiles: 'image/jpeg, image/png',
                    sizeLimit: 2000000,
                    allowedExtensions: ['jpg', 'jpeg', 'png'],
                },
                callbacks: {
                    onSubmit: function(id, fileName) {
                        this.setParams({ 'name': document.getElementById('name').value, 'tag': document.getElementById('tag').value });
                    },
                    onError: function(id, name, errorReason, response) {
                        var obj = JSON.parse(response['response']);
                        var errorList = obj;
                        var errorString = qq.format("Error on file {}.\n\n{}", name, errorList);
                        console.log({'obj': obj, 'errorList' :errorList, 'errorString' :errorString});
                        swal("Failed!", errorString , "error");
                    },
                    onComplete: function(id, name, responseJSON, xhr){
                        if(responseJSON['success'])
                            swal(responseJSON['message'], '', responseJSON['class']);
                    },
                    onDeleteComplete: function(id, xhr, isError) {
                    },
                }
            });
            $('#submit').on('click', function (e) {
                e.preventDefault();
                var imagesArray = uploadPictureImages.getUploads();
                console.log(imagesArray);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: '/admin/pictures/uploadedImagesUpdateInfo',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        '_token' : $('meta[name="csrf-token"]').attr('content'),
                        'images': imagesArray,
                        'name': document.getElementById('name').value,
                        'tag': document.getElementById('tag').value
                    }),
                    success: function (responseJSON) {
                        swal(responseJSON['message'], '', responseJSON['class'])
                    },
                });
            })
        });
    </script>
@endpush
