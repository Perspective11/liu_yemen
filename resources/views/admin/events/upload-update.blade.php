@push('top-stack')
    <link href="/plugins/fine-uploader/fine-uploader-new.min.css" rel="stylesheet">
    <script src="/plugins/fine-uploader/fine-uploader.min.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
    @include('partials.fineuploader-template')
@endpush
<div id="uploader" class="uploader"></div>
@push('bottom-stack')
    <script type="text/javascript">
        var uploader = new qq.FineUploader({
            element: document.getElementById('uploader'),
            debug: {{ env('APP_DEBUG') }},
            request: {
                inputName: 'image',
                endpoint: '{{ url('/admin/events/uploadImagesUpdate') }}',
                customHeaders: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                params: {
                    event_id: '{{ isset($event_id)? $event_id : "" }}'
                }
            },
            deleteFile: {
                enabled: true,
                endpoint: '{{ url('/admin/events/deleteImage') }}',
                customHeaders: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                method: 'POST',
                params: {
                    event_id: '{{ isset($event_id)? $event_id : "" }}'
                }
            },
            session : {
                endpoint: '{{ url('/admin/events/listEventImages') }}',
                params: {
                    event_id: '{{ isset($event_id)? $event_id : "" }}'
                }
            },
            multiple: true,
            showMessage: function(message) { swal(message); },
            validation: {
                acceptFiles: 'image/jpeg, image/png',
                sizeLimit: 2000000,
                allowedExtensions: ['jpg', 'jpeg', 'png'],
                itemLimit: {{ \App\Event::$image_limit }}
            },
            callbacks: {
                onError: function(id, name, errorReason, response) {
                    var obj = JSON.parse(response['response']);
                    var errorList = obj;
                    var errorString = qq.format("Error on file {}.\n\n{}", name, errorList);
                    console.log({'obj': obj, 'errorList' :errorList, 'errorString' :errorString});
                    swal("Failed!", errorString , "error");
                },
                onComplete: function(id, name, responseJSON, xhr){
                    if(responseJSON['success'])
                        swal(responseJSON['message'], '', responseJSON['class']);
                },
                onDeleteComplete: function(id, xhr, isError) {
                }
            }

        });

    </script>
@endpush
