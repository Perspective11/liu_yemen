@extends('adminlte::page')
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ url('admin/events') }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="temp" value="{{ $temp_token }}">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Event</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.events.form',[
                    'event' => new App\Event
                    ])
                    <div class="row">
                        <div class="col-md-12">
                            <label for="event_images" class="control-label">Event Images</label>
                            @include('admin.events.upload-store')
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Create Event</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
