@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('location_en') ? ' has-error' : ''  }}">
            <label for="location_en">Location:</label> <span style="color:orangered">*</span>
            <input type="text" name="location_en" class="form-control" id="location_en" value="{{ old('location_en') ?: $event->location_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('location_ar') ? ' has-error' : ''  }}">
            <label for="location_ar">المكان:</label>
            <input type="text" name="location_ar" class="form-control" id="location_ar" value="{{ old('location_ar') ?: $event->location_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('title_en') ? ' has-error' : ''  }}">
            <label for="title_en">Title:</label> <span style="color:orangered">*</span>
            <input type="text" name="title_en" class="form-control" id="title_en" value="{{ old('title_en') ?: $event->title_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('title_ar') ? ' has-error' : ''  }}">
            <label for="title_ar">العنوان:</label>
            <input type="text" name="title_ar" class="form-control" id="title_ar" value="{{ old('title_ar') ?: $event->title_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('body_en') ? ' has-error' : ''  }}">
            <label for="body_en">Body:</label>
            <textarea class="form-control" name="body_en" id="body_en" rows="5">{{ old('body_en') ?: $event->body_en }}</textarea>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group{{  $errors->has('body_ar') ? ' has-error' : ''  }}">
            <label for="body_ar">النص:</label>
            <textarea class="form-control" name="body_ar" id="body_ar" rows="5">{{ old('body_ar') ?: $event->body_ar }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('date') ? ' has-error' : ''  }}">
            <label for="date">Date:</label> <span style="color:orangered">*</span>
            <div class="input-group date">
                <input placeholder="Select Date"
                       type="text"
                       value="{{ old('date') ?? ($event->date? $event->date->format('Y-m-d') : '') }}"
                       name="date"
                       id="date"
                       class="form-control">
                <div class="input-group-addon">
                    <span class="fa fa-calendar"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('club') ? ' has-error' : ''  }}">
            <label for="club">Club</label>
            <select name="club" class="form-control select-club" id="club">
                <option value=""  {{ $event->club_id? 'selected' : '' }}></option>
                @foreach($clubs as $club)
                    @php
                        $selected = '';
                        if (old('club')){
                            if (old('club') == $club->id)
                            $selected = true;
                        }
                        elseif ($event->club && $event->club->id == $club->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $club->id }}">
                        {{ $club->name_en . ' - ' . $club->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('poster') ? ' has-error' : '' }}">
            <label for="poster" class="control-label">Event Poster</label>
            @if($event->poster_path)
                <div class="alert">
                    <p>This event already contains a poster</p>
                    <a class="btn btn-xs btn-warning change-image-poster">Change Image</a>
                    <a class="btn btn-xs btn-danger delete-image-poster">Delete Image</a>
                </div>
            @else
                <input type="file" name="poster" id="poster">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    @if ($event->poster_path)
        <div class="col-md-6">
            <img class="img-responsive" src="{{ $event->poster_path }}" alt="{{  $event->title }}">
        </div>
    @endif
</div>


@section('bottom-ex')
    <script type="text/javascript" src="/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $('.change-image-poster').on('click', function (e) {
            e.preventDefault();
            $(this).parent().replaceWith('<input type="file" name="poster" id="poster">');
        });
        $('.delete-image-poster').on('click', function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().parent().replaceWith('<input type="hidden" name="delete_poster" value="true">');
        });
        CKEDITOR.replace('body_ar', {
            removePlugins: 'image',
            height: 500,
            contentsLangDirection : 'rtl',
        });
        CKEDITOR.replace('body_en', {
            removePlugins: 'image',
            height: 500,
        });

        $('#date').datepicker({
            startDate: '+1d',
            format: 'yyyy-mm-dd',
        });
        $(".select-club").select2({
            placeholder: "Select a Club",
            allowClear: true,
            width: '100%'
        });

    </script>
@endsection