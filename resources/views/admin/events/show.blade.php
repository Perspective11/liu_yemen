@extends("adminlte::page")
@section('top-ex')
    <link rel="stylesheet" href="/plugins/simpleLightbox/simpleLightbox.min.css">
    <script src="/plugins/simpleLightbox/simpleLightbox.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Event Details</h3>
    <div class="">
        <a href="{{ $event->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $event->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i
                        class="fa fa-times"></i> Delete
            </button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        <div class="col-md-6">
            @if ($event->poster_path)
                <div class="form-group">
                    <label for="poster">Poster:</label>
                    <img src="{{ $event->poster_path }}"
                         class="img-responsive img-rounded" alt="">
                </div>
            @endif
        </div>

        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="form-group">
                        <label for="created_at">Posted At:</label>
                        <p id="created_at">{{$event->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$event->updated_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        @php
                            $class = $event->status ? 'success' : 'danger';
                            $status = $event->status ? 'Unpublish' : 'Publish';
                        @endphp
                        <label for="status">Status:</label><br>
                        <a href="{{ $event->path(true) . '/activation' }}"
                           class="btn btn-xs event-status btn-{{ $class}}">{{ $status }}</a>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Club:</label>
                        <p id="club">{{ $event->club? $event->club->name_en . ' - ' . $event->club->name_ar : '' }}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Date:</label>
                        <p id="date">{{$event->date->format('l jS \\of F Y')}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Event</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="location_en">Location:</label>
                        <p id="location_en">{{$event->location_en}}</p>
                    </div>
                    <div class="form-group">
                        <label for="title_en">Title:</label>
                        <p id="title_en">{{$event->title_en}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body_en">Body:</label>
                        <p id="body_en">{!! $event->body_en !!}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-6">
            <div class="box box-primary rtl">
                <div class="box-header with-border">
                    <h3 class="box-title">الفعالية</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="location_ar">المكان:</label>
                        <p id="location_ar">{{$event->location_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label for="title_ar">العنوان:</label>
                        <p id="title_ar">{{$event->title_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label for="body_ar">النص:</label>
                        <p id="body_ar">{!! $event->body_ar !!}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Event Images</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="imageGallery">
                        @foreach($event->pictures as $picture)
                            <a href="{{ $picture->image_path }}" title="{{ $picture->name }}" class="col-md-6"><img src="{{ $picture->thumb_path }}" alt="{{ $picture->name }}" class="img-responsive"/></a>
                        @endforeach
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

    <hr>
@endsection

@section('bottom-ex')
    <script>
        $(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this event',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });
            $('.imageGallery a').simpleLightbox();
        });


    </script>
@endsection