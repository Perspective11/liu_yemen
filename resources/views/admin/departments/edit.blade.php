@extends('adminlte::page')
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ $department->path(true) }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Department</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.departments.form')
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-warning pull-right">Edit Department</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
