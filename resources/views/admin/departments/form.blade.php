@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="/plugins/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css"/>
    <script type="text/javascript"
            src="/plugins/bootstrap-iconpicker/js/iconset/iconset-fontawesome-4.7.0.min.js"></script>
    <script type="text/javascript" src="/plugins/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js"></script>
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name_en') ? ' has-error' : ''  }}">
            <label for="name_en">Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en') ?: $department->name_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $department->name_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
            <label for="picture" class="control-label">Picture</label>
            @if($department->picture)
                <div class="alert">
                    <p>This department already contains an image</p>
                    <a class="btn btn-xs btn-warning change-image-picture">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    @if ($department->picture)
        <div class="col-md-6">
            <img class="img-responsive" src="{{ $department->picture }}" alt="{{  $department->title }}">
        </div>
    @endif
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
            <label for="icon">Icon:</label>
            <div>
                <button
                        class="btn btn-default icon-picker"
                        data-iconset="fontawesome"
                        data-icon="{{ old('icon') ?? $department->icon }}"
                        name="icon"
                        role="iconpicker">
                </button>
            </div>
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $('.change-image-picture').on('click', function (e) {
            e.preventDefault();
            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        });
        $('.icon-picker').iconpicker({
            arrowClass: 'btn-danger',
            cols: 10,
            rows: 5,
            selectedClass: 'btn-success',
            unselectedClass: ''
        });
    </script>
@endsection