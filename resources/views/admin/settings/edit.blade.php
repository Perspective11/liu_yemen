@extends('adminlte::page')
@section('top-ex')
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>
@endsection
@section('content')
    <form action="{{ url('/admin/settings/') }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Social Links</h3>
                        <h3 class="box-title pull-right">قنوات التواصل الاجتماعي</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-links">
                            <div class="form-links">
                                <h5><strong>Links:</strong></h5>
                                <div class="input-group link-input {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i style="color: #3b5998;"
                                                                       class="fa fa-facebook-official icon"></i></span>
                                    <input type="text" class="form-control" placeholder="Facebook URL"
                                           name="facebook_link"
                                           value="{{ old('facebook_link') ?: (isset($socialLinks['facebook']) ? $socialLinks['facebook']['link'] : '') }}">
                                </div>
                                <div class="input-group link-input {{ $errors->has('twitter_link') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i style="color: #1da1f2;"
                                                                       class="fa fa-twitter icon"></i></span>
                                    <input type="text" class="form-control" placeholder="Twitter URL"
                                           name="twitter_link"
                                           value="{{ old('twitter_link') ?: (isset($socialLinks['twitter']) ? $socialLinks['twitter']['link'] : '') }}">
                                </div>
                                <div class="input-group link-input {{ $errors->has('instagram_link') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i style="color: #c13584;"
                                                                       class="fa fa-instagram icon"></i></span>
                                    <input type="text" class="form-control" placeholder="Instagram URL"
                                           name="instagram_link"
                                           value="{{ old('instagram_link') ?: (isset($socialLinks['instagram']) ? $socialLinks['instagram']['link'] : '') }}">
                                </div>
                                <div class="input-group link-input {{ $errors->has('linkedin_link') ? ' has-error' : '' }}">
                                    <span class="input-group-addon"><i style="color: #0077b5;"
                                                                       class="fa fa-linkedin icon"></i></span>
                                    <input type="text" class="form-control" placeholder="Linked in URL"
                                           name="linkedin_link"
                                           value="{{ old('linkedin_link') ?: (isset($socialLinks['linkedin'])  ? $socialLinks['linkedin']['link'] : '') }}">
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Toggle Pages</h3>
                        <h3 class="box-title pull-right">اظهار واخفاء الصفحات</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-horizontal">
                            <div class="form-group  {{ $errors->has('view_courses') ? ' has-error' : '' }}">
                                <div class="col-xs-11">
                                    <label class="control-label" for="view_courses">View Courses Page</label>
                                    <span id="view_coursesHelpBlock" class="help-block">Whether or not to view the courses page in the user interface</span>
                                </div>
                                <div class="col-xs-1">
                                    <input class="toggle-button pull-right" type="checkbox"
                                           name="view_courses" {{ ($settings['view_courses'] == 'true') ? 'checked': '' }}>
                                </div>
                            </div>
                            <div class="form-group  {{ $errors->has('view_job_apps') ? ' has-error' : '' }}">
                                <div class="col-xs-11">
                                    <label class="control-label" for="view_job_apps">View Job Applications Page</label>
                                    <span id="view_job_appsHelpBlock" class="help-block">Whether or not to view the job applications page in the user interface</span>
                                </div>
                                <div class="col-xs-1">
                                    <input class="toggle-button pull-right" type="checkbox"
                                           name="view_job_apps" {{ ($settings['view_job_apps'] == 'true') ? 'checked': '' }}>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Site Info</h3>
                        <h3 class="box-title pull-right">معلومات متعلقة بالموقع</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-inputs">
                            <div class="form-group {{ $errors->has('nav_phone') ? ' has-error' : '' }}">
                                <label for="nav_phone" class="control-label col-xs-4">Navigation Phone No.</label>
                                <div class="col-xs-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input id="nav_phone" name="nav_phone" placeholder="Phone No." type="text"
                                               aria-describedby="nav_phoneHelpBlock" required="required"
                                               class="form-control" value="{{ old('nav_phone') ?: $settings['nav_phone'] }}">
                                    </div>
                                    <span id="nav_phoneHelpBlock" class="help-block">Phone number that is displayed in the top navigation</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('nav_email') ? ' has-error' : '' }}">
                                <label for="nav_email" class="control-label col-xs-4">Navigation Email Add.</label>
                                <div class="col-xs-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <input id="nav_email" name="nav_email" placeholder="Email" type="email"
                                               class="form-control" aria-describedby="nav_emailHelpBlock"
                                               required="required" value="{{ old('nav_email') ?: $settings['nav_email'] }}">
                                    </div>
                                    <span id="nav_emailHelpBlock" class="help-block">Email Address that is displayed in the top navigation</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('courses_phone') ? ' has-error' : '' }}">
                                <label for="courses_phone" class="control-label col-xs-4">Courses Phone No.</label>
                                <div class="col-xs-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input id="courses_phone" name="courses_phone" placeholder="Phone No."
                                               type="text" class="form-control"
                                               aria-describedby="courses_phoneHelpBlock" required="required"
                                               value="{{ old('courses_phone') ?: $settings['courses_phone'] }}">
                                    </div>
                                    <span id="courses_phoneHelpBlock" class="help-block">Phone number for courses inquiry</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('contact_email') ? ' has-error' : '' }}">
                                <label for="contact_email" class="control-label col-xs-4">Contact Info Email</label>
                                <div class="col-xs-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <input id="contact_email" name="contact_email" placeholder="Email" type="email"
                                               class="form-control" aria-describedby="contact_emailHelpBlock"
                                               required="required" value="{{ old('contact_email') ?: $settings['contact_email'] }}">
                                    </div>
                                    <span id="contact_emailHelpBlock" class="help-block">Email Address that is displayed in the contact info page</span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('contact_phone') ? ' has-error' : '' }}">
                                <label for="contact_phone" class="control-label col-xs-4">Contact Info Phone No.</label>
                                <div class="col-xs-8">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <input id="contact_phone" name="contact_phone" placeholder="Phone No."
                                               type="text" class="form-control"
                                               aria-describedby="contact_phoneHelpBlock" required="required"
                                               value="{{ old('contact_phone') ?: $settings['contact_phone'] }}">
                                    </div>
                                    <span id="contact_phoneHelpBlock" class="help-block">Phone No. That is displayed in the contact info page</span>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Home Page Metrics</h3>
                        <h3 class="box-title pull-right">الارقام اللتي تظهر في الصفحة الرئيسية</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-horizontal form-inputs">
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <h5>{{ $contents['home_count_1']['value_en'] }}</h5>
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_value_1" name="metric_value_1" placeholder="Value"
                                           required="required" class="form-control {{ $errors->has('metric_value_1') ? ' has-error' : '' }}"
                                           type="number"
                                           value="{{ old('metric_value_1') ?: $settings['home_metric_1'] }}">
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_symbol_1" name="metric_symbol_1" placeholder="Symbol"
                                           class="form-control {{ $errors->has('metric_symbol_1') ? ' has-error' : '' }}"
                                           value="{{ old('metric_symbol_1') ?: (isset($settings['metric_symbol_1']) ? $settings['metric_symbol_1'] : '') }}">
                                </div>
                                <div class="col-xs-4 rtl-right">
                                    <h5>{{ $contents['home_count_1']['value_ar'] }}</h5>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <h5>{{ $contents['home_count_2']['value_en'] }}</h5>
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_value_2" name="metric_value_2" placeholder="Value"
                                           required="required" class="form-control {{ $errors->has('metric_value_2') ? ' has-error' : '' }}"
                                           type="number"
                                           value="{{ old('metric_value_2') ?: $settings['home_metric_2'] }}">
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_symbol_2" name="metric_symbol_2" placeholder="Symbol"
                                           class="form-control {{ $errors->has('metric_symbol_2') ? ' has-error' : '' }}"
                                           value="{{ old('metric_symbol_2') ?: (isset($settings['metric_symbol_2']) ? $settings['metric_symbol_2'] : '') }}">
                                </div>
                                <div class="col-xs-4 rtl-right">
                                    <h5>{{ $contents['home_count_2']['value_ar'] }}</h5>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <h5>{{ $contents['home_count_3']['value_en'] }}</h5>
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_value_3" name="metric_value_3" placeholder="Value"
                                           required="required" class="form-control {{ $errors->has('metric_value_3') ? ' has-error' : '' }}"
                                           type="number"
                                           value="{{ old('metric_value_3') ?: $settings['home_metric_3'] }}">
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_symbol_3" name="metric_symbol_3" placeholder="Symbol"
                                           class="form-control {{ $errors->has('metric_symbol_3') ? ' has-error' : '' }}"
                                           value="{{ old('metric_symbol_3') ?: (isset($settings['metric_symbol_3']) ? $settings['metric_symbol_3'] : '') }}">
                                </div>
                                <div class="col-xs-4 rtl-right">
                                    <h5>{{ $contents['home_count_3']['value_ar'] }}</h5>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <h5>{{ $contents['home_count_4']['value_en'] }}</h5>
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_value_4" name="metric_value_4" placeholder="Value"
                                           required="required" class="form-control {{ $errors->has('metric_value_4') ? ' has-error' : '' }}"
                                           type="number"
                                           value="{{ old('metric_value_4') ?: $settings['home_metric_4'] }}">
                                </div>
                                <div class="col-xs-2">
                                    <input id="metric_symbol_4" name="metric_symbol_4" placeholder="Symbol"
                                           class="form-control {{ $errors->has('metric_symbol_4') ? ' has-error' : '' }}"
                                           value="{{ old('metric_symbol_4') ?: (isset($settings['metric_symbol_4']) ? $settings['metric_symbol_4'] : '') }}">
                                </div>
                                <div class="col-xs-4 rtl-right">
                                    <h5>{{ $contents['home_count_4']['value_ar'] }}</h5>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                @include('errors.errors')
            </div>
        </div>
        <section class="FAB">
            <div class="FAB__action-button">
                <button class="btn-link"><i class="action-button__icon bg-orange fa fa-edit"></i></button>
                <p class="action-button__text--hide">Submit Settings</p>
            </div>
        </section>
    </form>
    {{--</form>--}}
@endsection
@section('bottom-ex')

    <script>
        $('.FAB__action-button').hover(function () {
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function () {
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.toggle-button'));

        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });

    </script>

@endsection
