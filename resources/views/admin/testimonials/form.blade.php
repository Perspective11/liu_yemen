@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name_en') ? ' has-error' : ''  }}">
            <label for="name_en">Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en') ?: $testimonial->name_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $testimonial->name_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('quote_en') ? ' has-error' : ''  }}">
            <label for="quote_en">Quote:</label>
            <textarea class="form-control" name="quote_en" id="quote_en" rows="5">{{ old('quote_en') ?: $testimonial->quote_en }}</textarea>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group{{  $errors->has('quote_ar') ? ' has-error' : ''  }}">
            <label for="quote_ar">المقولة:</label>
            <textarea class="form-control" name="quote_ar" id="quote_ar" rows="5">{{ old('quote_ar') ?: $testimonial->quote_ar }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
            <label for="picture" class="control-label">Picture</label>
            @if($testimonial->picture)
                <div class="alert">
                    <p>This testimonial already contains an image</p>
                    <a class="btn btn-xs btn-warning change-image-picture">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    @if ($testimonial->picture)
        <div class="col-md-6">
            <img class="img-responsive" src="{{ $testimonial->picture }}" alt="{{  $testimonial->title }}">
        </div>
    @endif
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $('.change-image-picture').on('click', function (e) {
            e.preventDefault();
            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        });
    </script>
@endsection