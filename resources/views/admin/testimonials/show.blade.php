@extends("adminlte::page")
@section('top-ex')
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Testimonial Details</h3>
    <div class="">
        <a href="{{ $testimonial->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $testimonial->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i
                        class="fa fa-times"></i> Delete
            </button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="image">Image:</label>
                <img src="{{ $testimonial->getPicture() }}"
                     class="img-responsive img-rounded" alt="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="form-group">
                        <label for="created_at">Posted At:</label>
                        <p id="created_at">{{$testimonial->created_at->diffForHumans()}}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{$testimonial->updated_at->diffForHumans()}}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Testimonial</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="name_en">Name:</label>
                        <p id="name_en">{{$testimonial->name_en}}</p>
                    </div>
                    <div class="form-group">
                        <label for="quote_en">Quote:</label>
                        <p id="quote_en">{{ $testimonial->quote_en }}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-6">
            <div class="box box-primary rtl">
                <div class="box-header with-border">
                    <h3 class="box-title">المقولة</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="name_ar">الاسم:</label>
                        <p id="name_ar">{{$testimonial->name_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label for="quote_ar">الوصف:</label>
                        <p id="quote_ar">{{ $testimonial->quote_ar }}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
    <hr>
@endsection

@section('bottom-ex')
    <script>
        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            title: 'Are you sure you want to delete this testimonial',
            btnOkClass: 'btn btn-xs btn-danger',
            btnCancelClass: 'btn btn-xs btn-default',
            btnOkIcon: 'fa fa-trash',
            btnCancelIcon: 'fa fa-ban'
        });
    </script>
@endsection