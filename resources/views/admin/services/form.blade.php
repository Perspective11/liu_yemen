@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="/plugins/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css"/>
    <script type="text/javascript"
            src="/plugins/bootstrap-iconpicker/js/iconset/iconset-fontawesome-4.7.0.min.js"></script>
    <script type="text/javascript" src="/plugins/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js"></script>
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('title_en') ? ' has-error' : ''  }}">
            <label for="title_en">Title:</label> <span style="color:orangered">*</span>
            <input type="text" name="title_en" class="form-control" id="title_en" value="{{ old('title_en') ?: $service->title_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('title_ar') ? ' has-error' : ''  }}">
            <label for="title_ar">العنوان:</label>
            <input type="text" name="title_ar" class="form-control" id="title_ar" value="{{ old('title_ar') ?: $service->title_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('description_en') ? ' has-error' : ''  }}">
            <label for="description_en">Description:</label>
            <textarea class="form-control" name="description_en" id="description_en" rows="5">{{ old('description_en') ?: $service->description_en }}</textarea>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group{{  $errors->has('description_ar') ? ' has-error' : ''  }}">
            <label for="description_ar">الوصف:</label>
            <textarea class="form-control" name="description_ar" id="description_ar" rows="5">{{ old('description_ar') ?: $service->description_ar }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
            <label for="icon">Icon:</label>
            <div>
                <button
                        class="btn btn-default icon-picker"
                        data-iconset="fontawesome"
                        data-icon="{{ old('icon') ?? $service->icon }}"
                        name="icon"
                        role="iconpicker">
                </button>
            </div>
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $('.change-image-picture').on('click', function (e) {
            e.preventDefault();
            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        });

        $('.icon-picker').iconpicker({
            arrowClass: 'btn-danger',
            cols: 10,
            rows: 5,
            selectedClass: 'btn-success',
            unselectedClass: ''
        });
    </script>
@endsection