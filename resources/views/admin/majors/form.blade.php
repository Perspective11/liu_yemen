@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name_en') ? ' has-error' : ''  }}">
            <label for="name_en">Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en') ?: $major->name_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $major->name_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('code') ? ' has-error' : ''  }}">
            <label for="code">Code:</label> <span style="color:orangered">*</span>
            <input type="text" name="code" class="form-control" id="code" value="{{ old('code') ?: $major->code }}" required>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('credits') ? ' has-error' : ''  }}">
            <label for="credits">Credits:</label>
            <input type="number" name="credits" class="form-control" id="credits" value="{{ old('credits') ?: $major->credits }}">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('department') ? ' has-error' : ''  }}">
            <label for="department">Department</label>
            <select name="department" class="form-control select-department" id="department">
                <option value=""  {{ $major->department_id? 'selected' : '' }}></option>
                @foreach($departments as $department)
                    @php
                        $selected = '';
                        if (old('department')){
                            if (old('department') == $department->id)
                            $selected = true;
                        }
                        elseif ($major->department && $major->department->id == $department->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $department->id }}">
                        {{ $department->name_en . ' - ' . $department->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $('.change-image-picture').on('click', function (e) {
            e.preventDefault();
            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        });

        $(".select-department").select2({
            placeholder: "Select a Department",
            allowClear: true,
            width: '100%'
        });
    </script>
@endsection