@extends('adminlte::page')
@section('top-ex')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Manage clubs</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/clubs/create') }}"><i class="action-button__icon fa bg-red fa-plus"></i></a>
            <p class="action-button__text--hide">Add Club</p>
        </div>
    </section>
    <table class="table table-condensed jdatatable display smallest" id="clubs-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Image</th>
            <th>Name</th>
            <th>Description</th>
            <th class="rtl-right">الاسم</th>
            <th class="rtl-right">الوصف</th>
            <th>Created At</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $clubsTable = $('#clubs-table').DataTable({
                "order": [[ 5, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/clubs/getData")}}',
                    data: function (d) {

                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],

                columns: [
                    {data: 'picture', name: 'picture', sortable: 'no', orderable: 'no', searchable: 'no', width: "10%"},
                    {data: 'name_en', name: 'name_en'},
                    {data: 'description_en', name: 'description_en'},
                    {data: 'name_ar', name: 'name_ar', sClass: 'rtl'},
                    {data: 'description_ar', name: 'description_ar', sClass: 'rtl'},
                    {data: 'created_at', name: 'created_at'}
                ],
            });

            var $modal = $('.action-modal');
            $('#clubs-table tbody').on('click', 'tr', function () {

                var data = $clubsTable.row(this).data();
                $('.action-modal').modal();
                $modal.find('.view-club-details').attr('href' , '/admin/clubs/' + data['id']);
                $modal.find('.edit-club').attr('href' , '/admin/clubs/' + data['id'] + '/edit');
                $modal.find('.view-club-events').attr('href' , '/admin/events?club=' + data['name_en']);
                $modal.find('.delete-club').attr('action' , '/admin/clubs/' + data['id']);
            });

            $('#clubs-table tbody').on('click', 'a.btn', function (e) {
                e.stopPropagation();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this club?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <a href="#" class="list-group-item edit-club">Edit Club</a>
                        <a href="#" class="list-group-item view-club-details">View Club details</a>
                        <a href="#" class="list-group-item view-club-events">View Events From This Club</a>
                        <form class="list-group-item list-group-item-danger delete-club" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Club</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection