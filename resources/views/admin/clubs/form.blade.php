@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name_en') ? ' has-error' : ''  }}">
            <label for="name_en">Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en') ?: $club->name_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $club->name_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('description_en') ? ' has-error' : ''  }}">
            <label for="description_en">Description:</label>
            <textarea class="form-control" name="description_en" id="description_en" rows="5">{{ old('description_en') ?: $club->description_en }}</textarea>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group{{  $errors->has('description_ar') ? ' has-error' : ''  }}">
            <label for="description_ar">الوصف:</label>
            <textarea class="form-control" name="description_ar" id="description_ar" rows="5">{{ old('description_ar') ?: $club->description_ar }}</textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
            <label for="picture" class="control-label">Picture</label>
            @if($club->picture)
                <div class="alert">
                    <p>This club already contains an image</p>
                    <a class="btn btn-xs btn-warning change-image-picture">Change Image</a>
                </div>
            @else
                <input type="file" name="picture" id="picture">
                {{--TODO: add a drag and drop plugin--}}
            @endif
        </div>
    </div>
    @if ($club->picture)
        <div class="col-md-6">
            <img class="img-responsive" src="{{ $club->picture }}" alt="{{  $club->title }}">
        </div>
    @endif
</div>
@section('bottom-ex')
    <script type="text/javascript">
        $('.change-image-picture').on('click', function (e) {
            e.preventDefault();
            $(this).parent().replaceWith('<input type="file" name="picture" id="picture">');
        });
    </script>
@endsection