@extends('adminlte::page')
@section('top-ex')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content_header')
    <h3>Job Applications</h3>
@endsection
@section('content')
    @include('admin.job-applications.filters')
    <table class="table table-condensed jdatatable display smaller" id="applications-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Level of Education</th>
            <th>Field of Study</th>
            <th>Note</th>
            <th>Created At</th>
            <th>Resume</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('#applications-table tbody').on('click','.model-link', function (e) {
                e.stopPropagation()
            })

            var $applicationsTable = $('#applications-table').DataTable({
                "order": [[ 5, 'desc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/applications/getData")}}',
                    data: function (d) {
                        d.daterange = '{{ request('daterange')? request('daterange') : ''}}'
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'level_of_education', name: 'level_of_education'},
                    {data: 'field_of_study', name: 'field_of_study'},
                    {data: 'notes', name: 'notes'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'resume', name: 'resume'},
                ]
            });


            var $modal = $('.action-modal');
            $('#applications-table tbody').on('click', 'tr', function () {

                var data = $applicationsTable.row(this).data();
                console.log(data);
                $('.action-modal').modal();
                $modal.find('.modal-application-name').text(data['name']);
                $modal.find('.modal-application-email').text(data['email']);
                $modal.find('.modal-application-level-of-education').text(data['level_of_education']);
                $modal.find('.modal-application-field-of-study').text(data['field_of_study']);
                $modal.find('.modal-application-notes').text(data['notes']);
                $modal.find('.modal-application-resume').html(data['resume']);
                $modal.find('.application-reply').attr('href', 'mailto:' + data['email']);
                $modal.find('.delete-application').attr('action' , '/admin/applications/' + data['id']);

            });
            $('#applications-table tbody').on('click', 'a.btn', function (e) {
                e.stopPropagation();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this job application?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="modal-field">
                                <strong>Name:</strong> <span class="modal-application-name"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Email:</strong> <span class="modal-application-email"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Level of Education:</strong> <span class="modal-application-level-of-education"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Field of Study:</strong> <span class="modal-application-field-of-study"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Created At:</strong> <span class="modal-application-created"></span> <br>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="modal-field">
                                <strong>Notes:</strong> <span class="modal-application-notes"></span> <br>
                            </div>
                            <div class="modal-field">
                                <strong>Resume:</strong> <span class="modal-application-resume"></span> <br>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="list-group">
                        <a href="#" class="list-group-item application-reply">Reply to Application</a>
                        <form class="list-group-item list-group-item-danger delete-application" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete Message</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection