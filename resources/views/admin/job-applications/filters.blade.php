<form id="contact-filter" action="" method="get">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Application Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-4">
                <h5><strong>Date Range:</strong></h5>
                <div class="form-group">
                    <input type="text" class="form-control" name="daterange" id="reportrange"
                           value="{{ request('daterange')? request('daterange') : '' }}">
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-info">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $(function () {

        var start = moment().subtract(29, 'days');
        var end = moment();


        $('#reportrange').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            },
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
        });

        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
        });

        $('#clear-fields').on('click', function () {
            var strippedUrl = stripQuery(window.location.href);
            window.location.replace(strippedUrl);
        })

        function stripQuery(url) {
            return url.split("?")[0].split("#")[0];
        }
    });
</script>

