@extends('adminlte::page')
@section('content')
    <div class="row">
        <form class="col-md-10 col-md-offset-1" action="{{ url('admin/facilities') }}" method="post" accept-charset="UTF-8"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="temp" value="{{ $temp_token }}">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Create Facility</h3>
                    <div class="box-tools text-muted">
                        <p><small>Fields with <span style="color:orangered">*</span> are required</small></p>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    @include('admin.facilities.form',[
                    'facility' => new App\Facility
                    ])
                    <div class="row">
                        <div class="col-md-12">
                            <label for="facility_images" class="control-label">Facility Images</label>
                            @include('admin.facilities.upload-store')
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Create Facility</button>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </form>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('errors.errors')
        </div>
    </div>
@endsection
