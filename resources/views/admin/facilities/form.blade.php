@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <link rel="stylesheet" href="/plugins/datepicker/datepicker3.css">
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name_en') ? ' has-error' : ''  }}">
            <label for="name_en">Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en') ?: $facility->name_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $facility->name_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('description_en') ? ' has-error' : ''  }}">
            <label for="description_en">Description:</label>
            <textarea class="form-control" name="description_en" id="description_en" rows="5">{{ old('description_en') ?: $facility->description_en }}</textarea>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group{{  $errors->has('description_ar') ? ' has-error' : ''  }}">
            <label for="description_ar">الوصف:</label>
            <textarea class="form-control" name="description_ar" id="description_ar" rows="5">{{ old('description_ar') ?: $facility->description_ar }}</textarea>
        </div>
    </div>
</div>

@section('bottom-ex')
    <script type="text/javascript">


    </script>
@endsection