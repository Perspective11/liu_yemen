@extends("adminlte::page")
@section('top-ex')
    <link rel="stylesheet" href="/plugins/simpleLightbox/simpleLightbox.min.css">
    <script src="/plugins/simpleLightbox/simpleLightbox.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section("content_header")
    <h3>Facility Details</h3>
    <div class="">
        <a href="{{ $facility->path(true) . '/edit' }}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
        <form class="form-inline inline" method="post" action="{{ $facility->path(true) }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <button type="submit" class="btn btn-danger btn-sm" data-toggle="confirmation" data-popout="true"><i
                        class="fa fa-times"></i> Delete
            </button>
        </form>
    </div>
@endsection


@section("content")
    <div class="row">
        <div class="col-md-6">
            @if ($facility->getPicture())
                <div class="form-group">
                    <label for="poster">Image:</label>
                    <img src="{{ $facility->getPicture() }}"
                         class="img-responsive img-rounded" alt="">
                </div>
            @endif
        </div>

        <div class="col-md-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="form-group">
                        <label for="created_at">Posted At:</label>
                        <p id="created_at">{{ $facility->created_at->diffForHumans() }}</p>
                    </div>
                    <div class="form-group">
                        <label for="updated_at">Updated At:</label>
                        <p id="updated_at">{{ $facility->updated_at->diffForHumans() }}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Facility</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="name_en">Name:</label>
                        <p id="name_en">{{$facility->name_en}}</p>
                    </div>
                    <div class="form-group">
                        <label for="description_en">Description:</label>
                        <p id="description_en">{{ $facility->description_en }}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
        <div class="col-md-6">
            <div class="box box-primary rtl">
                <div class="box-header with-border">
                    <h3 class="box-title">المنشأة</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <label for="name_ar">الاسم:</label>
                        <p id="name_ar">{{$facility->name_ar}}</p>
                    </div>
                    <div class="form-group">
                        <label for="description_ar">الوصف:</label>
                        <p id="description_ar">{{ $facility->description_ar }}</p>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Facility Images</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="imageGallery">
                        @foreach($facility->pictures as $picture)
                            <a href="{{ $picture->image_path }}" title="{{ $picture->name }}" class="col-md-6"><img src="{{ $picture->thumb_path }}" alt="{{ $picture->name }}" class="img-responsive"/></a>
                        @endforeach
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

    <hr>
@endsection

@section('bottom-ex')
    <script>
        $(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this facility',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });
            $('.imageGallery a').simpleLightbox();
        });

    </script>
@endsection