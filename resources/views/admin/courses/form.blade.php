@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
@endsection
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{  $errors->has('name_en') ? ' has-error' : ''  }}">
            <label for="name_en">Name:</label> <span style="color:orangered">*</span>
            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en') ?: $course->name_en }}" required>
        </div>
    </div>
    <div class="col-md-6 rtl">
        <div class="form-group {{  $errors->has('name_ar') ? ' has-error' : ''  }}">
            <label for="name_ar">الاسم:</label>
            <input type="text" name="name_ar" class="form-control" id="name_ar" value="{{ old('name_ar') ?: $course->name_ar }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('code') ? ' has-error' : ''  }}">
            <label for="code">Code:</label> <span style="color:orangered">*</span>
            <input type="text" name="code" class="form-control" id="code" value="{{ old('code') ?: $course->code }}" required>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('credits') ? ' has-error' : ''  }}">
            <label for="credits">Credits:</label>
            <input type="number" name="credits" class="form-control" id="credits" value="{{ old('credits') ?: $course->credits}}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('order_number') ? ' has-error' : ''  }}">
            <label for="order_number">Order Number:</label>
            <input type="number" name="order_number" class="form-control" id="order_number" value="{{ old('order_number') ?: $course->order_number ?: 0 }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group {{  $errors->has('course_type') ? ' has-error' : ''  }}">
            <label for="course_type">Course Type</label>
            <select name="course_type" class="form-control select-course-type" id="course-type">
                @foreach($courseTypes as $courseType)
                    @php
                        $selected = '';
                        if (old('course_type')){
                            if (old('course_type') == $courseType->id)
                            $selected = true;
                        }
                        elseif ($course->courseType && $course->courseType->id == $courseType->id)
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected? 'selected' : '' }}
                            value="{{ $courseType->id }}">
                        {{ $courseType->name_en . ' - ' . $courseType->name_ar }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('majors') ? ' has-error' : '' }}">
            <label for="majors">Majors</label>
            <select class="select-majors form-control" name="majors[]" multiple="multiple">
                @foreach($majors as $major)
                    @php
                        $selected = '';
                        if (old('majors')){
                            if (in_array($major->id, old('majors')))
                            $selected = true;
                        }
                        elseif ($course->majors->pluck('id')->contains($major->id))
                            $selected = true;
                        else $selected = false;
                    @endphp
                    <option {{ $selected ? 'selected': ''}} value="{{ $major->id }}">{{ $major->name_en }} </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
@section('bottom-ex')
    <script type="text/javascript">
        var courseTypeSelect = $(".select-course-type").select2({
            placeholder: "Select a Course Type",
            allowClear: true,
            width: '100%'
        });
        $(".select-majors").select2({
            placeholder: "Select Majors",
            allowClear: true,
            maximumSelectionLength: 5,
            width: '100%'
        });

        $('.select-course-type').on('select2:select', function (e) {
            var data = e.params.data;
            if (data.id === "1") {
                $('.select-majors').removeAttr('disabled');
            }
            else{
                $('.select-majors').attr('disabled', true);
            }
        });
    </script>
@endsection