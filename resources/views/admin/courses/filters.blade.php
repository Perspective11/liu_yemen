<form id="user-filter" action="" method="get">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Course Filters</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                            class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-md-3">
                @php
                    $comboMajor = '';
                    $majorsArray =  \App\Major::has('courses')->pluck('name_en')->toArray();
                    if (request()->has('major')){
                        if (in_array(request('major'), $majorsArray))
                            $comboMajor = request('major');
                    }
                @endphp
                <h5><strong>Major:</strong></h5>
                <select name="major" class="form-control" id="major">
                    <option {{ $comboMajor == ''? 'selected': '' }} value="">All</option>
                    @foreach($majorsArray as $major)
                        <option {{ $comboMajor == $major? 'selected': '' }} value="{{ $major }}">{{ $major }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                @php
                    $comboCourseType = '';
                    $courseTypesArray =  \App\CourseType::has('courses')->pluck('name_en')->toArray();
                    if (request()->has('course_type')){
                        if (in_array(request('course_type'), $courseTypesArray))
                            $comboCourseType = request('course_type');
                    }
                @endphp
                <h5><strong>CourseType:</strong></h5>
                <select name="course_type" class="form-control" id="course_type">
                    <option {{ $comboCourseType == ''? 'selected': '' }} value="">All</option>
                    @foreach($courseTypesArray as $courseType)
                        <option {{ $comboCourseType == $courseType? 'selected': '' }} value="{{ $courseType }}">{{ $courseType }}</option>
                    @endforeach
                </select>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" id="search-submit" class="btn btn-primary">Apply Filters</button>
            <button type="reset" id="clear-fields" class="btn btn-default">Clear</button>
        </div><!-- box-footer -->
    </div><!-- /.box -->
</form>
<script>
    $(function () {

        $('#clear-fields').on('click', function () {
            var strippedUrl = stripQuery(window.location.href);
            window.location.replace(strippedUrl);
        })

        function stripQuery(url) {
            return url.split("?")[0].split("#")[0];
        }

    });

</script>