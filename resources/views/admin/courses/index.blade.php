@extends('adminlte::page')
@section('top-ex')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="/plugins/bootstrap-confirmation.min.js"></script>
@endsection
@section('content_header')
    <h3>Manage Courses</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/courses/create') }}"><i class="action-button__icon fa bg-red fa-plus"></i></a>
            <p class="action-button__text--hide">Add Course</p>
        </div>
    </section>
    @include('admin.courses.filters')
    <table class="table table-condensed jdatatable display small" id="courses-table"  style="background-color: #fff;">
        <thead>
        <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Credits</th>
            <th>Majors</th>
            <th>Course Type</th>
            <th class="rtl-right">الاسم</th>
            <th>Created At</th>
            <th>Order</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endsection
@section('bottom-ex')
    <script>
        $(function () {
            $('.FAB__action-button').hover(function(){
                $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
                $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
            }, function(){
                $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
            });

            var $coursesTable = $('#courses-table').DataTable({
                "order": [[ 7, 'asc' ]],
                pageLength: 25,
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("/admin/courses/getData")}}',
                    data: function (d) {
                        d.major = '{{ request('major')? request('major') : ''}}',
                        d.course_type = '{{ request('course_type')? request('course_type') : ''}}'
                    }
                },
                dom: 'lBfrtip',
                buttons: [
                    'colvis'
                ],

                columns: [
                    {data: 'code', name: 'code'},
                    {data: 'name_en', name: 'name_en'},
                    {data: 'credits', name: 'credits'},
                    {data: 'majors', name: 'majors', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'course_type', name: 'course_type', sortable: 'no', orderable: 'no', searchable: 'no'},
                    {data: 'name_ar', name: 'name_ar', sClass: 'rtl'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'order_number', name: 'order_number', visible: false}
                ],
            });

            var $modal = $('.action-modal');
            $('#courses-table tbody').on('click', 'tr', function () {

                var data = $coursesTable.row(this).data();
                $('.action-modal').modal();
                $modal.find('.view-course-details').attr('href' , '/admin/courses/' + data['id']);
                $modal.find('.edit-course').attr('href' , '/admin/courses/' + data['id'] + '/edit');
                $modal.find('.delete-course').attr('action' , '/admin/courses/' + data['id']);
            });

            $('#courses-table tbody').on('click', 'a.btn', function (e) {
                e.stopPropagation();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                title: 'Are you sure you want to delete this course?',
                btnOkClass: 'btn btn-xs btn-danger',
                btnCancelClass: 'btn btn-xs btn-default',
                btnOkIcon: 'fa fa-trash',
                btnCancelIcon: 'fa fa-ban'
            });

        });
    </script>
@endsection


@section('modals')
    <div class="modal fade action-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Action</h4>
                </div>
                <div class="modal-body">
                    <div class="list-group">
                        <a href="#" class="list-group-item edit-course">Edit course</a>
                        <a href="#" class="list-group-item view-course-details">View course details</a>
                        <form class="list-group-item list-group-item-danger delete-course" method="post" action="">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn-link text-red" data-toggle="confirmation" data-popout="true"> Delete course</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection