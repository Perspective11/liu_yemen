@extends('adminlte::page')
@section('top-ex')
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/plugins/sweetalert/sweetalert.css">
@endsection
@section('content')
    <ul class="nav nav-tabs nav-justified">
        @foreach ($contents->pluck('content_category')->unique() as $category)
            <li class=""><a href="{{ '#' . $category }}">{{ $category }}</a></li>
        @endforeach
    </ul>
    <a href="{{ url('/admin/contents/') }}" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a>
    <br>
    <br>
    <form action="{{ url('/admin/contents/') }}" method="post" class="edit-contents">
        {{ csrf_field() }}
        @foreach($contents->groupBy('content_category') as $groupKey => $groupedContents)
            <hr>
            <div id="{{ $groupKey }}" class="text-center"><h2>{{ $groupKey }}</h2></div>
            @foreach ($groupedContents as $content)
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $content->name_en }}</h3>
                            <h3 class="box-title pull-right">{{ $content->name_ar }}</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has($content->key . '[0]') ? ' has-error' : '' }}">
                                        <textarea name="{{$content->key . '[]'}}"
                                                  id="{{ $content->key . '_en' }}"
                                                  title="{{ $content->name_en }}"
                                                  class="textarea_en {{ $content->type === 0 ?: 'editor' }}"
                                                  maxlength="{{ $content->max_char }}"
                                                  rows="10">{!! old($content->key . '[0]') ?: $content->value_en  !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has($content->key . '[1]') ? ' has-error' : '' }}">
                                        <textarea name="{{$content->key . '[]'}}"
                                                  id="{{$content->key . '_ar'}}"
                                                  title="{{ $content->name_ar }}"
                                                  class="textarea_ar rtl {{ $content->type === 0 ?: 'editor' }}"
                                                  maxlength="{{ $content->max_char }}"
                                                  rows="10">{!! old($content->key . '[1]') ?: $content->value_ar  !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        @endforeach
        @endforeach
        <section class="FAB">
            <div class="FAB__action-button">
                <button class="btn-link"><i class="action-button__icon fa fa-edit"></i></button>
                <p class="action-button__text--hide">Submit Content</p>
            </div>
        </section>
    </form>
    {{--</form>--}}
@endsection
@section('bottom-ex')

    <script>
        $('.FAB__action-button').hover(function () {
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function () {
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });


        $(function(){
            $('.textarea_en.editor').each(function(e){
                CKEDITOR.replace( this.id, {
                    removePlugins: 'image',
                    height: 400,
                });
            });
            $('.textarea_ar.editor').each(function(e){
                CKEDITOR.replace( this.id, {
                    removePlugins: 'image',
                    height: 400,
                    contentsLangDirection : 'rtl',
                });
            });
        });

    </script>

@endsection
