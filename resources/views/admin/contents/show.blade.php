@extends('adminlte::page')
@section('content_header')
    <h3>View Text Content</h3>
@endsection
@section('content')
    <section class="FAB">
        <div class="FAB__action-button">
            <a href="{{ url('/admin/contents/edit') }}"><i class="action-button__icon bg-red fa fa-pencil"></i></a>
            <p class="action-button__text--hide">Edit Text Contents</p>
        </div>
    </section>
    <ul class="nav nav-tabs nav-justified">
        @foreach ($contents->pluck('content_category')->unique() as $category)
            <li class=""><a href="{{ '#' . $category }}">{{ $category }}</a></li>
        @endforeach
    </ul>
    @foreach($contents->groupBy('content_category') as $groupKey => $groupedContents)
        <hr>
        <div id="{{ $groupKey }}" class="text-center"><h2>{{ $groupKey }}</h2></div>
    @foreach ($groupedContents as $content)
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $content->name_en }}</h3>
                        <h3 class="box-title pull-right">{{ $content->name_ar }}</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group link-input">
                                    {!! $content->value_en !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group link-input center-block">
                                    <div class="pull-right rtl-right-body">
                                        {!! $content->value_ar !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    @endforeach
    @endforeach
@endsection
@section('bottom-ex')
    <script>
        $('.FAB__action-button').hover(function(){
            $(this).find('.action-button__text--hide').attr('class', 'action-button__text--show');
            $('.mini-action-button--hide').attr('class', 'mini-action-button--show');
        }, function(){
            $(this).find('.action-button__text--show').attr('class', 'action-button__text--hide');
        });
    </script>
@endsection
