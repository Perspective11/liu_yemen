<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function()
{
    Route::get('/', 'PagesController@index');
    Route::get('/contact', 'PagesController@contact');
    Route::post('/store-contact', 'PagesController@storeContact');
    Route::get('/gallery', 'PagesController@gallery');
    Route::get('/majors', 'PagesController@majors');
    Route::get('/calendar', 'PagesController@calendar');
    Route::get('/calendar/getEvents', 'EventsController@getCalendarEvents');
    Route::get('/courses', 'PagesController@courses');
    Route::get('/facilities', 'PagesController@facilities');
    Route::get('/about', 'PagesController@about');
    Route::get('/campus-map', 'PagesController@campusMap');
    Route::get('/teachers', 'TeachersController@index');
    Route::get('/teachers/create', 'TeachersController@create');
    Route::post('/teachers/store', 'TeachersController@store');
    Route::get('/events', 'EventsController@index');
    Route::get('/events/{id}', 'EventsController@show');
    Route::get('/event-gallery', 'EventsController@eventGallery');
    Route::get('/blog', 'PostsController@index');
    Route::get('/blog/{id}', 'PostsController@show');
    Route::get('/announcements', 'AnnouncementsController@index');
    Route::get('/announcements/{id}', 'AnnouncementsController@show');
    Route::get('/privacy-policy', 'PagesController@privacyPolicy');
    Route::get('/credits', 'PagesController@credits');
    Route::get('/admission', 'PagesController@admission');


});
// Front-end Json Request Routs


// Admin Panel Routes
Route::group(['prefix' => 'admin','middleware' => 'auth', 'as' => 'admin.'], function(){

    Route::get('/', function () {
        return redirect('/admin/home');
    });

    Route::get('/home', 'admin\HomeController@index')->name('home');


    //  admin users routes
    Route::get('/users/getData', 'admin\UsersController@getUsersData');
    Route::get('/change-password','admin\UsersController@changePassword');
    Route::post('/change-password','admin\UsersController@changePasswordStore');
    Route::resource('users','admin\UsersController');


    Route::get('/contacts/getData', 'admin\ContactsController@getContactsData');
    Route::delete('/contacts/{contact}', 'admin\ContactsController@destroy');
    Route::get('/contacts', 'admin\ContactsController@index');

    Route::get('/applications/getData', 'admin\ApplicationsController@getApplicationsData');
    Route::delete('/applications/{application}', 'admin\ApplicationsController@destroy');
    Route::get('/applications', 'admin\ApplicationsController@index');
    Route::get('/applications/{application}/view-resume', 'admin\ApplicationsController@viewResume');
    Route::get('/applications/{application}/download-resume', 'admin\ApplicationsController@downloadResume');

    Route::get('/posts/getData', 'admin\PostsController@getPostsData');
    Route::resource('/posts', 'admin\PostsController');
    Route::get('posts/{post}/activation', 'admin\PostsController@activation');
    Route::get('posts/{post}/featured', 'admin\PostsController@featured');
    Route::get('posts/{post}/slider', 'admin\PostsController@slider');

    Route::get('/clubs/getData', 'admin\ClubsController@getClubsData');
    Route::resource('/clubs', 'admin\ClubsController');

    Route::get('/courses/getData', 'admin\CoursesController@getCoursesData');
    Route::resource('/courses', 'admin\CoursesController');

    Route::get('/departments/getData', 'admin\DepartmentsController@getDepartmentsData');
    Route::resource('/departments', 'admin\DepartmentsController');

    Route::get('/majors/getData', 'admin\MajorsController@getMajorsData');
    Route::resource('/majors', 'admin\MajorsController');

    Route::get('/teachers/getData', 'admin\TeachersController@getTeachersData');
    Route::resource('/teachers', 'admin\TeachersController');

    Route::get('/services/getData', 'admin\ServicesController@getServicesData');
    Route::resource('/services', 'admin\ServicesController');

    Route::get('/testimonials/getData', 'admin\TestimonialsController@getTestimonialsData');
    Route::resource('/testimonials', 'admin\TestimonialsController');

    Route::get('/pictures/listPicturesByCat', 'admin\PicturesController@listPicturesByCat');
    Route::get('/pictures/listPictureById', 'admin\PicturesController@listPictureById');
    Route::post('/pictures/uploadedImagesUpdateInfo', 'admin\PicturesController@uploadedImagesUpdateInfo');
    Route::delete('/pictures/deleteImage', 'admin\PicturesController@DeleteImage');
    Route::get('/pictures/getData', 'admin\PicturesController@getPicturesData');
    Route::resource('/pictures', 'admin\PicturesController')->only([
        'index', 'show', 'create', 'destroy'
    ]);;
    Route::post('/pictures/uploadImagesStore', 'admin\PicturesController@uploadImagesStore');

    Route::delete('/facilities/deleteImage', 'admin\FacilitiesController@DeleteImage');
    Route::get('/facilities/listFacilityImages', 'admin\FacilitiesController@listFacilityImages');
    Route::get('/facilities/getData', 'admin\FacilitiesController@getFacilitiesData');
    Route::resource('/facilities', 'admin\FacilitiesController');
    Route::post('/facilities/uploadImagesStore', 'admin\FacilitiesController@uploadImagesStore');
    Route::post('/facilities/uploadImagesUpdate', 'admin\FacilitiesController@uploadImagesUpdate');

    Route::delete('/events/deleteImage', 'admin\EventsController@DeleteImage');
    Route::get('/events/getData', 'admin\EventsController@getEventsData');
    Route::get('/events/listEventImages', 'admin\EventsController@listEventImages');
    Route::resource('/events', 'admin\EventsController');
    Route::get('/events/{event}/activation', 'admin\EventsController@activation');
    Route::get('/events/{event}/featured', 'admin\EventsController@featured');
    Route::post('/events/uploadImagesStore', 'admin\EventsController@uploadImagesStore');
    Route::post('/events/uploadImagesUpdate', 'admin\EventsController@uploadImagesUpdate');

    // Calendar Routes
    Route::get('/calendar/getEvents', 'admin\CalendarController@getEvents');
    Route::post('/calendar/updateEvent', 'admin\CalendarController@updateEvent');
    Route::get('/calendar', 'admin\CalendarController@index');


    Route::get('/announcements/getData', 'admin\AnnouncementsController@getAnnouncementsData');
    Route::resource('/announcements', 'admin\AnnouncementsController');
    Route::get('announcements/{announcement}/activation', 'admin\AnnouncementsController@activation');
    Route::get('announcements/{announcement}/featured', 'admin\AnnouncementsController@featured');


    Route::get('/contents', 'admin\ContentsController@show');
    Route::get('/contents/edit', 'admin\ContentsController@edit');
    Route::post('/contents/', 'admin\ContentsController@update');

    Route::get('/image-contents', 'admin\ImageContentsController@show');
    Route::post('/image-contents/', 'admin\ImageContentsController@update');

    Route::get('/settings', 'admin\SettingsController@edit');
    Route::post('/settings/', 'admin\SettingsController@update');

});
