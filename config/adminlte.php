<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'LIU Yemen',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>LIU</b>Yemen',

    'logo_mini' => 'LIU<b>Y</b>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'yellow',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => '/admin/home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'DASHBOARD',
        [
            'text' => 'Dashboard',
            'url'  => 'admin/home',
            'icon'  => 'dashboard',
        ],
        [
            'text' => 'Settings',
            'url'  => 'admin/settings',
            'icon' => 'cog',
        ],
        'BLOG',
        [
            'text' => 'Posts',
            'url'  => 'admin/posts',
            'icon'  => 'clone',
        ],
        [
            'text' => 'Announcements',
            'url'  => 'admin/announcements',
            'icon'  => 'bullhorn',
        ],
        'STUDENT CENTER',
        [
            'text' => 'Clubs',
            'url'  => 'admin/clubs',
            'icon' => 'users',
        ],
        [
            'text' => 'Events',
            'url'  => 'admin/events',
            'icon'  => 'birthday-cake',
        ],
        [
            'text' => 'Calendar',
            'url'  => 'admin/calendar',
            'icon'  => 'calendar',
        ],
        'ACADEMIC',
        [
            'text' => 'Departments',
            'url'  => 'admin/departments',
            'icon' => 'sitemap',
        ],
        [
            'text' => 'Majors',
            'url'  => 'admin/majors',
            'icon' => 'graduation-cap',
        ],
        [
            'text' => 'Courses',
            'url'  => 'admin/courses',
            'icon' => 'book',
        ],
        [
            'text' => 'Teachers',
            'url'  => 'admin/teachers',
            'icon' => 'black-tie',
        ],
        'ABOUT LIU',
        [
            'text' => 'Services',
            'url'  => 'admin/services',
            'icon' => 'list',
        ],
        [
            'text' => 'Facilities',
            'url'  => 'admin/facilities',
            'icon' => 'building-o',
        ],
        [
            'text' => 'Testimonials',
            'url'  => 'admin/testimonials',
            'icon' => 'comments-o',
        ],
        [
            'text' => 'Image Gallery',
            'url'  => 'admin/pictures',
            'icon' => 'file-image-o',
        ],
        'WEBSITE CONTENT',
        [
            'text' => 'Text Content',
            'url'  => 'admin/contents',
            'icon' => 'i-cursor',
        ],
        [
            'text' => 'Images Content',
            'url'  => 'admin/image-contents',
            'icon' => 'picture-o',
        ],
        'FILLED FORMS',
        [
            'text' => 'Contact Us',
            'url'  => 'admin/contacts',
            'icon' => 'envelope-o',
        ],
        [
            'text' => 'Job Applications',
            'url'  => 'admin/applications',
            'icon' => 'files-o',
        ],
        'ACCOUNT SETTINGS',
        [
            'text' => 'Users',
            'url'  => 'admin/users',
            'icon' => 'user-circle',
        ],
        [
            'text' => 'Change Password',
            'url'  => 'admin/change-password',
            'icon' => 'lock',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
    ],
];
