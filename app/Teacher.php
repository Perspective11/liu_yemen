<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        "name_en",
        "name_ar",
        "title_en",
        "title_ar",
        "picture",
    ];

    public function getPicture(){
        if ($this->picture){
            return $this->picture;
        }
        else return '/images/teachers/default.png';
    }

    public function path($admin = null){
        if($admin){
            return '/admin/teachers/' . $this->id;
        }
        return '/teachers/' . $this->id;
    }
}
