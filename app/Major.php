<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    //
    protected $fillable = [
        "department_id",
        "name_en",
        "name_ar",
        "code",
        "credits",
    ];
    public function department()
    {
        return $this->belongsTo('App\Department');
    }
    public function courses()
    {
        return $this->belongsToMany('App\Course');
    }

    public function path($admin = false){
        if($admin)
            return '/admin/majors/' . $this->id;

        return '/majors/' . $this->id;
    }
}
