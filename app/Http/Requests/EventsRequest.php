<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'required|between:3, 100',
            'title_ar' => 'required|between:3, 100',
            'body_en'  => 'required|between:10, 10000',
            'body_ar'  => 'required|between:10, 10000',
            'club' => 'nullable|integer|exists:clubs,id',
            'date' => 'date|required',
            'location_en' => 'nullable|max:255',
            'location_ar' => 'nullable|max:255',
            'poster'  => 'nullable|image|dimensions:min_height=200|mimes:jpeg,png|max:2000',
        ];
    }
}
