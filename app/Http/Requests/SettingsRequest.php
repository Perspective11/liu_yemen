<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "facebook_link"   => "nullable|url",
            "twitter_link"    => "nullable|url",
            "instagram_link"  => "nullable|url",
            "linkedin_link"   => "nullable|url",
            "view_courses"    => "nullable|in:on",
            "view_job_apps"   => "nullable|in:on",
            "nav_phone"       => "required|between:6,20",
            "nav_email"       => "required|email",
            "courses_phone"   => "required|between:6,20",
            "contact_email"   => "required|email",
            "contact_phone"   => "required|between:6,20",
            "metric_value_1"  => "required|between:0,9999999999|numeric",
            "metric_value_2"  => "required|between:0,9999999999|numeric",
            "metric_value_3"  => "required|between:0,9999999999|numeric",
            "metric_value_4"  => "required|between:0,9999999999|numeric",
            "metric_symbol_1" => "max:10",
            "metric_symbol_2" => "max:10",
            "metric_symbol_3" => "max:10",
            "metric_symbol_4" => "max:10",
        ];
    }
}
