<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClubsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|between:3, 100',
            'name_ar' => 'required|between:3, 100',
            'description_en'  => 'nullable|max:255',
            'description_ar'  => 'nullable|max:255',
            'picture'  => 'image|dimensions:min_width=100,min_height=200|mimes:jpeg,png|max:2000',
        ];
    }
}
