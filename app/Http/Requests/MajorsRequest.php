<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MajorsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required|between:3, 100',
            'name_ar' => 'required|between:3, 100',
            'credits' => 'nullable|integer|between:0,300',
            'code' => 'nullable|alpha_dash|max:8',
            'department' => 'required|integer|exists:departments,id',
        ];
    }
}
