<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requiredPassword = '';
        if ($this->method() == 'POST'){ // not a put or patch request
            $requiredPassword = '|required';
        }
        else{
            $requiredPassword = '|nullable';
        }
        return [
            'name' => 'required',
            'email' => 'required|email|max:50|unique:users,email,' . request('id'),
            'password' => 'min:6|confirmed'. $requiredPassword,
        ];
    }
}
