<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeachersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requiredImage = '';
        if ($this->method() == 'POST'){
            $requiredImage = '|required';
        }
        return [
            'name_en' => 'required|between:3, 100',
            'name_ar' => 'required|between:3, 100',
            'title_en'  => 'required|between:3, 100',
            'title_ar'  => 'required|between:3, 100',
            'picture'  => 'image|dimensions:min_width=100,min_height=200|mimes:jpeg,png|max:2000' . $requiredImage,
        ];
    }
}
