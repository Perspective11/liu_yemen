<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoursesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en'      => 'required|between:3, 100',
            'name_ar'      => 'required|between:3, 100',
            'credits'      => 'nullable|integer|between:1,5',
            'order_number' => 'required|integer|min:0',
            'code'         => 'required|alpha_dash|max:8',
            'course_type'  => 'required|integer|exists:course_types,id',
            'majors'       => 'required_if:course_type,1|array|min:1',
            'majors.*'     => 'required_if:course_type,1|integer|distinct|exists:majors,id',
            // majors are required if course type is normal
        ];
    }
}
