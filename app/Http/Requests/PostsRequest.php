<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'required|between:3, 100',
            'title_ar' => 'required|between:3, 100',
            'body_en'  => 'required|between:10, 10000',
            'body_ar'  => 'required|between:10, 10000',
            'picture'  => 'image|dimensions:min_width=100,min_height=200|mimes:jpeg,png|max:2000',
        ];
    }
}
