<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestimonialsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requiredImage = '';
        if ($this->method() == 'POST'){
            $requiredImage = '|required';
        }

        return [
            'name_en' => 'required|between:3, 100',
            'name_ar' => 'required|between:3, 100',
            'quote_en'  => 'required|between:3, 255',
            'quote_ar'  => 'required|between:3, 255',
            'picture'  => 'image|dimensions:min_width=50,min_height=50|mimes:jpeg,png|max:2000' . $requiredImage,
        ];
    }
}
