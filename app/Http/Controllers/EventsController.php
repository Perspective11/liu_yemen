<?php

namespace App\Http\Controllers;

use App\Club;
use App\Event;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelLocalization;
use View;

class EventsController extends Controller
{
    public $lang;
    public $dir;
    function __construct()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        $dir = LaravelLocalization::getCurrentLocaleDirection();
        $this->lang = $lang;
        $this->dir = $dir;
        $footerAnnouncements = \App\Announcement::featured()->take(Post::$LIMIT_FEATURED_ANNOUNCEMENTS)->get();
        Carbon::setLocale($this->lang);
        $contents = \App\Content::all()->keyBy('key')->toArray();
        $allEvents = Event::published()->orderByDesc('date')->get();
        $groupedEvents = $allEvents->groupBy(function($e) {
            return Carbon::parse($e->date)->format('Y - m');
        });
        $featuredEvents = Event::featured()->take(3)->get();
        $upcomingCount = Event::published()->upcoming()->count();
        $pastCount = Event::published()->past()->count();
        $clubs = Club::whereHas('events')->withCount('events')->get();
        $images = \App\ImageContent::all()->keyBy('key');
        View::share(compact('lang', 'dir', 'footerAnnouncements', 'contents', 'featuredEvents', 'groupedEvents', 'clubs', 'images', 'upcomingCount', 'pastCount'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = \App\Event::published()->with('club');

        if ($request->filled('club')) {
            $events->whereHas('club', function ($query) use ($request) {
                $query->where('name_en', 'like', $request->club);
            });
        }
        if ($request->filled('search')) {
            $events->search($request->search);
        }

        if ($request->filled('date')) {
            $parsedDate = Carbon::parse($request->date);
            $startDate = $parsedDate->toDateString();
            $afterMonth = $parsedDate->addMonth(1)->addDays(-1);
            $endDate = $afterMonth->toDateString();
            $events->whereBetween('date', [$startDate, $endDate]);
        }

        if ($request->filled('time')) {
            if ($request->time == 'upcoming'){
                $events->upcoming();
            }
            elseif ($request->time == 'past'){
                $events->past();
            }
        }

        $events = $events->paginate();
        return view('events.index', compact('events'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $singleEvent = Event::with('club')->findOrFail($id);
        if ($singleEvent->status !== 1)
            abort(404);
        return view('events.show', compact('singleEvent'));
    }
    function eventGallery(){
        return view('events.event-gallery');
    }

    public function getCalendarEvents(Request $request)
    {
        $arr = [];
        $events = Event::published()->whereBetween('date', [$request->start, $request->end])->each(function ($event) use (&$arr) {
            $color = '#fc8835';
            if ($event->status === 0){
                $color = 'grey';
            }
            array_push($arr, [
                'id'    => $event->id,
                'title' => str_limit($event['title_' . $this->lang], 100),
                'start' => $event->date->toDateString(),
                'end'   => $event->date->toDateString(),
                'location'   => $event['location_' . $this->lang],
                'club'   => $event->club? $event->club['name_' . $this->lang] : '',
                'url'   => $event->path(),
                'color' => $color
            ]);
        });

        return $arr;
    }

}
