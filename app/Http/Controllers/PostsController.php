<?php

namespace App\Http\Controllers;

use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelLocalization;
use View;


class PostsController extends Controller
{
    public $lang;
    public $dir;

    function __construct()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        $dir = LaravelLocalization::getCurrentLocaleDirection();
        $this->lang = $lang;
        $this->dir = $dir;
        $footerAnnouncements = \App\Announcement::featured()->take(Post::$TYPE_FEATURED_ANNOUNCEMENT)->get();
        Carbon::setLocale($this->lang);
        $contents = \App\Content::all()->keyBy('key')->toArray();
        $allPosts = Post::published()->orderByDesc('created_at')->get();
        $groupedPosts = $allPosts->groupBy(function($e) {
            return Carbon::parse($e->created_at)->format('Y - m');
        });
        $images = \App\ImageContent::all()->keyBy('key');
        View::share(compact('lang', 'dir', 'footerAnnouncements', 'contents', 'allPosts', 'groupedPosts', 'images'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::published();

        if ($request->filled('date')) {
            $parsedDate = Carbon::parse($request->date);
            $startDate = $parsedDate->toDateString();
            $afterMonth = $parsedDate->addMonth(1)->addDays(-1);
            $endDate = $afterMonth->toDateString();
            $posts->whereBetween('created_at', [$startDate, $endDate]);
        }
        if ($request->filled('search')) {
            $posts->search($request->search);
        }
        $posts = $posts->latest()->paginate();

        return view('posts.index', compact('posts'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $singlePost = Post::findOrFail($id);

        if ($singlePost->status !== 1)
            abort(404);
        return view('posts.show', compact('singlePost'));
    }

}
