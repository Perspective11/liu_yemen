<?php

namespace App\Http\Controllers;

use App\Application;
use App\Post;
use App\Teacher;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelLocalization;
use Session;
use Validator;
use View;


class TeachersController extends Controller
{
    function __construct()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        $dir = LaravelLocalization::getCurrentLocaleDirection();
        $footerAnnouncements = \App\Announcement::featured()->take(Post::$LIMIT_FEATURED_ANNOUNCEMENTS)->get();
        Carbon::setLocale($lang);
        $contents = \App\Content::all()->keyBy('key')->toArray();
        $images = \App\ImageContent::all()->keyBy('key');
        View::share(compact('lang', 'dir', 'footerAnnouncements', 'contents', 'images'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::all();
        return view('teachers.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $appSettings = Cache::get('appSettings');
        if ($appSettings['view_job_apps'] != 'true'){
            abort(404);
        }
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "app_name"               => 'required|string|between:2,50',
            "app_email"              => 'required|email|between:6,100',
            "app_level_of_education" => 'nullable|between:2,30',
            "app_field_of_specialty" => 'nullable|between:3,30',
            "app_notes"              => 'nullable|between:3,500',
            "app_resume"             => 'file|mimetypes:application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document|mimes:doc,docx,pdf|max:2000',
        ]);
        $validator->setAttributeNames([
            "app_name"               => __('contact.your_name'),
            "app_email"              => __('contact.your_email'),
            "app_level_of_education" => __('teachers.level-of-education'),
            "app_field_of_study"     => __('teachers.fill-in-your-info'),
            "app_notes"              => __('teachers.notes'),
            "app_resume"             => __('teachers.upload-your-cv'),
        ]);

        if ($validator->fails()) {
            Session::flash('toast', [__('teachers.notif_not_sent'), 'error', __('toast.error')]);
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $file = $request->file('app_resume');

        $fileName = Application::getSanitizedFileName($file);
        $path = $file->storeAs('resumes', $fileName);

        Application::create([
            "name"               => $request->app_name,
            "email"              => $request->app_email,
            "level_of_education" => $request->app_level_of_education,
            "field-of_study"     => $request->app_field_of_study,
            "notes"              => $request->app_notes,
            "resume"             => $path,
        ]);
        Session::flash('toast', [__('teachers.notif_sent'), 'success', __('toast.success')]);

        return redirect()->back();
    }

}
