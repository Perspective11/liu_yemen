<?php

namespace App\Http\Controllers;

use App;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelLocalization;
use Session;
use Validator;
use View;

class PagesController extends Controller
{
    function __construct()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        $dir = LaravelLocalization::getCurrentLocaleDirection();
        $footerAnnouncements = \App\Announcement::featured()->take(App\Post::$LIMIT_FEATURED_ANNOUNCEMENTS)->get();
        Carbon::setLocale($lang);
        $contents = \App\Content::all()->keyBy('key')->toArray();
        $images = \App\ImageContent::all()->keyBy('key');
        View::share(compact('lang', 'dir', 'footerAnnouncements', 'contents', 'images'));
    }

    function index(){
        $events = App\Event::featured()->take(App\Event::$LIMIT_FEATURED_EVENTS)->get();
        $sliderPosts = App\Post::inSlider()->take(App\Post::$LIMIT_SLIDER_POSTS)->get();
        $featuredPosts = App\Post::featured()->take(App\Post::$LIMIT_FEATURED_POSTS)->get();
        if (! count($featuredPosts)){
            $featuredPosts = App\Post::published()->take(4)->get();
        }
        $departments = App\Department::with('majors')->get();
        $services = App\Service::all();
        $gallery = App\Picture::latest()->take(3)->get();
        return view('index', compact('events', 'featuredPosts', 'sliderPosts', 'departments', 'services', 'gallery'));
    }
    function gallery(){
        $gallery = App\Picture::with('tags')->where('imageable_type', '!=' , 'App\Facility')->get();
        foreach ($gallery as $picture){
            $picture['tag_id'] = $picture->getTagId();
        }
        $gallery = $gallery->toArray();
        $clubs = App\Club::whereNotNull('picture')->get();
        $madeUpClubTag = [
            'id'      => random_int(100000000, 900000000),
            'name_en' => 'Clubs',
            'name_ar' => 'النوادي',
        ];
        foreach ($clubs as $club){
            $element = [
                'name'       => $club->name_en,
                'image_path' => $club->picture,
                'thumb_path' => null,
                'tag_id'     => $madeUpClubTag['id'],
            ];
            array_push($gallery, $element);
        }
        $tags = App\Tag::whereHas('pictures')->select('id', 'name_en', 'name_ar')->get()->toArray();
        array_push($tags, $madeUpClubTag);
        return view('pages.gallery', compact('gallery', 'tags'));
    }
    function majors(){
        $departments = App\Department::with('majors')->get();
        return view('pages.majors', compact('departments'));
    }
    function calendar(){
        $eventCount = App\Event::all()->count();
        return view('calendar.index', compact('eventCount'));
    }
    function courses(){
        $appSettings = Cache::get('appSettings');
        if ($appSettings['view_job_apps'] != 'true'){
            abort(404);
        }
        $courses = App\Course::with('majors', 'courseType')->withCount('majors', 'courseType')->get();
        $majors = App\Major::has('courses')->get();
        return view('courses.index', compact('courses', 'majors'));
    }

    function contact(){
        return view('pages.contact');
    }
    function facilities(){
        $facilities = App\Facility::with('pictures')->paginate(5);
        return view('pages.facilities', compact('facilities'));
    }
    function about(){
        $posts = App\Post::inSlider()->take(3)->get();
        $testimonials = App\Testimonial::latest()->take(4)->get();

        return view('pages.about', compact('posts' , 'testimonials'));
    }
    function campusMap(){
        return view('pages.campus-map');
    }
    function privacyPolicy(){
        return view('pages.privacy-policy');
    }
    function credits(){
        return view('pages.credits');
    }
    function admission(){
        return view('pages.admission');
    }


    function storeContact(Request $request){
        $validator = Validator::make($request->all(), [
            "name"    => 'nullable|string|between:2,50',
            "email"   => 'required|email|between:6,100',
            "subject" => 'nullable|between:2,15',
            "message" => 'required|between:3,500',
        ]);
        $validator->setAttributeNames([
            "name"    => __('contact.your_name'),
            "email"   => __('contact.your_email'),
            "subject" => __('contact.subject'),
            "message" => __('contact.your_message'),
        ]);



        if ($validator->fails())
        {
            Session::flash('toast', [__('contact.notif_not_sent'), 'error', __('toast.error')]);

            if ($request->has('footer_submit')){
                $footerErrors = [];
                foreach ($validator->errors()->toArray() as $key => $error){
                    $footerErrors ['footer_'. $key] = $error;
                }
                return redirect()->back()->withErrors($footerErrors, 'footer')->withInput();
            }
            return redirect()->back()->withErrors($validator)->withInput();

        }

        App\Contact::create($request->all());
        Session::flash('toast', [__('contact.notif_sent'), 'success', __('toast.success')]);
        return redirect()->back();
    }
}
