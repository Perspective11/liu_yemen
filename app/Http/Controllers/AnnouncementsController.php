<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use LaravelLocalization;
use View;


class AnnouncementsController extends Controller
{
    public $lang;
    public $dir;

    function __construct()
    {
        $lang = LaravelLocalization::getCurrentLocale();
        $dir = LaravelLocalization::getCurrentLocaleDirection();
        $this->lang = $lang;
        $this->dir = $dir;
        $footerAnnouncements = \App\Announcement::featured()->take(Post::$TYPE_FEATURED_ANNOUNCEMENT)->get();
        Carbon::setLocale($this->lang);
        $contents = \App\Content::all()->keyBy('key')->toArray();
        $allAnnouncements = Announcement::published()->orderByDesc('created_at')->get();
        $groupedAnnouncements = $allAnnouncements->groupBy(function($e) {
            return Carbon::parse($e->created_at)->format('Y - m');
        });
        $images = \App\ImageContent::all()->keyBy('key');
        View::share(compact('lang', 'dir', 'footerAnnouncements', 'contents', 'allAnnouncements', 'groupedAnnouncements', 'images'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $announcements = Announcement::published();

        if ($request->filled('date')) {
            $parsedDate = Carbon::parse($request->date);
            $startDate = $parsedDate->toDateString();
            $afterMonth = $parsedDate->addMonth(1)->addDays(-1);
            $endDate = $afterMonth->toDateString();
            $announcements->whereBetween('created_at', [$startDate, $endDate]);
        }
        if ($request->filled('search')) {
            $announcements->search($request->search);
        }
        $announcements = $announcements->paginate();

        return view('announcements.index', compact('announcements'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $singleAnnouncement = Announcement::findOrFail($id);
        if ($singleAnnouncement->status !== 1)
            abort(404);
        return view('announcements.show', compact('singleAnnouncement'));
    }

}
