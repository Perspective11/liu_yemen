<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicesRequest;
use App\Picture;
use App\Service;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service = new Service;

        return view('admin.services.create', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServicesRequest $request)
    {
        $service = Service::create([
            'title_ar'       => $request->title_ar,
            'title_en'       => $request->title_en,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'icon'           => $request->icon,
        ]);

        Session::flash('toast', ['Service is Successfully Created', 'success']);
        return redirect("/admin/services");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServicesRequest $request, Service $service)
    {
        $service->update([
            'title_en'       => $request->title_en,
            'title_ar'       => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'icon'           => $request->icon,
        ]);
        Session::flash('toast','Service is Successfully Updated');
        return redirect("/admin/services");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        Session::flash('toast','Service is Successfully Deleted');
        return redirect('admin/services');
    }

    public function getServicesData(Request $request)
    {
        $services = Service::query();

        return DataTables::of($services)->editColumn('created_at', function ($service) {
            return $service->created_at->toFormattedDateString();
        })

        ->editColumn('icon', function ($service) {
            return '<div class="model-icon"><i class="fa ' . $service->icon . '"></i></div>';
        })

        ->rawColumns(['picture', 'icon'])

        ->make(true);
    }
}
