<?php

namespace App\Http\Controllers\admin;

use App\CourseType;
use App\Major;
use App\Http\Controllers\Controller;
use App\Http\Requests\CoursesRequest;
use App\Picture;
use App\Course;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.courses.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $course = new Course;
        $majors = Major::all();
        $courseTypes = CourseType::all();

        return view('admin.courses.create', compact('course', 'majors', 'courseTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CoursesRequest $request)
    {
        $course = Course::create([
            'code'           => $request->code,
            'name_ar'        => $request->name_ar,
            'name_en'        => $request->name_en,
            'credits'        => $request->credits,
            'order_number'   => $request->order_number,
            'course_type_id' => $request->course_type,
        ]);

        $course->majors()->attach($request->majors);

        Session::flash('toast', ['Course is Successfully Created', 'success']);
        return redirect("/admin/courses");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);
        return view('admin.courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        $majors = Major::all();
        $courseTypes = CourseType::all();
        return view('admin.courses.edit', compact('course', 'majors', 'courseTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CoursesRequest $request, Course $course)
    {
        $course->update([
            'code'           => $request->code,
            'name_ar'        => $request->name_ar,
            'name_en'        => $request->name_en,
            'credits'        => $request->credits,
            'order_number'   => $request->order_number,
            'course_type_id' => $request->course_type,
        ]);

        $course->majors()->sync($request->majors);

        Session::flash('toast','Course is Successfully Updated');
        return redirect("/admin/courses");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->majors()->detach();
        $course->delete();
        Session::flash('toast','Course is Successfully Deleted');
        return redirect('admin/courses');
    }

    public function getCoursesData(Request $request)
    {
        $courses = Course::with(['majors', 'courseType']);

        if ($request->filled('major')) {
            $courses->whereHas('majors', function ($query) use ($request) {
                $query->where('name_en', 'like', $request->major);
            });
        }
        if ($request->filled('course_type')) {
            $courses->whereHas('courseType', function ($query) use ($request) {
                $query->where('name_en', 'like', $request->course_type);
            });
        }

        return DataTables::of($courses)
        ->editColumn('created_at', function ($course) {
            return $course->created_at->toFormattedDateString();
            })

        ->addColumn('majors', function ($course) {
            return $course->majorsString();
        })
        ->addColumn('course_type', function ($course) {
            return $course->courseType->name_en . ' - ' . $course->courseType->name_ar;
        })
        ->make(true);
    }
}
