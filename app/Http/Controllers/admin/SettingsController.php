<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;
use App\Setting;
use Illuminate\Http\Request;
use Psy\Util\Json;
use Session;
use Validator;
use View;

class SettingsController extends Controller
{
    /**
     * SettingsController constructor.
     */
    public function __construct()
    {
        $contents = \App\Content::all()->keyBy('key')->toArray();
        View::share(compact('contents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settings = Setting::pluck('value', 'key')->toArray();
        $socialLinks = Setting::links();
        return view('admin.settings.edit' , compact('settings', 'socialLinks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SettingsRequest $request)
    {
        $linksArray = json_decode(Setting::where('key','social_links')->first()->value);
        $linksArray->facebook = $request->facebook_link;
        $linksArray->twitter = $request->twitter_link;
        $linksArray->instagram = $request->instagram_link;
        $linksArray->linkedin = $request->linkedin_link;

        if (Setting::where('key', '=', 'social_links')->exists()) {
            Setting::where('key', '=', 'social_links')->update(['value' => Json::encode($linksArray) ]);
        }
        else{
            Setting::create([
                'key'     => 'social_links',
                'name_en' => 'Social Media Links',
                'name_ar' => 'قنوات التواصل الاجتماعي',
                'value'   => Json::encode($linksArray),
            ]);
        }
        if ($request->filled('view_courses')){
            Setting::where('key', 'view_courses')->firstOrFail()->update(['value' => 'true']);
        }
        else{
            Setting::where('key', 'view_courses')->firstOrFail()->update(['value' => 'false']);
        }
        if ($request->filled('view_job_apps')){
            Setting::where('key', 'view_job_apps')->firstOrFail()->update(['value' => 'true']);
        }else{
            Setting::where('key', 'view_job_apps')->firstOrFail()->update(['value' => 'false']);

        }

        Setting::where('key', 'nav_phone')->firstOrFail()->update(['value' => $request->nav_phone]);
        Setting::where('key', 'nav_email')->firstOrFail()->update(['value' => $request->nav_email]);
        Setting::where('key', 'courses_phone')->firstOrFail()->update(['value' => $request->courses_phone]);
        Setting::where('key', 'contact_email')->firstOrFail()->update(['value' => $request->contact_email]);
        Setting::where('key', 'contact_phone')->firstOrFail()->update(['value' => $request->contact_phone]);
        Setting::where('key', 'home_metric_1')->firstOrFail()->update(['value' => $request->metric_value_1]);
        Setting::where('key', 'home_metric_2')->firstOrFail()->update(['value' => $request->metric_value_2]);
        Setting::where('key', 'home_metric_3')->firstOrFail()->update(['value' => $request->metric_value_3]);
        Setting::where('key', 'home_metric_4')->firstOrFail()->update(['value' => $request->metric_value_4]);

        Setting::where('key', 'metric_symbol_1')->firstOrFail()->update(['value' => $request->metric_symbol_1 ?: '']);
        Setting::where('key', 'metric_symbol_2')->firstOrFail()->update(['value' => $request->metric_symbol_2 ?: '']);
        Setting::where('key', 'metric_symbol_3')->firstOrFail()->update(['value' => $request->metric_symbol_3 ?: '']);
        Setting::where('key', 'metric_symbol_4')->firstOrFail()->update(['value' => $request->metric_symbol_4 ?: '']);




        $appSettings = Setting::pluck('value', 'key')->toArray();
        \Cache::forever('appSettings', $appSettings);
        
        
        Session::flash('toast', ['Values Updated Successfully', 'success']);


        return redirect('/admin/settings');
    }

}
