<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Tag;
use App\Picture;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use DataTables;
use Response;
use Session;
use Validator;

/**
 * Class PicturesController
 *
 * @package App\Http\Controllers\admin
 */
class PicturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pictures.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $picture = null;
        if ($request->filled('picture')){
            $picture = Picture::with('tags')->findOrFail($request->picture);
        }
        else{
            $picture = new Picture;
        }
        $singleTag = null;
        if ($request->filled('tag')){
            $singleTag = Tag::findOrFail($request->tag);
        }
        else{
            $singleTag = new Tag;
        }
        $tags = Tag::all();
        return view('admin.pictures.create', compact('picture', 'tags', 'singleTag'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Picture $picture)
    {
        return view('admin.pictures.show', compact('picture'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Picture $picture)
    {
        File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
        File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        $picture->delete();
        Session::flash('toast', ['Picture Successfully Deleted', 'success']);
        return redirect('admin/pictures');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadImagesStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image"  => 'required|mimes:jpg,png,jpeg|max:2000',
            "qquuid" => 'required',
            "name"   => 'nullable',
            'tag'    => 'nullable|integer|exists:tags,id',
        ]);
        $name = '';
        if ($request->filled('name'))
        {
            $name = $request->name . ' _ ' . str_random(5);
        }
        $temp_uuid = '';
        if ($request->filled('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }

        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_' . Picture::getSanitizedName($image);
            $picture_path = 'images/events/' . $picture_name;
            $thumbnail_path = 'images/events/' . $thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/' . $picture_path;
            $thumbnail_path = '/' . $thumbnail_path;
            $singlePicture = Picture::create([
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_uuid'  => $temp_uuid,
                'name'       => $name? $name : $picture_name,
            ]);
            $singlePicture->tags()->attach($request->tag);
        }
        return Response::json(["success" => true ,"class" => "success", "message" => "Upload Successful"], 201);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadedImagesUpdateInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "images" => 'required|array',
            "name"   => 'nullable',
            "tag"    => 'nullable|integer|exists:tags,id',
        ]);
        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }
        $name = '';
        if ($request->name){
            $name = $request->name;
            $sanitizedName = str_limit( preg_replace( '/[^a-z0-9]+/', '-', strtolower($name)), 20 , '');
            $name = Carbon::now()->timestamp . '_' . str_random(5) . "_" . $sanitizedName . '.';
        }

        foreach ($request->images as $picture){
            $picture = Picture::where('temp_uuid', $picture['uuid'])->first();
            if ($name){
                $picture->update([
                    'name' => $name
                ]);
            }
            $picture->tags()->sync($request->tag);
        }

        return Response::json(["success" => true ,"class" => "success", "message" => "Update Successful"], 201);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listPictureById(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "picture_id" => 'required|integer|exists:pictures,id',
        ]);
        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }

        $picture = Picture::findOrFail($request->picture_id);
        $returnedImages[] = [
            "id"=> $picture->id,
            "name"=> $picture->key? $picture->key : '',
            "uuid"=> $picture->temp_uuid,
            "thumbnailUrl"=> $picture->thumb_path
        ];
        return Response::json($returnedImages);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listPicturesByCat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "cat" => 'required|integer|exists:tags,id',
        ]);
        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }
        $tag = Tag::findOrFail($request->cat);

        $returnedImages = [];
        $pictures = $tag->pictures;
        foreach ($pictures as $picture){
            $returnedImages[] = [
                "id"=> $picture->id,
                "name"=> $picture->name? $picture->name : '',
                "uuid"=> $picture->temp_uuid,
                "thumbnailUrl"=> $picture->thumb_path
            ];
        }
        return Response::json($returnedImages);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function DeleteImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "qquuid" => 'required',
        ]);
        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }
        $temp_uuid = '';
        if ($request->has('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }
        $picture = Picture::where('temp_uuid' , $temp_uuid)->get()->first();
        if ($picture->image_path)
        {
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
        }
        if ($picture->thumb_path)
        {
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        }
        $picture->delete();

        return Response::json(["success" => true ,"class" => "success", "message" => "Image Deleted Successfully"], 201);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getPicturesData(Request $request)
    {
        $pictures = Picture::with(['tags']);

        if ($request->filled('tag'))
        {
            $pictures->whereHas('tags', function ($query) use ($request) {
                return $query->where('name_en', 'like', $request->tag);
            });
        }

        return DataTables::of($pictures)->editColumn('created_at', function ($picture) {
            return $picture->created_at->toFormattedDateString();
        })->editColumn('picture', function ($picture) {
            return "<img class='table-img img-responsive' src='$picture->thumb_path' alt='$picture->name'>";
        })->addColumn('tag', function ($picture) {
            return $picture->getTag();
        })->rawColumns(['picture'])->make(true);
    }
}
