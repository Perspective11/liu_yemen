<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClubsRequest;
use App\Picture;
use App\Club;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class ClubsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.clubs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $club = new Club;

        return view('admin.clubs.create', compact('club'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClubsRequest $request)
    {

        $image_path = '';
        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/clubs/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $image_path = '/' . $image_path;
        }

        $club = Club::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'description_en'  => $request->description_en,
            'description_ar'  => $request->description_ar,
            'picture'  => $image_path,
        ]);

        Session::flash('toast', ['Club is Successfully Created', 'success']);
        return redirect("/admin/clubs");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $club = Club::findOrFail($id);
        return view('admin.clubs.show', compact('club'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Club $club)
    {
        return view('admin.clubs.edit', compact('club'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClubsRequest $request, Club $club)
    {
        if ($request->hasFile('picture')) {
            File::delete(public_path(str_replace('/', '\\', $club->picture)));
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/clubs/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $club->update([
                'picture' => '/'.$image_path,
            ]);
        }
        $club->update([
            'name_en'  => $request->name_en,
            'name_ar'  => $request->name_ar,
            'description_en'   => $request->description_en,
            'description_ar'   => $request->description_ar,
        ]);
        Session::flash('toast','Club is Successfully Updated');
        return redirect("/admin/clubs");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Club $club)
    {
        File::delete(public_path(str_replace('/', '\\', $club->picture)));
        $club->delete();
        Session::flash('toast','Club is Successfully Deleted');
        return redirect('admin/clubs');
    }

    public function getClubsData(Request $request)
    {
        $clubs = Club::query();

        return DataTables::of($clubs)->editColumn('created_at', function ($club) {
            return $club->created_at->toFormattedDateString();
        })->editColumn('picture', function ($club) {
            return "<img class='table-img img-responsive' src='" . $club->getPicture() . "' alt='$club->title'>";

        })->rawColumns(['picture'])
            ->make(true);
    }
}
