<?php

namespace App\Http\Controllers\admin;

use App\Application;
use Carbon\Carbon;
use DataTables;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Storage;

class ApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.job-applications.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        $application->delete();
        Session::flash('toast', 'Application Successfully Deleted');
        return redirect('/admin/applications');
    }

    public function viewResume(Application $application)
    {
        $file = Storage::get($application->resume);
        $mimeType = Storage::mimeType($application->resume);
        return response($file, 200, [
            'Content-Type' => $mimeType,
        ]);
    }
    public function downloadResume(Application $application)
    {
        return Storage::download($application->resume);
    }

    public function getApplicationsData(Request $request){
        $applications = Application::query();

        if ($request->filled('daterange')) {
            $arr = explode(' - ', request('daterange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $applications->whereBetween('created_at', [$startDate, $endDate]);
        }
        return Datatables::of($applications)
            ->editColumn('created_at', function ($application) {
                return $application->created_at->toFormattedDateString();
            })
            ->editColumn('resume', function ($application) {
                $download_url = $application->path(true) . '/download-resume';
                $view_url = $application->path(true) . '/view-resume';
                return "<div class='text-center'><a href='$download_url' target='_blank' class='btn btn-info btn-xs'><i class='fa fa-download'></i></a>" .
                    "<a href='$view_url' target='_blank' class='btn btn-success btn-xs'><i class='fa fa-eye'></i></a></div>";
            })
            ->rawColumns(['resume'])
            ->make(true);
    }
}
