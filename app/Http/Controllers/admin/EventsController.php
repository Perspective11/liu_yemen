<?php

namespace App\Http\Controllers\admin;

use App\Club;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventsRequest;
use App\Picture;
use App\Event;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use DataTables;
use Ramsey\Uuid\Uuid;
use Response;
use Session;
use Validator;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.events.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = new Event;
        $clubs = Club::all();
        $uuid = Uuid::uuid4();
        $temp_token = $uuid->toString();
        return view('admin.events.create', compact('event', 'clubs', 'temp_token'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventsRequest $request)
    {

        $image_path = '';
        if ($request->hasFile('poster')) {
            $image = $request->file('poster');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/events/poster_' . $image_name;
            $poster = Picture::modifyImage($image);
            $poster->save($image_path);
            $image_path = '/' . $image_path;
        }

        $club = 0;
        if ($request->has('club')){
            $club = $request->club;
        }
        else{
            $club = null;
        }
        $event = Event::create([
            'title_ar'    => $request->title_ar,
            'title_en'    => $request->title_en,
            'body_en'     => $request->body_en,
            'body_ar'     => $request->body_ar,
            'location_en' => $request->location_en,
            'location_ar' => $request->location_ar,
            'date'        => $request->date,
            'type'        => 0,
            'status'      => 0,
            'poster_path' => $image_path,
            'club_id'     => $club,
        ]);

        $temp_token = '';
        if ($request->has('temp'))
        {
            $temp_token = $request->temp;
        }

        $pictures = Picture::where('temp_token', $temp_token)->get();
        for ($i = 0; $i < $pictures->count(); $i++){
            if ($i >= Event::$image_limit){
                break;
            }
            $event->pictures()->save($pictures[$i]);
            $pictures[$i]->update(['temp_token' => null]);
        }
        Picture::deleteTemp();
        Session::flash('toast', ['Event is Successfully Created', 'success']);
        return redirect("/admin/events");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        return view('admin.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $clubs = Club::all();
        $event->load('pictures');
        $event_id = $event->id;
        return view('admin.events.edit', compact('event' , 'clubs', 'event_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventsRequest $request, Event $event)
    {
        if ($request->hasFile('poster')) {
            File::delete(public_path(str_replace('/', '\\', $event->poster_path)));
            $image = $request->file('poster');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/events/poster_' . $image_name;
            $poster = Picture::modifyImage($image);
            $poster->save($image_path);
            $event->update([
                'poster_path' => '/' . $image_path,
            ]);
        }
        if ($request->has('delete_poster')){
            File::delete(public_path(str_replace('/', '\\', $event->poster_path)));
            $event->update([
                'poster_path' => null,
            ]);
        }
        $club = 0;
        if ($request->has('club')){
            $club = $request->club;
        }
        else{
            $club = null;
        }

        $event->update([
            'title_en'    => $request->title_en,
            'title_ar'    => $request->title_ar,
            'body_en'     => $request->body_en,
            'body_ar'     => $request->body_ar,
            'location_en' => $request->location_en,
            'location_ar' => $request->location_ar,
            'date'        => $request->date,
            'club_id'     => $club,
        ]);
        Session::flash('toast','Event is Successfully Updated');
        return redirect("/admin/events");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        foreach ($event->pictures as $picture){
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        }
        if ($event->poster_path){
            File::delete(public_path(str_replace('/', '\\', $event->poster_path)));
        }
        $event->pictures()->delete();
        $event->delete();
        Session::flash('toast','Event is Successfully Deleted');
        return redirect('admin/events');
    }
    public function activation(Event $event)
    {
        $event->togglePublished();

        return back();
    }

    public function featured(Event $event)
    {
        $event->toggleFeatured();

        return back();
    }

    public function uploadImagesStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image" => 'required|mimes:jpg,png,jpeg|max:2000',
            "temp" => 'required',
            "qquuid" => 'required',
        ]);
        $temp_token = '';
        if ($request->has('temp'))
        {
            $temp_token = $request->temp;
        }
        $temp_uuid = '';
        if ($request->has('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }
        if ($temp_token){
            $validator->after(function ($validator) use ($temp_token){
                if (Picture::where('temp_token', $temp_token)->count() >= Event::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than ' . Event::$image_limit . ' images!');
                }
            });
        }


        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_' . Picture::getSanitizedName($image);
            $picture_path = 'images/events/' . $picture_name;
            $thumbnail_path = 'images/events/' . $thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/' . $picture_path;
            $thumbnail_path = '/' . $thumbnail_path;
            Picture::create([
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_token' => $temp_token,
                'temp_uuid' => $temp_uuid,
                'name_en' => $picture_name
            ]);
        }
        return Response::json(["success" => true ,"class" => "success", "message" => "Upload Successful"], 201);
    }

    public function uploadImagesUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image" => 'required|mimes:jpg,png,jpeg|max:2000',
            "qquuid" => 'required',
            "event_id" => 'required',
        ]);

        $temp_uuid = '';
        if ($request->has('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }

        $event = Event::findOrFail($request->event_id);

        if (isset($event)){
            $validator->after(function ($validator) use ($event) {
                if ($event->pictures()->count() > Event::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than ' . Event::$image_limit . ' images!');
                }
            });
        }

        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_' . Picture::getSanitizedName($image);
            $picture_path = 'images/events/' . $picture_name;
            $thumbnail_path = 'images/events/' . $thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/' . $picture_path;
            $thumbnail_path = '/' . $thumbnail_path;
            $event->pictures()->create([
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_uuid' => $temp_uuid,
                'name_en' => $picture_name
            ]);
        }
        return Response::json(["success" => true ,"class" => "success", "message" => "Upload Successful"], 201);
    }

    public function listEventImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "event_id" => 'required',
        ]);

        $event = Event::findOrFail($request->event_id);

        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }
        $returnedImages = [];
        $pictures = $event->pictures;
        foreach ($pictures as $picture){
            $returnedImages[] = [
                "id"=> $picture->id,
                "name"=> $picture->name? $picture->name : '',
                "uuid"=> $picture->temp_uuid,
                "thumbnailUrl"=> $picture->thumb_path
            ];
        }


        return Response::json($returnedImages);
    }


    public function DeleteImage(Request $request)
    {
        $temp_uuid = '';
        if ($request->has('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }
        $picture = Picture::where('temp_uuid' , $temp_uuid)->get()->first();
        if ($picture->image_path)
        {
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
        }
        if ($picture->thumb_path)
        {
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        }
        $picture->delete();

        return Response::json(["success" => true ,"class" => "success", "message" => "Image Deleted Successfully"], 201);
    }


    public function getEventsData(Request $request)
    {
        $events = Event::with('club');

        if ($request->has('status'))
        {
            if ($request->status == 'published')
                $events->where('status', 1);
            else if ($request->status == 'unpublished')
                $events->where('status', 0);
        }
        if ($request->has('type'))
        {
            if ($request->type == 'featured')
                $events->where('type', Event::$TYPE_FEATURED_EVENT);
            else if ($request->type == 'unfeatured')
                $events->where('type', Event::$TYPE_FEATURED_EVENT);
        }
        if ($request->filled('daterange')) {
            $arr = explode(' - ', request('daterange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $events->whereBetween('date', [$startDate, $endDate]);
        }
        if ($request->filled('club')) {
            $events->whereHas('club', function ($query) use ($request) {
                $query->where('name_en', 'like', $request->club);
            });
        }
        if ($request->filled('time')) {
            if ($request->time == 'upcoming'){
                $events->upcoming();
            }
            elseif ($request->time == 'past'){
                $events->past();
            }
        }
        return DataTables::of($events)
            ->editColumn('date', function ($event) {
                return $event->date->toFormattedDateString();
            })->addColumn('club', function ($event) {
                if ($event->club){
                    return $event->club->name_en . ' - ' . $event->club->name_ar;
                }
                else
                    return '';
            })->editColumn('created_at', function ($event) {
                return $event->created_at->toFormattedDateString();
            })->editColumn('picture', function ($event) {
                return "<img class='table-img img-responsive' src='".$event->getPicture()."' alt='$event->title'>";
            })->editColumn('status', function ($event) {
            $class = $event->status ? 'success' : 'danger';
            $status = $event->status ? 'Unpublish' : 'Publish';
            $featureClass= $event->type ===  Event::$TYPE_FEATURED_EVENT ? 'warning' : 'default';
            $featured = $event->type === Event::$TYPE_FEATURED_EVENT ? 'Unfeature' : 'Feature';

            return '<div class="text-center">' .
                '<a data-toggle="tooltip" title="Click to ' . $status . ' the event" href="' . $event->path(true) . '/activation" class="btn btn-xs event-status btn-' . $class . '"><i class="fa fa-rss"></i> ' . $status . '</a>' .
                '<a data-toggle="tooltip" title="Click to ' . $featured . ' the event" href="' . $event->path(true) . '/featured" class="btn btn-xs event-featured btn-' . $featureClass . '"><i class="fa fa-star"></i> ' . $featured . '</a>' .
                '</div>';

        })->rawColumns(['body_en', 'body_ar', 'picture', 'status'])
            ->make(true);
    }
}
