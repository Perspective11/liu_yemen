<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostsRequest;
use App\Picture;
use App\Announcement;
use App\Post;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.announcements.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $announcement = new Announcement;

        return view('admin.announcements.create', compact('announcement'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {

        $image_path = '';
        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/announcements/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $image_path = '/' . $image_path;
        }

        $announcement = Announcement::create([
            'title_ar' => $request->title_ar,
            'title_en' => $request->title_en,
            'type'     => 1,
            'body_en'  => $request->body_en,
            'body_ar'  => $request->body_ar,
            'picture'  => $image_path,
            'status'   => 0,

        ]);

        Session::flash('toast', ['Announcement is Successfully Created', 'success']);
        return redirect("/admin/announcements");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $announcement = Announcement::findOrFail($id);
        return view('admin.announcements.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        return view('admin.announcements.edit', compact('announcement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsRequest $request, Announcement $announcement)
    {
        if ($request->hasFile('picture')) {
            File::delete(public_path(str_replace('/', '\\', $announcement->picture)));
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/announcements/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $announcement->update([
                'picture' => '/'.$image_path,
            ]);
        }
        $announcement->update([
            'title_en'  => $request->title_en,
            'title_ar'  => $request->title_ar,
            'body_en'   => $request->body_en,
            'body_ar'   => $request->body_ar,
        ]);
        Session::flash('toast','Announcement is Successfully Updated');
        return redirect("/admin/announcements");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        File::delete(public_path(str_replace('/', '\\', $announcement->picture)));
        $announcement->delete();
        Session::flash('toast','Announcement is Successfully Deleted');
        return redirect('admin/announcements');
    }
    public function activation(Announcement $announcement)
    {
        $announcement->togglePublished();

        return back();
    }

    public function featured(Announcement $announcement)
    {
        $announcement->toggleFeatured();

        return back();
    }
    public function getAnnouncementsData(Request $request)
    {
        $announcements = Announcement::query();

        if ($request->filled('status'))
        {
            if ($request->status == 'published')
                $announcements->where('status', 1);
            else if ($request->status == 'unpublished')
                $announcements->where('status', 0);
        }
        if ($request->filled('daterange')) {
            $arr = explode(' - ', request('daterange'));
            $startDate = Carbon::parse($arr[0])->toDateString();
            $endDate = Carbon::parse($arr[1])->addDay()->toDateString();

            $announcements->whereBetween('created_at', [$startDate, $endDate]);
        }

        if ($request->filled('type'))
        {
            if ($request->type == 'featured')
                $announcements->featured();
            else if ($request->type == 'unfeatured')
                $announcements->where('type', '!=' , Post::$TYPE_FEATURED_ANNOUNCEMENT);
        }

        return DataTables::of($announcements)->editColumn('created_at', function ($announcement) {
            return $announcement->created_at->toFormattedDateString();
        })->editColumn('picture', function ($announcement) {
            return "<img class='table-img img-responsive' src='" . $announcement->getPicture() . "' alt='$announcement->title'>";
        })->editColumn('status', function ($announcement) {
            $class = $announcement->status ? 'success' : 'danger';
            $status = $announcement->status ? 'Unpublish' : 'Publish';
            $featureClass= $announcement->type === Post::$TYPE_FEATURED_ANNOUNCEMENT ? 'warning' : 'default';
            $featured = $announcement->type === Post::$TYPE_FEATURED_ANNOUNCEMENT ? 'Unfeature' : 'Feature';

            return '<div class="text-center">' .
                '<a data-toggle="tooltip" title="Click to ' . $status . ' the announcement" href="' . $announcement->path(true) . '/activation" class="btn btn-xs announcement-status btn-' . $class . '"><i class="fa fa-rss"></i> ' . $status . '</a>' .
                '<a data-toggle="tooltip" title="Click to ' . $featured . ' the announcement" href="' . $announcement->path(true) . '/featured" class="btn btn-xs announcement-featured btn-' . $featureClass . '"><i class="fa fa-star"></i> ' . $featured . '</a>' .
                '</div>';

        })
            ->rawColumns(['body_en', 'body_ar', 'picture', 'status'])
            ->make(true);
    }
}
