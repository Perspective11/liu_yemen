<?php

namespace App\Http\Controllers\admin;

use App\Announcement;
use App\Http\Controllers\Controller;
use App\Post;
use Carbon\Carbon;
use App\Event;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recentPosts = Post::latest()->take(6)->get();
        $recentAnnouncements = Announcement::latest()->take(6)->get();
        $recentEvents = Event::latest()->take(6)->get();
        return view('admin.home', compact('recentEvents', 'recentAnnouncements', 'recentPosts'));
    }
}
