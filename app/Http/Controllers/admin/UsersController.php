<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsersRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DataTables;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        return view('admin.users.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $user = User::create([
            'name'        => $request->name,
            'email'       => $request->email,
            'password'    => bcrypt($request->password),
        ]);
        return redirect("/admin/users");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersRequest $request, User $user)
    {
        $userArray = [
            'name'        => $request->name,
            'email'       => $request->email,
        ];
        if ($request['password']) // because nullable rule doesnt fucking work
        {
            $userArray['password'] = bcrypt($request->password);
        }
        $user->update($userArray);

        return redirect("/admin/users");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect('admin/users');
    }

    public function changePassword()
    {
        $user = Auth::user();

        return view('admin.users.change-password', compact('user'));
    }

    public function changePasswordStore(Request $request)
    {
        $user = Auth::user();
        Validator::make($request->all(), [
            'old_password' => 'required',
            'password'     => 'min:6|confirmed|required',
        ])->validate();

        if (!Hash::check($request->old_password, Auth::user()->password))
        {
            return redirect()->back()->withErrors(['old_password' => 'Old password is invalid.']);
        }

        $user->update([
            'password' => bcrypt($request->password),
        ]);

        return redirect('/admin/users');
    }

    public function getUsersData(Request $request)
    {
        $users = User::select(['id', 'name', 'email', 'created_at']);

        return DataTables::of($users)
            ->editColumn('created_at', function ($user)
            {
                return $user->created_at->toFormattedDateString();
            })
            ->make(true);
    }
}
