<?php

namespace App\Http\Controllers\admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\MajorsRequest;
use App\Picture;
use App\Major;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class MajorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.majors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $major = new Major;
        $departments = Department::all();

        return view('admin.majors.create', compact('major', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MajorsRequest $request)
    {
        $department = 0;
        if ($request->has('department')){
            $department = $request->department;
        }

        $major = Major::create([
            'code' => $request->code,
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'credits' => $request->credits,
            'department_id' => $department,
        ]);

        Session::flash('toast', ['Major is Successfully Created', 'success']);
        return redirect("/admin/majors");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $major = Major::findOrFail($id);
        return view('admin.majors.show', compact('major'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Major $major)
    {
        $departments = Department::all();
        return view('admin.majors.edit', compact('major', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MajorsRequest $request, Major $major)
    {
        $department = 0;
        if ($request->has('department')){
            $department = $request->department;
        }
        $major->update([
            'code' => $request->code,
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'credits' => $request->credits,
            'department_id' => $department,
        ]);
        Session::flash('toast','Major is Successfully Updated');
        return redirect("/admin/majors");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Major $major)
    {
        $major->delete();
        Session::flash('toast','Major is Successfully Deleted');
        return redirect('admin/majors');
    }

    public function getMajorsData(Request $request)
    {
        $majors = Major::with('department');

        if ($request->filled('department')) {
            $majors->whereHas('department', function ($query) use ($request) {
                $query->where('name_en', 'like', $request->department);
            });
        }

        return DataTables::of($majors)
        ->editColumn('created_at', function ($major) {
            return $major->created_at->toFormattedDateString();
            })

        ->addColumn('department', function ($event) {
            if ($event->department){
                return $event->department->name_en . ' - ' . $event->department->name_ar;
            }
            else
                return '';
            })
        ->make(true);
    }
}
