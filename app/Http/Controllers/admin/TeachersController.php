<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeachersRequest;
use App\Picture;
use App\Teacher;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.teachers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = new Teacher;

        return view('admin.teachers.create', compact('teacher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeachersRequest $request)
    {

        $image_path = '';
        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/teachers/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $image_path = '/' . $image_path;
        }

        $teacher = Teacher::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'title_en'  => $request->title_en,
            'title_ar'  => $request->title_ar,
            'picture'  => $image_path,
        ]);

        Session::flash('toast', ['Teacher is Successfully Created', 'success']);
        return redirect("/admin/teachers");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = Teacher::findOrFail($id);
        return view('admin.teachers.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('admin.teachers.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeachersRequest $request, Teacher $teacher)
    {
        if ($request->hasFile('picture')) {
            File::delete(public_path(str_replace('/', '\\', $teacher->picture)));
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/teachers/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $teacher->update([
                'picture' => '/'.$image_path,
            ]);
        }
        $teacher->update([
            'name_en'  => $request->name_en,
            'name_ar'  => $request->name_ar,
            'title_en'   => $request->title_en,
            'title_ar'   => $request->title_ar,
        ]);
        Session::flash('toast','Teacher is Successfully Updated');
        return redirect("/admin/teachers");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        File::delete(public_path(str_replace('/', '\\', $teacher->picture)));
        $teacher->delete();
        Session::flash('toast','Teacher is Successfully Deleted');
        return redirect('admin/teachers');
    }

    public function getTeachersData(Request $request)
    {
        $teachers = Teacher::query();

        return DataTables::of($teachers)->editColumn('created_at', function ($teacher) {
            return $teacher->created_at->toFormattedDateString();
        })->editColumn('picture', function ($teacher) {
            return "<img class='table-img img-responsive' src='" . $teacher->getPicture() . "' alt='$teacher->title'>";

        })->rawColumns(['picture'])
            ->make(true);
    }
}
