<?php

namespace App\Http\Controllers\admin;

use App\ImageContent;
use App\Http\Controllers\Controller;
use App\Picture;
use DB;
use Illuminate\Http\Request;
use Session;

class ImageContentsController extends Controller
{
    public function show()
    {
        $imageContents = ImageContent::all();
        return view('admin.image-contents.show' , compact('imageContents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $imageContents = ImageContent::all();
        return view('admin.image-contents.edit' , compact('imageContents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $paths = $request->updated_path;
        $keys = $request->key;
        $updatedCounter = 0;
        for ($i = 0; $i <= count($keys); $i++) {
            if (! isset($paths[$i])){
                continue;
            }
            $image = $paths[$i];
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_' . Picture::getSanitizedName($image);
            $picture_path = 'images/image_contents/' . $picture_name;
            $thumbnail_path = 'images/image_contents/' . $thumbnail_name;
            $pic = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $pic->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/' . $picture_path;
            $thumbnail_path = '/' . $thumbnail_path;
            ImageContent::where('key', $keys[$i])->first()->update([
                "image_path" => $picture_path,
                "thumb_path" => $thumbnail_path,
            ]);
            $updatedCounter ++;
        }
        if (! $updatedCounter){
            Session::flash('toast', ['No Image Contents Were Updated', 'info']);
        }
        else
            Session::flash('toast', [$updatedCounter . ' Image Contents Successfully Updated', 'success']);
        return redirect()->back();
    }


}
