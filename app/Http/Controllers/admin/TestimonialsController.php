<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TestimonialsRequest;
use App\Picture;
use App\Testimonial;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.testimonials.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $testimonial = new Testimonial;

        return view('admin.testimonials.create', compact('testimonial'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestimonialsRequest $request)
    {

        $image_path = '';
        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/testimonials/'.$image_name;
            $picture = Picture::modifyImage($image, 500, true);
            $picture->save($image_path);
            $image_path = '/' . $image_path;
        }

        $testimonial = Testimonial::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'quote_en'  => $request->quote_en,
            'quote_ar'  => $request->quote_ar,
            'picture'  => $image_path,
        ]);

        Session::flash('toast', ['Testimonial is Successfully Created', 'success']);
        return redirect("/admin/testimonials");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $testimonial = Testimonial::findOrFail($id);
        return view('admin.testimonials.show', compact('testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        return view('admin.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TestimonialsRequest $request, Testimonial $testimonial)
    {
        if ($request->hasFile('picture')) {
            File::delete(public_path(str_replace('/', '\\', $testimonial->picture)));
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/testimonials/'.$image_name;
            $picture = Picture::modifyImage($image, 500, true);
            $picture->save($image_path);
            $testimonial->update([
                'picture' => '/'.$image_path,
            ]);
        }
        $testimonial->update([
            'name_en'  => $request->name_en,
            'name_ar'  => $request->name_ar,
            'quote_en'   => $request->quote_en,
            'quote_ar'   => $request->quote_ar,
        ]);
        Session::flash('toast','Testimonial is Successfully Updated');
        return redirect("/admin/testimonials");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        File::delete(public_path(str_replace('/', '\\', $testimonial->picture)));
        $testimonial->delete();
        Session::flash('toast','Testimonial is Successfully Deleted');
        return redirect('admin/testimonials');
    }

    public function getTestimonialsData(Request $request)
    {
        $testimonials = Testimonial::query();

        return DataTables::of($testimonials)->editColumn('created_at', function ($testimonial) {
            return $testimonial->created_at->toFormattedDateString();
        })->editColumn('picture', function ($testimonial) {
            return "<img class='table-img img-responsive img-circle' src='" . $testimonial->getPicture() . "' alt='$testimonial->title'>";

        })->rawColumns(['picture'])
            ->make(true);
    }
}
