<?php

namespace App\Http\Controllers\admin;

use App\Club;
use App\Http\Controllers\Controller;
use App\Http\Requests\FacilitiesRequest;
use App\Picture;
use App\Facility;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use DataTables;
use Ramsey\Uuid\Uuid;
use Response;
use Session;
use Validator;

class FacilitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.facilities.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $facility = new Facility;
        $uuid = Uuid::uuid4();
        $temp_token = $uuid->toString();
        return view('admin.facilities.create', compact('facility', 'temp_token'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FacilitiesRequest $request)
    {
        $facility = Facility::create([
            'name_ar'    => $request->name_ar,
            'name_en'    => $request->name_en,
            'description_en'     => $request->description_en,
            'description_ar'     => $request->description_ar,
        ]);

        $temp_token = '';
        if ($request->has('temp'))
        {
            $temp_token = $request->temp;
        }

        $pictures = Picture::where('temp_token', $temp_token)->get();
        for ($i = 0; $i < $pictures->count(); $i++){
            if ($i >= Facility::$image_limit){
                break;
            }
            $facility->pictures()->save($pictures[$i]);
            $pictures[$i]->update(['temp_token' => null]);
        }
        Picture::deleteTemp();
        Session::flash('toast', ['Facility is Successfully Created', 'success']);
        return redirect("/admin/facilities");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $facility = Facility::findOrFail($id);
        $facility->load('pictures');
        return view('admin.facilities.show', compact('facility'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility)
    {
        $facility->load('pictures');
        $facility_id = $facility->id;
        return view('admin.facilities.edit', compact('facility', 'facility_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FacilitiesRequest $request, Facility $facility)
    {
        $facility->update([
            'name_en'        => $request->name_en,
            'name_ar'        => $request->name_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
        ]);
        Session::flash('toast','Facility is Successfully Updated');
        return redirect("/admin/facilities");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility)
    {
        foreach ($facility->pictures as $picture){
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        }
        $facility->pictures()->delete();
        $facility->delete();
        Session::flash('toast','Facility is Successfully Deleted');
        return redirect('admin/facilities');
    }

    public function uploadImagesStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image" => 'required|mimes:jpg,png,jpeg|max:2000',
            "temp" => 'required',
            "qquuid" => 'required',
        ]);
        $temp_token = '';
        if ($request->has('temp'))
        {
            $temp_token = $request->temp;
        }
        $temp_uuid = '';
        if ($request->has('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }
        if ($temp_token){
            $validator->after(function ($validator) use ($temp_token){
                if (Picture::where('temp_token', $temp_token)->count() >= Facility::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than ' . Facility::$image_limit . ' images!');
                }
            });
        }


        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_' . Picture::getSanitizedName($image);
            $picture_path = 'images/facilities/' . $picture_name;
            $thumbnail_path = 'images/facilities/' . $thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/' . $picture_path;
            $thumbnail_path = '/' . $thumbnail_path;
            Picture::create([
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_token' => $temp_token,
                'temp_uuid' => $temp_uuid,
                'name_en' => $picture_name
            ]);
        }
        return Response::json(["success" => true ,"class" => "success", "message" => "Upload Successful"], 201);
    }

    public function uploadImagesUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "image" => 'required|mimes:jpg,png,jpeg|max:2000',
            "qquuid" => 'required',
            "facility_id" => 'required',
        ]);

        $temp_uuid = '';
        if ($request->has('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }

        $facility = Facility::findOrFail($request->facility_id);

        if (isset($facility)){
            $validator->after(function ($validator) use ($facility) {
                if ($facility->pictures()->count() > Facility::$image_limit) {
                    $validator->errors()->add('image', 'You cant upload more than ' . Facility::$image_limit . ' images!');
                }
            });
        }

        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $picture_name = Picture::getSanitizedName($image);
            $thumbnail_name = 'thumb_' . Picture::getSanitizedName($image);
            $picture_path = 'images/facilities/' . $picture_name;
            $thumbnail_path = 'images/facilities/' . $thumbnail_name;
            $picture = Picture::modifyImage($image);
            $thumbnail = Picture::makeThumbnail($image);
            $picture->save($picture_path);
            $thumbnail->save($thumbnail_path);
            $picture_path = '/' . $picture_path;
            $thumbnail_path = '/' . $thumbnail_path;
            $facility->pictures()->create([
                'image_path' => $picture_path,
                'thumb_path' => $thumbnail_path,
                'temp_uuid' => $temp_uuid,
                'name_en' => $picture_name
            ]);
        }
        return Response::json(["success" => true ,"class" => "success", "message" => "Upload Successful"], 201);
    }

    public function listFacilityImages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "facility_id" => 'required',
        ]);

        $facility = Facility::findOrFail($request->facility_id);

        if ($validator->fails())
        {
            return response()->json(implode( '
            ' ,array_flatten( $validator->messages()->toArray() )) , 500);
        }
        $returnedImages = [];
        $pictures = $facility->pictures;
        foreach ($pictures as $picture){
            $returnedImages[] = [
                "id"=> $picture->id,
                "name"=> $picture->name? $picture->name : '',
                "uuid"=> $picture->temp_uuid,
                "thumbnailUrl"=> $picture->thumb_path
            ];
        }


        return Response::json($returnedImages);
    }


    public function DeleteImage(Request $request)
    {
        $temp_uuid = '';
        if ($request->has('qquuid'))
        {
            $temp_uuid = $request->qquuid;
        }
        $picture = Picture::where('temp_uuid' , $temp_uuid)->get()->first();
        if ($picture->image_path)
        {
            File::delete(public_path(str_replace('/', '\\', $picture->image_path)));
        }
        if ($picture->thumb_path)
        {
            File::delete(public_path(str_replace('/', '\\', $picture->thumb_path)));
        }
        $picture->delete();

        return Response::json(["success" => true ,"class" => "success", "message" => "Image Deleted Successfully"], 201);
    }


    public function getFacilitiesData(Request $request)
    {
        $facilities = Facility::query();

        return DataTables::of($facilities)
            ->editColumn('created_at', function ($facility) {
                return $facility->created_at->toFormattedDateString();
            })->editColumn('picture', function ($facility) {
                return "<img class='table-img img-responsive' src='".$facility->getPicture()."' alt='$facility->title'>";
            })->rawColumns(['picture'])
            ->make(true);
    }
}
