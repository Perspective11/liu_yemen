<?php

namespace App\Http\Controllers\admin;

use App\Event;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;

/**
 * Class CalendarController
 *
 * @package App\Http\Controllers
 */
class CalendarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return view('admin.calendar.index', compact('events'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function getEvents(Request $request)
    {
        $arr = [];
        $events = Event::whereBetween('date', [$request->start, $request->end])->each(function ($event) use (&$arr) {
            $color = '#4286f4';
            if ($event->status === 0){
                $color = 'grey';
            }
            array_push($arr, [
                'id'    => $event->id,
                'title' => str_limit($event->title_en, 100),
                'start' => $event->date->toDateString(),
                'end'   => $event->date->toDateString(),
                'location'   => $event->location_en,
                'club'   => $event->club? $event->club->name_en : '',
                'url'   => $event->path(true),
                'color' => $color
            ]);
        });

        return $arr;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function updateEvent(Request $request)
    {
        $event = Event::findOrFail($request->id);
        $date = Carbon::parse($request->date)->toDateString();
        $updateArray = [
            'date'  => $date,
        ];
        $event->update($updateArray);

        if ($event->update($updateArray)){
            return Response::json(compact('date'), 201);
        }
        else return Response::json(['msg' => 'unable to update'], 500);

    }



}
