<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentsRequest;
use App\Picture;
use App\Department;
use File;
use Illuminate\Http\Request;
use DataTables;
use Session;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.departments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department = new Department;

        return view('admin.departments.create', compact('department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentsRequest $request)
    {

        $image_path = '';
        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/departments/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $image_path = '/' . $image_path;
        }

        $department = Department::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'picture' => $image_path,
            'icon'    => $request->icon,
        ]);

        Session::flash('toast', ['Department is Successfully Created', 'success']);
        return redirect("/admin/departments");
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::findOrFail($id);
        return view('admin.departments.show', compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('admin.departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentsRequest $request, Department $department)
    {
        if ($request->hasFile('picture')) {
            File::delete(public_path(str_replace('/', '\\', $department->picture)));
            $image = $request->file('picture');
            $image_name = Picture::getSanitizedName($image);
            $image_path = 'images/departments/'.$image_name;
            $picture = Picture::modifyImage($image);
            $picture->save($image_path);
            $department->update([
                'picture' => '/'.$image_path,
            ]);
        }
        $department->update([
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'icon'    => $request->icon,
        ]);
        Session::flash('toast','Department is Successfully Updated');
        return redirect("/admin/departments");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        File::delete(public_path(str_replace('/', '\\', $department->picture)));
        $department->delete();
        Session::flash('toast','Department is Successfully Deleted');
        return redirect('admin/departments');
    }

    public function getDepartmentsData(Request $request)
    {
        $departments = Department::query();

        return DataTables::of($departments)->editColumn('created_at', function ($department) {
            return $department->created_at->toFormattedDateString();
        })->editColumn('picture', function ($department) {
            return "<img class='table-img img-responsive' src='" . $department->getPicture() . "' alt='$department->title'>";

        })->editColumn('icon', function ($department) {
            return '<div class="model-icon"><i class="fa ' . $department->icon . '"></i></div>';
        })->rawColumns(['picture', 'icon'])
            ->make(true);
    }
}
