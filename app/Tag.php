<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $fillable = [
        "name_en",
        "name_ar",
    ];

    public function pictures()
    {
        return $this->morphedByMany('App\Picture', 'taggable');
    }

    public function facilities()
    {
        return $this->morphedByMany('App\Facility', 'taggable');
    }

    public function events()
    {
        return $this->morphedByMany('App\Event', 'taggable');
    }

    public function path($admin = false){
        if($admin)
            return '/admin/tags/' . $this->id;
        return '/tag/'.$this->id;
    }
}
