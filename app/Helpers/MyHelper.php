<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;


class MyHelper
{
    /**
     * @param $targetUrls
     * @param string $urlPathInfo
     * @param bool $strict
     * @return string
     */
    public static function urlActive($targetUrls, string $urlPathInfo, $strict = false)
    {
        if ($strict){
            if (ends_with($urlPathInfo, $targetUrls))
                return 'active';
            return '';
        }
        if (str_contains($urlPathInfo, $targetUrls)){
            return 'active';
        }
        return '';
    }
}