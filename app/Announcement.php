<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Session;

class Announcement extends Model
{
    protected $table = 'posts';

    use StatisticsFunctions;

    protected $fillable = [
        "title_en",
        "title_ar",
        "body_en",
        "body_ar",
        "status",
        "type",
        "picture"
    ];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            $builder->whereIn('type', [Post::$TYPE_FEATURED_ANNOUNCEMENT, Post::$TYPE_NORMAL_ANNOUNCEMENT]);
        });
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    public function tagsString()
    {
        return implode(', ', $this->tags->pluck('name')->toArray());
    }
    public function tagsWithLinks()
    {
        $tags = $this->tags->map(function ($tag) {
            $url = route('announcements.tag', $tag->id);
            return  "<a href='{$url}'>{$tag->name}</a>";
        })->toArray();
        return implode(', ', $tags);
    }

    public function path($admin = null){
        if($admin){
            return '/admin/announcements/' . $this->id;
        }
        return '/announcements/' . $this->id;
    }
    public function togglePublished(){
        if ($this->status){
            $this->status = 0;
            Session::flash('toast','announcement is unpublished');
        }
        else{
            $this->status = 1;
            Session::flash('toast','announcement is published');
        }
        $this->save();
    }
    public function toggleFeatured(){
        if ($this->type === Post::$TYPE_FEATURED_ANNOUNCEMENT){// if announcement is featured announcement
            $this->type = Post::$TYPE_NORMAL_ANNOUNCEMENT;
            Session::flash('toast', ['Announcement is not featured anymore', 'success']);
        }
        else if ($this->type === Post::$TYPE_NORMAL_ANNOUNCEMENT){
            if (static::where('type', Post::$TYPE_FEATURED_ANNOUNCEMENT)->count() >= Post::$LIMIT_FEATURED_ANNOUNCEMENTS){ // if there are more than 4 featured announcements
                Session::flash('toast', ['You cant have more than ' . Post::$LIMIT_FEATURED_ANNOUNCEMENTS . ' featured announcements', 'error']);
                return;
            }
            if (! $this->picture) {
                Session::flash('toast', ['A featured announcement must have an image', 'error']);
                return;
            }
            $this->type = Post::$TYPE_FEATURED_ANNOUNCEMENT;
            Session::flash('toast', ['Announcement will now appear in the website footer', 'success']);
        }
        $this->save();
    }

    public function getPicture(){
        if ($this->picture){
            return $this->picture;
        }
        else return '/images/announcements/default.png';
    }
    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }
    public function scopeFeatured($query)
    {
        return $query->where('type', Post::$TYPE_FEATURED_ANNOUNCEMENT);
    }
    public function scopeSearch($query, $keywords)
    {
        return $query
            ->where('title_en', 'LIKE', '%' . $keywords . '%')
            ->orWhere('title_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('body_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('body_en', 'LIKE', '%' . $keywords . '%');
    }

    public static function publishedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);
        $publishedCount = static::where('created_at', '>=', $date->toDateString())->where('status', 1)->count();
        $count = static::where('created_at', '>=', $date->toDateString())->count();

        return compact('publishedCount', 'count');
    }
}
