<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        "code",
        "name_en",
        "name_ar",
        "credits",
        "course_type_id",
        "order_number",
        "batch",
    ];

    public function precos()
    {
        return $this->hasMany('App\Course', 'pre_id');
    }
    public function coReqs()
    {
        return $this->precos()->where('is_coreq', '1');
    }
    public function preReqs()
    {
        return $this->precos()->where('is_coreq', '0');
    }
    public function majors()
    {
        return $this->belongsToMany('App\Major');
    }
    public function courseType()
    {
        return $this->belongsTo('App\CourseType');
    }
    public function majorsString()
    {
        return implode(', ', $this->majors->pluck('code')->toArray());
    }
    public function majorsWithLinks()
    {
        $majors = $this->majors->map(function ($major) {
            $url = '/courses/majors/' . $major->id;
            return  "<a href='{$url}'>{$major->name_en}</a>";
        })->toArray();
        return implode(', ', $majors);
    }

    public function path($admin = false){
        if($admin)
            return '/admin/courses/' . $this->id;

        return '/courses/' . $this->id;
    }

    public function getDataCategory()
    {
        if ($this->majors_count){
            return $this->majorsString();
        }

        return $this->courseType->name_en;

    }
}
