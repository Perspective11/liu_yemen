<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        "id_number",
        "major_id",
        "full_name",
    ];

    public function certs()
    {
        return $this->belongsToMany('App\Cert');
    }
    public function events()
    {
        return $this->belongsToMany('App\Event');
    }
    public function templates()
    {
        return $this->belongsToMany('App\CertTemplate');
    }
}
