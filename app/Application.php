<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use StatisticsFunctions;

    protected $fillable = [
        "name",
        "email",
        "level_of_education",
        "field_of_study",
        "notes",
        "resume",
    ];

    public function path($admin = null)
    {
        if ($admin)
        {
            return '/admin/applications/' . $this->id;
        }
        return '/applications/' . $this->id;
    }

    public static function getSanitizedFileName($file){
        $extension = $file->guessExtension();
        $fileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $sanitizedName = str_limit( preg_replace( '/[^a-z0-9]+/', '-', strtolower( $fileName)), 20 , '');
        $fileName = Carbon::now()->timestamp . '_' . str_random(5) . "_" . $sanitizedName . '.' . $extension;
        return $fileName;
    }
}
