<?php

namespace App\Providers;

use App\Setting;
use Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        try{
            if (! Cache::has('appSettings')){
                $appSettings = Setting::pluck('value', 'key')->toArray();
                \Cache::forever('appSettings', $appSettings);
            }
        }
        catch (\Exception $e){

        }

        $appSettings = Cache::get('appSettings');

        View::share(compact('appSettings'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
