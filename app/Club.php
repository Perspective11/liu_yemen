<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $fillable = [
        "name_en",
        "name_ar",
        "description_en",
        "description_ar",
        "picture"
    ];

    public function events()
    {
        return $this->hasMany('App\Event');
    }
    public function getPicture(){
        if ($this->picture){
            return $this->picture;
        }
        else return '/images/clubs/default.png';
    }

    public function path($admin = null){
        if($admin){
            return '/admin/clubs/' . $this->id;
        }
        return '/clubs/' . $this->id;
    }

}
