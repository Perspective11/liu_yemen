<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseType extends Model
{
    protected $fillable = [
        "name_en",
        "name_ar",
        "description_en",
        "description_ar",
    ];

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function path($admin = null){
        if($admin){
            return '/admin/courses/' . $this->id;
        }
        return '/courses/' . $this->id;
    }


}
