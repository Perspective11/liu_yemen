<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        "title_en",
        "title_ar",
        "description_en",
        "description_ar",
        "icon",
    ];

    public function path($admin = false){
        if($admin)
            return '/admin/services/' . $this->id;

        return '/services/' . $this->id;
    }
}
