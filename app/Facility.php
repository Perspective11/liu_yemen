<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $fillable = [
        "name_en",
        "name_ar",
        "description_en",
        "description_ar",
    ];

    public static $image_limit = 7;

    public function pictures()
    {
        return $this->morphMany('App\Picture', 'imageable');
    }

    public function getPicture(){
        if ($this->pictures()->count()){
            return $this->pictures()->first()->image_path;
        }
        else return '/images/facilities/default.png';
    }

    public function path($admin = null){
        if($admin){
            return '/admin/facilities/' . $this->id;
        }
        return '/facilities/' . $this->id;
    }
}
