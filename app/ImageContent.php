<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageContent extends Model
{
    protected $fillable = [
        "name",
        "description",
        "position",
        "size",
        "repeat",
        "attachment",
        "color",
        "resolution_x",
        "resolution_y",
        "aspect_ratio",
        "image_path",
        "thumb_path",
        "type",
        "image_category",
    ];
    public function getBackgroundCssString(){
        $color = 'white';
        $image = 'url(' . $this->image_path . ')';
        $position = $this->position ? $this->position : 'center';
        $size = $this->size ? $this->size : 'cover';
        $repeat = $this->repeat ? $this->repeat : 'no-repeat';
        $attachment = $this->attachment ? $this->attachment : 'scroll';

        return $color . ' ' . $image . ' ' . $position . '/' . $size . ' ' . $repeat . ' ' . $attachment;
    }
}
