<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = [
        "name_en",
        "name_ar",
        "quote_en",
        "quote_ar",
        "picture",
    ];

    public function getPicture(){
        if ($this->picture){
            return $this->picture;
        }
        else return '/images/testimonials/default.png';
    }

    public function path($admin = null){
        if($admin){
            return '/admin/testimonials/' . $this->id;
        }
        return '/testimonials/' . $this->id;
    }
}
