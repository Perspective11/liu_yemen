<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cert extends Model
{
    protected $fillable = [
        "barcode",
        "student_id",
        "event_id",
        "cert_template_id",
    ];

    public function students()
    {
        return $this->belongsToMany('App\Student');
    }
    public function events()
    {
        return $this->belongsToMany('App\Event');
    }
    public function templates()
    {
        return $this->belongsToMany('App\CertTemplate');
    }
}
