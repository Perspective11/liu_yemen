<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentCategory extends Model
{
    protected $fillable = [
        "name_en",
        "name_ar",
    ];

    public function contents()
    {
        return $this->hasMany('App\Content');
    }
}
