<?php

namespace App;

use Cache;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'key', 'name_en', 'name_ar', 'value'
    ];

    public function path()
    {
        return '/admin/settings/' . $this->id;
    }

    public static function links()
    {
        $appSettings = Cache::get('appSettings');
        $linksArray = '';
        if ($appSettings){
            $linksArray = (array)json_decode($appSettings['social_links']);
        }
        else{
            $linksArray = (array)json_decode(Setting::where('key','social_links')->first()->value);
        }
        $c = collect([]);
        if (isset($linksArray['facebook'])) {
            $c->put('facebook', [
                'site'  => 'facebook',
                'icon'  => 'facebook-f',
                'color' => '#3b5998',
                'link'  => $linksArray['facebook'],
            ]);
        }
        if (isset($linksArray['twitter'])) {
            $c->put('twitter',[
                'site'  => 'twitter',
                'icon'  => 'twitter',
                'color' => '#1da1f2',
                'link'  => $linksArray['twitter'],
            ]);
        }

        if (isset($linksArray['instagram'])) {
            $c->put('instagram',[
                'site' => 'instagram',
                'icon' => 'instagram',
                'color' => '#c13584',
                'link' => $linksArray['instagram'],
            ]);
        }
        if (isset($linksArray['linkedin'])) {
            $c->put('linkedin',[
                'site' => 'linkedin',
                'icon' => 'linkedin',
                'color' => '#0077b5',
                'link' => $linksArray['linkedin'],
            ]);
        }

        return $c;
    }

}
