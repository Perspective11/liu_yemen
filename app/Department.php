<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        "name_en",
        "name_ar",
        "picture",
        "icon"
    ];
    public function majors()
    {
        return $this->hasMany('App\Major');
    }

    public function getPicture(){
        if ($this->picture){
            return $this->picture;
        }
        else return '/images/clubs/default.png';
    }

    public function path($admin = null){
        if($admin){
            return '/admin/departments/' . $this->id;
        }
        return '/departments/' . $this->id;
    }
}
