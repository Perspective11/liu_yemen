<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertTemplate extends Model
{
    protected $fillable = [
        "name",
        "description",
        "view_path",
    ];
}
