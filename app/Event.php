<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Session;

class Event extends Model
{
    use StatisticsFunctions;

    public static $TYPE_NORMAL_EVENT = 0;
    public static $TYPE_FEATURED_EVENT = 1;
    public static $LIMIT_FEATURED_EVENTS = 3;


    protected $fillable = [
        "club_id",
        "title_en",
        "title_ar",
        "body_en",
        "body_ar",
        "location_en",
        "location_ar",
        "date",
        "poster_path",
        "status"
    ];
    protected $dates = [
      'date'
    ];

    public static $image_limit = 4;

    public function club()
    {
        return $this->belongsTo('App\Club');
    }
    public function students()
    {
        return $this->belongsToMany('App\Student');
    }
    public function templates()
    {
        return $this->belongsToMany('App\CertTemplate');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    public function tagsString()
    {
        return implode(', ', $this->tags->pluck('name')->toArray());
    }
    public function tagsWithLinks()
    {
        $tags = $this->tags->map(function ($tag) {
            $url = '/events/tag/' . $tag->id;
            return  "<a href='{$url}'>{$tag->name}</a>";
        })->toArray();
        return implode(', ', $tags);
    }
    public function pictures()
    {
        return $this->morphMany('App\Picture', 'imageable');
    }

    public function path($admin = null){
        if($admin){
            return '/admin/events/' . $this->id;
        }
        return '/events/' . $this->id;
    }
    public function togglePublished(){
        if ($this->status){
            $this->status = 0;
            Session::flash('toast','Event is unpublished');
        }
        else{
            $this->status = 1;
            Session::flash('toast','Event is published');
        }

        $this->save();
    }
    public function toggleFeatured(){
        if ($this->type === static::$TYPE_FEATURED_EVENT){// if event is featured event
            $this->type = static::$TYPE_NORMAL_EVENT;
            Session::flash('toast', ['Event is not featured anymore', 'success']);
        }
        else if ($this->type === static::$TYPE_NORMAL_EVENT){
            if (static::where('type', static::$TYPE_FEATURED_EVENT)->count() >= static::$LIMIT_FEATURED_EVENTS){ // if there are more than 4 featured events
                Session::flash('toast', ['You cant have more than ' . static::$LIMIT_FEATURED_EVENTS . ' featured events', 'error']);
                return;
            }
            if (! $this->poster_path) {
                Session::flash('toast', ['A featured event must have a poster', 'error']);
                return;
            }
            $this->type = static::$TYPE_FEATURED_EVENT;
            Session::flash('toast', ['Event will now appear in the home page', 'success']);
        }
        $this->save();
    }
    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }
    public function scopeFeatured($query)
    {
        return $query->where('type', static::$TYPE_FEATURED_EVENT);
    }
    public function scopeSearch($query, $keywords)
    {
        return $query
            ->where('title_en', 'LIKE', '%' . $keywords . '%')
            ->orWhere('title_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('body_ar', 'LIKE', '%' . $keywords . '%')
            ->orWhere('body_en', 'LIKE', '%' . $keywords . '%');
    }
    public function scopeUpcoming($query)
    {
        return $query->where('date', '>=', Carbon::today()->toDateString());
    }
    public function scopePast($query)
    {
        return $query->where('date', '<', Carbon::today()->toDateString());
    }
    public function getTime($localized = false)
    {
        if ($this->date < Carbon::today()->toDateString()){
            if ($localized){
                return __('events.past');
            }
            return 'past';
        }

        if ($this->date >= Carbon::today()->toDateString()){
            if($localized){
                return __('events.upcoming');
            }
            return 'upcoming';
        }

    }

    public function getPicture($large = false){
        if ($this->poster_path)
            return $this->poster_path;
        if ($this->pictures()->count()){
            if ($large){
                return $this->pictures()->first()->image_path;
            }
            return $this->pictures()->first()->thumb_path;
        }
        else return '/images/events/default.jpg';
    }

    public static function publishedPercentage($days = 30)
    {
        $date = Carbon::now()->addDays(-$days);

        $count = static::where('created_at', '>=', $date->toDateString())->count();
        $publishedCount = static::where('created_at', '>=', $date->toDateString())->where('status', 1)->count();

        return compact('publishedCount', 'count');
    }
}
