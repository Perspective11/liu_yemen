<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    const TYPE_NORMAL = 0;
    const TYPE_EDITOR = 1;

    protected $fillable = [
        'content_category',
        'key',
        'name_en',
        'name_ar',
        'value_en',
        'value_ar',
        'type',
        'max_char',
    ];
    public function path()
    {
        return '/admin/contents/'.$this->id;
    }
}
